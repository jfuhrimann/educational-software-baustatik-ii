function result = locklines(handles)
for j = 1 : numel(handles.pointnames)
    nr = handles.points.(char(handles.pointnames(j))).pointnr;
    r = handles.points.(char(handles.pointnames(j))).r;
    constrainttype = handles.points.(char(handles.pointnames(j))).constrainttype;
    angle = handles.points.(char(handles.pointnames(j))).constraintangle;
    neighbours = handles.points.(char(handles.pointnames(j))).neighbours;
    %which beams are at this point?
    beams = cell(numel(neighbours),3);
    for i = 1 : numel(neighbours)
        beams{i,1} = beamname(nr,neighbours(i));
        beams{i,2} = handles.beams.(beams{i,1}).n;
        beams{i,3} = handles.beams.(beams{i,1}).t;
    end
% go through all of the constraints
% torsional spring/ fixed/ pinned
    if strcmp(constrainttype,'pin')||strcmp(constrainttype,'fixed')||strcmp(constrainttype,'torsional spring')
        if numel(beams(:,1)) == 1
            handles.points.(pointname(nr)).pvec = [r,r];
            handles.points.(pointname(nr)).tvec = [beams{1,2}, beams{1,3}];
        else
            handles.points.(pointname(nr)).pvec = [r];
            handles.points.(pointname(nr)).tvec = [beams{1,3}];
            for m = 2 : numel(beams(:,1))
                for n = 1 : numel(handles.points.(pointname(nr)).tvec(1,:))
                if handles.points.(pointname(nr)).tvec(:,n) == beams{m,3}
                    stop = true;
                    break;
                else
                    stop = false;
                end
                end
                if not(stop)
                    handles.points.(pointname(nr)).pvec = [handles.points.(pointname(nr)).pvec, r];
                    handles.points.(pointname(nr)).tvec = [handles.points.(pointname(nr)).tvec, beams{m,3}];
                end
            end
        end
        if numel(handles.points.(pointname(nr)).pvec(1,:))<2
            handles.points.(pointname(nr)).pvec = [handles.points.(pointname(nr)).pvec, r];
            handles.points.(pointname(nr)).tvec = [handles.points.(pointname(nr)).tvec, beams{1,2}];
        end
             
    elseif strcmp(constrainttype,'roller')
        handles.points.(pointname(nr)).pvec = r;
        if angle == 1000
            handles.points.(pointname(nr)).tvec = beams{1,2};
        else 
            handles.points.(pointname(nr)).tvec = [-sind(angle); cosd(angle)];
        end
    elseif strcmp(constrainttype,'spring')
        handles.points.(pointname(nr)).pvec = r;
        if numel(neighbours)==1
        handles.points.(pointname(nr)).tvec = beams{1,2};
        else
            p=[];
            q=[];
            for j = 1 : numel(neighbours)
                if neighbours(j) > nr
                    p = [p,neighbours(j)];
                end
                if handles.points.(pointname(nr)).neighbours(j) < nr
                    q = [q, neighbours(j)];
                end
            end
            if min(q) < nr
                handles.points.(pointname(nr)).tvec = handles.beams.(beamname(min(q),nr)).n;
            else handles.points.(pointname(nr)).tvec = handles.beams.(beamname(nr,min(p))).n;
            end
        end
    end
end
%plotting of all locklines
% for u = 1 : numel(handles.pointnames)
%     if isfield(handles.points.(pointname(u)),'pvec')
%     for v = 1 : numel(handles.points.(pointname(u)).pvec(1,:))
%         lname = strcat('l',num2str(v));
%         handles.points.(pointname(u)).lines.(lname) = plotline(handles.points.(pointname(u)).pvec(:,v),handles.points.(pointname(u)).tvec(:,v));
%     end
%     end
% end
result = handles;
end