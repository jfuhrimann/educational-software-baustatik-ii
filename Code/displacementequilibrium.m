function [result,h,Fig] = displacementequilibrium(var,handles,h,Fig)
pointnames = handles.pointnames;
othervars = symvar(handles.varindep);
othervars(othervars == var) = [];
%For all of the points, find their virtual displacement, depending on a
%virtual rotation of angle "var" with value 1. All other degrees of freedom
%are zero at this point of time

h = equation(Fig,h,strcat(' $Displacement equilibrium of $ ',latexpsi(char(string(var))),' $:'));
for i = 1 : numel(pointnames)
    handles.points.(char(pointnames(i))).vdisp = subs(handles.points.(char(pointnames(i))).v,var,1);
    for j = 1 : numel(othervars)
        handles.points.(char(pointnames(i))).vdisp = subs(handles.points.(char(pointnames(i))).vdisp,othervars(j),0);
    end
    point = char(pointnames(i));
    bugsymvar = [];
    for s = 1 : 2
    currentsymvar = symvar(handles.points.(point).vdisp(s));
    for n = 1 : numel(currentsymvar)
    if not(ismember(currentsymvar(n),bugsymvar))
        bugsymvar = [bugsymvar, currentsymvar(n)];
    end
    end
    end
    if not(isempty(bugsymvar))
    for s = 1 : numel(bugsymvar)
        handles.points.(point).vdisp = subs(handles.points.(point).vdisp,bugsymvar(s),0);
    end
    end
end

if not(isfield(handles,'DisplVisFignr'))
    handles.DisplVisFignr = 1;
end
if h.interactive
    [h,Fig] = VirtualDisplacementVisulisation_MC(var,handles,h,Fig);
    h = question(Fig,h,'Choose the correct virtual displacement mechanism',' ',' ',true,'virtual mechanism');
end
[h,Fig] = VirtualDisplacementVisualisation(var,handles,h,Fig);
set(getpoints,'currentaxes',handles.axes1);
set(handles.axes1,'Parent',getpoints);
hold on
set(0,'currentfigure',Fig);
%Go through all points and project their pointloads
dw_ext = 0;
dw_int = 0;

if strcmp(handles.unit,'kN'), syms kNm; unit = kNm;
elseif strcmp(handles.unit,'q'), syms ql; unit = ql;
elseif strcmp(handles.unit,'Q'), syms Ql; unit = Ql;
else syms M; unit = M; end

for i = 1 : numel(pointnames)
    currentpoint = char(pointnames(i));
    %Pointloads at points
    dw_ext = dw_ext + dot(handles.points.(currentpoint).vdisp,handles.points.(currentpoint).F)*unit;
    new_work = dot(handles.points.(currentpoint).vdisp,handles.points.(currentpoint).F)*unit;
    if new_work ~= 0
        if not(h.pldone)
            h = theory(Fig,h,[{'External Work of point loads'},{'title'},{1};...
            {'Calculate the virtual displacement of the point the load acts on, depending on the current independent beam angle of rotation \psi_{i,j}=1. Project the point load in the direction of the displacement and multipliy the values.'},{'text'},{1}]);
            h.pldone = true;
        end
        h = equation(Fig,h,strcat(' $ Point load at point ',num2str(handles.points.(currentpoint).pointnr),'$: '));
        if h. interactive
            h = diplacementequilibrium_wrong(h,new_work,var,'ext');
            h = question(Fig,h,strcat('Please choose the correct equation for work done by the Point load at point ',num2str(handles.points.(currentpoint).pointnr)),' ','Look at the polplan drawn above. At the point of an external point load, calculate the virtual displacement caused by a unit rotation of the current angle. Project the force in the direction of the virtual displacement and multiply these values. Be careful with the signs!',true,'beam-end-moment');
        end
        h = equation(Fig,h,strcat('\delta W_{ext} = \delta W_{ext} +',latex_equationconverter(new_work),'=', latex_equationconverter(dw_ext)));
        new_work = 0;
    end
    %Pointmoments
    %ABKL�REN! NEGLIGLIBLE?
end
beamnames = handles.beamnames;
for i = 1 : numel(beamnames)
    currentbeam = char(beamnames(i));
    pt1 = handles.beams.(currentbeam).beampoints(1);
    pt2 = handles.beams.(currentbeam).beampoints(2);
    l = handles.beams.(currentbeam).l;
    F = cell2mat(handles.beams.(currentbeam).F);
    %Work of Pointloads in beam
    for p = 1 : numel(F(:,1))
        %interpolate the virtual displacement at the location of F(i,:)
        %from the known velocities at the endpoints of the beam
        vload = handles.points.(pointname(pt1)).vdisp + ...
            F(p,2)*(handles.points.(pointname(pt2)).vdisp - handles.points.(pointname(pt1)).vdisp)/l;
        % Create a vector from the angle of the pointload F(i,3) for
        alpha = F(p,3);
        Fvec = F(p,1)*[cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)]*handles.beams.(currentbeam).n;
        dw_ext = dw_ext + dot(Fvec,vload)*unit;
        new_work = dot(Fvec,vload)*unit;
        if new_work ~= 0
            if not(h.pldone)
                h = theory(Fig,h,[{'External Work of point loads'},{'title'},{1};...
                {'Calculate the virtual displacement of the point the force acts on, depending on the current independent beam angle of rotation \psi_{i,j}=1. Project the point load in the direction of the displacement and multipliy the values.'},{'text'},{1};...
                {'theory_pl.PNG'},{'image'},{[10]}]);
                h.pldone = true;
            end
            %Write the equation for the external work:
            h = equation(Fig,h,strcat(' $Point load at beam ',num2str(pt1),'-',num2str(pt2),' at s = ',num2str(F(p,2)),'$: '));
            if h. interactive
                h = diplacementequilibrium_wrong(h,new_work,var,'ext');
                h = question(Fig,h,strcat('Equation for Work done by the Point load at beam ',num2str(pt1),'-',num2str(pt2),' at s = ', {' '},(checkfracstring(F(p,2)))),' ','Look at the polplan drawn above. At the point of an external point load, calculate the virtual displacement caused by a unit rotation of the current angle. Project the force in the direction of the virtual displacement and multiply these values. Be careful with the signs!',true,'beam-end-moment');
            end
            h = equation(Fig,h,strcat('\delta W_{ext} = \delta W_{ext} +',latex_equationconverter(new_work),'=', latex_equationconverter(dw_ext)));
            new_work = 0;
        end
    end
        
    %Angle of the beam in the current state of virtual displacement
    pvec1 = handles.points.(pointname(pt1)).r;
    pvec2 = handles.points.(pointname(pt2)).r;
    tvec1 = rot90vec(handles.points.(pointname(pt1)).vdisp);
    tvec2 = rot90vec(handles.points.(pointname(pt2)).vdisp);
    if not(iscollinear(tvec1,tvec2)) && not(all(tvec1==0)) && not(all(tvec2==0))
        center = joinlines(pvec1,tvec1,pvec2,tvec2);
        tvectot = pvec1 - center;
        normal = rot90vec(tvectot);
        factor = sign(dot(normal,handles.points.(pointname(pt1)).vdisp)); %Factor = 1:  counterclockwise. -1: Clockwise
    else
        if all(tvec1 == tvec2) %translation
            factor = 0;
        else
            tbeam = handles.beams.(currentbeam).t;
            v1p = dot(handles.beams.(currentbeam).n,handles.points.(pointname(pt1)).vdisp);
            v2p = dot(handles.beams.(currentbeam).n,handles.points.(pointname(pt2)).vdisp);
            center = -v1p*l/(v2p-v1p)*tbeam + pvec1;
            if not(all(center == pvec1))
                tvectot = pvec1 - center;
                normal = rot90vec(tvectot);
                factor = sign(dot(normal,handles.points.(pointname(pt1)).vdisp));                
            else
                tvectot = pvec2 - center;
                normal = rot90vec(tvectot);
                factor = sign(dot(normal,handles.points.(pointname(pt2)).vdisp));
            end

        end
    end

    psibeam = factor*abs(dot(handles.points.(pointname(pt2)).vdisp,handles.beams.(currentbeam).n)...
                - dot(handles.points.(pointname(pt1)).vdisp,handles.beams.(currentbeam).n))/l;
    %Drehrichtung?????
    %Work of externeal moments of the beam
    dw_ext = dw_ext + psibeam*handles.beams.(currentbeam).M(1)*unit;
    new_work = psibeam*handles.beams.(currentbeam).M(1)*unit;
    if new_work ~= 0
        if not(h.mdone)
            h = theory(Fig,h,[{'External Work of moments'},{'title'},{1};...
            {'Calculate the angle of rotation \psi of the current beam, depending on the current independent beam angle of rotation \psi_{i,j}=1. Multiply this value with the Moment. If the beam and the external moment do not rotate in the same direction, the work done is negative.'},{'text'},{1};...
            {'theory_m.PNG'},{'image'},{[10]}]);
            h.mdone = true;
        end
        %Write the equation for the external work:
        h = equation(Fig,h,strcat(' $Moment at beam ',num2str(pt1),'-',num2str(pt2),'$: '));
            if h. interactive
                h = diplacementequilibrium_wrong(h,new_work,var,'ext');
                h = question(Fig,h,strcat('Equation for Work done by the Moment at beam ',num2str(pt1),'-',num2str(pt2)),' ','Look at the polplan drawn above. Calculate the virtual rotation of the beam the moment acts on, caused by a unit rotation of the current angle. Multiply this virtual rotation with the moment. Be careful with the signs: Clockwise orientations lead to negative signs!',true,'beam-end-moment');
            end
        h = equation(Fig,h,strcat('\delta W_{ext} = \delta W_{ext} +',latex_equationconverter(new_work),'=', latex_equationconverter(dw_ext)));
        new_work = 0;
    end
    %Work of distributed loads
    qangle = handles.beams.(currentbeam).qangle;
    qa = handles.beams.(currentbeam).q1(1);
    a = handles.beams.(currentbeam).q1(2);
    qb = handles.beams.(currentbeam).q2(1);
    b = handles.beams.(currentbeam).q2(2);
    syms x
    qx(x) = (qb-qa)/(b-a)*(x-a)+qa; %load at position x
    if not((sign(qa)>=0 && sign(qb)>=0) || (sign(qa)<=0 && sign(qb)<=0))
        c = solve( qx(x) == 0 ,x); %point on beam where the load turns 0 (changes sign)
        R1(x) = ((-qa/(c-a)*(x-a)+qa)+qa)/2*(x-a)*[cosd(qangle) -sind(qangle); sind(qangle) cosd(qangle)]*handles.beam.(currentbeam).n; %resultant of load at position x from a to c
        R2(x) = ((qb/(b-c)*(x-c)))/2*(x-c)*[cosd(qangle) -sind(qangle); sind(qangle) cosd(qangle)]*handles.beam.(currentbeam).n; %resultant of load at position x from c to b

        xs1(x) = (x-a)/3 * (qa+2*qx(x))/(qa+qx(x)) + a; %center of mass of load at position x from a to c
        xs2(x) = 2/3*(x-c) + c; %center of mass of load at position x from c to b
        vload1 = handles.points.(pointname(pt1)).vdisp + ...
            xs1(c)*(handels.points.(pointname(pt2)).vdisp - handles.points.(pointname(pt1)).vdisp)/l;
        vload2 = handles.points.(pointname(pt1)).vdisp + ...
            xs2(b)*(handels.points.(pointname(pt2)).vdisp - handles.points.(pointname(pt1)).vdisp)/l; 
        dw_ext = dw_ext + dot(R1(c),vload1) + dot(R2(b),vload2)*unit;
        new_work = dot(R1(c),vload1) + dot(R2(b),vload2)*unit;
    if new_work ~= 0
        %Write the equation for the external work:
        h = equation(Fig,h,strcat(' $Distributed load at beam ',num2str(pt1),'-',num2str(pt2),'$: '));
            if not(h.dldone)
                h = theory(Fig,h,[{'External work of distributed loads'},{'title'},{1};...
                {'Calculate the resultant force of the distributed load and its point of action. Calculate the virtual displacement of this point, depending on the current independent beam angle of rotation \psi_{i,j}=1. Project the resultant force in the direction of the displacement and multiply these values.'},{'text'},{1};...
                {'theory_dl.PNG'},{'image'},{[10]}]);
                h.dldone = true;
            end
            if h. interactive
                h = diplacementequilibrium_wrong(h,new_work,var,'ext');
                h = question(Fig,h,strcat('Equation for Work done by the distributed load at beam ',num2str(pt1),'-',num2str(pt2)),' ','Look at the polplan drawn above. Calculate the resultant force of the distributed load. At the point where the resultant acts, calculate the virtual displacement caused by a unit rotation of the current angle. Project the resultant in the direction of the virtual displacement and multiply these values. Be careful with the signs!',true,'beam-end-moment');
            end
        h = equation(Fig,h,strcat('\delta W_{ext} = \delta W_{ext} +',latex_equationconverter(new_work),'=', latex_equationconverter(dw_ext)));
        new_work = 0;
    end
    elseif qa == 0 && qb == 0
        dw_ext = dw_ext;
     else          
        c = 0;
        R0(x) = (((qb-qa)/(b-a)*(x-a)+qa)+qa)/2*(x-a)*[cosd(qangle) -sind(qangle); sind(qangle) cosd(qangle)]*handles.beams.(currentbeam).n; %resultant of load
        xs0(x) = (x-a)/3 * (qa+2*qx(x))/(qa+qx(x)) + a; %center of mass of load
        vload0 = handles.points.(pointname(pt1)).vdisp + ...
            xs0(b)*(handles.points.(pointname(pt2)).vdisp - handles.points.(pointname(pt1)).vdisp)/l;
        dw_ext = dw_ext + dot(R0(b),vload0)*unit;
        new_work = dot(R0(b),vload0)*unit;
    if new_work ~= 0
        %Write the equation for the external work:
        h = equation(Fig,h,strcat(' $Distributed load at beam ',num2str(pt1),'-',num2str(pt2),'$: '));
            if not(h.dldone)
                h = theory(Fig,h,[{'External work of distributed loads'},{'title'},{1};...
                {'Calculate the resultant force of the distributed load and its point of action. Calculate the virtual displacement of this point, depending on the current independent beam angle of rotation \psi_{i,j}=1. Project the resultant force in the direction of the displacement and multiply these values.'},{'text'},{1};...
                {'theory_dl.PNG'},{'image'},{[10]}]);
                h.dldone = true;
            end
            if h. interactive
                h = diplacementequilibrium_wrong(h,new_work,var,'ext');
                h = question(Fig,h,strcat('Equation for Work done by the distributed load at beam ',num2str(pt1),'-',num2str(pt2)),' ','Look at the pole-plan drawn above: Calculate the virtual rotation of each beam and multiply this value with the corresponding beam end moments. Be careful with the signs: A clockwise orientation leads to a negative sign!',true,'beam-end-moment');
            end
        h = equation(Fig,h,strcat('\delta W_{ext} = \delta W_{ext} +',latex_equationconverter(new_work),'=', latex_equationconverter(dw_ext)));
        new_work = 0;
    end
     end
    
    %Internal work:
    dw_int = dw_int + handles.points.(pointname(pt1)).(pointname(pt2)).M*psibeam;
    new_work = handles.points.(pointname(pt1)).(pointname(pt2)).M*psibeam;
    if new_work ~= 0
        %Write the equation for the external work:
        if not(h.intdone)
            h = theory(Fig,h,[{'Internal Work'},{'title'},{1};...
            {'Calculate the angle of rotation \psi of the current beam, depending on the current independent beam angle of rotation \psi_{i,j}=1. Multiply this value with both beam end moments. According to the sign convention, the beam end moments are positive if they rotate counter-clockwise. If the beam does not rotate in the same direction, the internal work done is negative.'},{'text'},{1};...
            {'theory_mint.PNG'},{'image'},{[10]}]);
            h.intdone = true;
        end
        h = equation(Fig,h,strcat(' $Internal work at beam-end ',num2str(pt1),'-',num2str(pt2),'$: '));
            if h. interactive
                h = diplacementequilibrium_wrong(h,new_work,var,'int');
                h = question(Fig,h,strcat('Equation for internal Work at beam ',num2str(pt1),'-',num2str(pt2)),' ','Look at the pole-plan drawn above: Calculate the virtual rotation of each beam and multiply this value with the corresponding beam end moments. Be careful with the signs: A clockwise orientation leads to a negative sign!',true,'beam-end-moment');
            end
        h = equation(Fig,h,strcat('\delta W_{int} = \delta W_{int} +',latex_equationconverter(new_work),'=', latex_equationconverter(dw_int)));
        new_work = 0;
    end
    dw_int = dw_int + handles.points.(pointname(pt2)).(pointname(pt1)).M*psibeam;
    new_work = handles.points.(pointname(pt2)).(pointname(pt1)).M*psibeam;
    if new_work ~= 0
        %Write the equation for the external work:
        if not(h.intdone)
            h = theory(Fig,h,[{'Internal Work'},{'title'},{1};...
            {'Calculate the angle of rotation \psi of the current beam, depending on the current independent beam angle of rotation \psi_{i,j}=1. Multiply this value with both beam end moments. According to the sign convention, the beam end moments are positive if they rotate counter-clockwise. If the beam does not rotate in the same direction, the internal work done is negative.'},{'text'},{1};...
            {'theory_mint.PNG'},{'image'},{[10]}]);
            h.intdone = true;
        end        
        h = equation(Fig,h,strcat(' $Internal work at beam-end{} ',num2str(pt2),'-',num2str(pt1),'$: '));
            if h. interactive
                h = diplacementequilibrium_wrong(h,new_work,var,'int');
                h = question(Fig,h,strcat('Equation for internal Work at beam-end ',num2str(pt2),'-',num2str(pt1)),' ','External work: Look at the polplan drawn above. At the point of an external force or moment, calculate the virtual displacement or rotation caused by a unit rotation of the current angle. Multiply those virtual displacements with the forces and those virtual rotations with the moments. Internal work: Calculate the virtual rotation of each beam and multiply this with the corresponding beam end moments.',true,'beam-end-moment');
            end
        h = equation(Fig,h,strcat('\delta W_{int} = \delta W_{int} +',latex_equationconverter(new_work),'=', latex_equationconverter(dw_int)));
        new_work = 0;
    end
end

handles.displeqn = [handles.displeqn; dw_ext + dw_int == 0];
equationstr = latex_equationconverter(dw_ext + dw_int == 0);
equationstr = strrep(equationstr,'$','');
set(0,'currentfigure',Fig)

h = equation(Fig,h,' $ Final displacement equilibrium $:');
h = equation(Fig,h,'\delta W_{int} + \delta W_{ext} = 0 \Longleftrightarrow');
h = equation(Fig,h,equationstr);
result = handles;
end