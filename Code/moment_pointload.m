function [M0, Vatot, Vbtot, diagrams] = moment_pointload(l,F,knottype_a,knottype_b,M0, diagrams)

diagrams.pl = [];

syms x M00(x)

    Vatot = [];
    Vbtot = [];

    for i=1:numel(F(:,1))
            F(i,1) = F(i,1)*cosd(F(i,3));
            s = F(i,2);

            if strcmp(knottype_b,'free')
                 Va = F(i,1);
                 Mfixed = -F(i,1)*F(i,2);
                 Vb = 0;
             elseif strcmp(knottype_a,'free')
                 Va = 0;
                 Mfixed = 0;
                 Vb = F(i,1);
             else
                 Va = F(i,1)*(l-s)/l;
                 Mfixed = 0;
                 Vb = F(i,1) - Va;
            end

            Vatot = [Vatot;Va];
            Vbtot = [Vbtot;Vb];

            M01(x) = piecewise(x>=0 & x<s, Va*x, x>=s & x<=l, 0);
            M02(x) = piecewise(x>=0 & x<s, 0, x>=s & x<=l, M01(0)+Va*x - F(i,1)*(x-s));
            M00(x) = piecewise(x>=0 & x<=l, Mfixed) + M01(x) + M02(x);
            
            if i==1
                    diagrams.dl = [M00(x)]; 
            elseif i==2
                    diagrams.m = [M00(x)]; 
            else %if i>=3
                    diagrams.pl = [diagrams.pl; M00(x)];
            end
            
            M0(x) = M00(x) + M0(x);

    end
end