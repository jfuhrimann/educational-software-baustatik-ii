function angles = findangles(eqnarray1, eqnarray2)
%Finds the non-unit symvar of the equations eqnarray1/2
syms EI kNm ql Ql M m l
if not(isempty(eqnarray1))
symvar1 = symvar(eqnarray1);
end
if not(isempty(eqnarray2))
symvar2 = symvar(eqnarray2);
end
angles = [];
if not(isempty(eqnarray1))
    for i = 1 : numel(symvar1)
        if symvar1(i) ~= EI && symvar1(i) ~= kNm && symvar1(i) ~= ql && symvar1(i) ~= Ql && symvar1(i) ~= M && symvar1(i) ~= m && symvar1(i) ~= l && not(ismember(symvar1(i),angles))
        angles = [angles symvar1(i)];
        end
    end
end
if not(isempty(eqnarray2))
    for i = 1 : numel(symvar2)
        if symvar2(i) ~= EI && symvar2(i) ~= kNm && symvar2(i) ~= ql && symvar2(i) ~= Ql && symvar2(i) ~= M && symvar2(i) ~= m && symvar2(i) ~= l && not(ismember(symvar2(i),angles))
        angles = [angles symvar2(i)];
        end
    end
end
end