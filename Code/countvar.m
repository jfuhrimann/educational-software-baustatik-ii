function n = countvar(struct)
%struct must contain (a) field(s) of several symbolic variables.
%countvar counts the amount of different symbolic variables in all fields.
fn = fieldnames(struct);
n = 0;
var = [];
for i = 1 : numel(fn)
    if islogical(struct.(char(fn(i))))
        varnew = [];
    else
    varnew = symvar(struct.(char(fn(i))));
    end
    if not(isempty(var))
    for j = 1 : numel(varnew)
        for k = 1 : numel(var)
            found = false;
        if varnew(j) == var(k)
            found = true;
            break;
        end
        end
        if not(found)
            var = [var varnew(j)];
            n = n + 1;
        end
    end
        else
            var = varnew;
            n = n + numel(varnew);
    end
end
end