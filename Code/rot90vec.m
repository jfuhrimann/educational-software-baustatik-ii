function vrot = rot90vec(v)
%Rotates the 2x1 vector v in counterclockwise direction
vrot = [cosd(90) -sind(90); sind(90) cosd(90)]*v;
end