function p = joinlines(pvec1, tvec1, pvec2, tvec2)

syms lambda1 lambda2
eqns = [pvec1 + tvec1*lambda1 == pvec2 + tvec2*lambda2];
[l1, l2] = solve(eqns,[lambda1, lambda2]);
p = pvec1 + l1*tvec1;
end