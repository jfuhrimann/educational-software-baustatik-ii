function N = unknows(c, N, angleroller, t,neighbours,knottype)

% check how many unknows has the specific constraint 
% (it must be taken in account if the beam has any neighbours)

if neighbours == 1
    if strcmp(c,'fixed') || strcmp(c,'torsional spring')
        N = N + 2;
    elseif strcmp(c, 'pin') || strcmp(c,'spring')
        N = N + 1;
    elseif strcmp(c, 'roller')
               %Find the starting angle (alpha(x direction)=0)
                if t(1) >= 0 && t(2) >= 0             %first sector
                    angle_beam = 270 - atand(t(2)/t(1));
                elseif t(1) < 0 && t(2) >= 0
                    angle_beam = 90 + atand(t(2)/t(1));   %second sector
                elseif t(1) < 0 && t(2) < 0
                    angle_beam = 90 + atand(t(2)/t(1));   %third sector
                elseif t(1) >= 0 && t(2) <= 0
                    angle_beam = 270 + atand(t(2)/t(1));   %fourth sector
                end
                
                if angleroller ~= angle_beam
                    N = N + 1;
                end
%     elseif strcmp(c,'none')
%         N = N;
    end
    
else 
    if strcmp(knottype,'connection')
        N = N + 2;
    elseif strcmp(knottype,'hinge')
        N = N + 1;
    end
end
end