function result = labelmomentlines(ra,rb,n,t,imscale,beam,handles)
%This function labels all of the moment lines at their ends, at 

if strcmp(handles.unit,'kN'), unit = 'kNm';
elseif strcmp(handles.unit,'q'), unit = 'ql^2';
elseif strcmp(handles.unit,'Q'), unit = 'Ql';
else unit = 'M'; end

labelnr = 1;
%Value at a
    xval = handles.beams.(beam).S.a;
    val = handles.beams.(beam).Mx(xval);
    if val ~= 0
        if val < 0, alignment = 'bottom'; else alignment = 'top';end
        pos = ra + xval*t + n*imscale*val;
        handles.beams.(beam).labels.(strcat('text',num2str(labelnr))) = ...
            text(pos(1),pos(2),{' ',' ', ['$' checkfrac(val) unit '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);
        labelnr = labelnr + 1;
    end
%Value at b
    xval = handles.beams.(beam).S.b;
    val = handles.beams.(beam).Mx(xval); 
    if isnan(val), val = 0; end
    if val ~= 0
        if val < 0, alignment = 'bottom'; else alignment = 'top';end
        pos = ra + xval*t + n*imscale*val;
        handles.beams.(beam).labels.(strcat('text',num2str(labelnr))) = ...
            text(pos(1),pos(2),{' ',' ', ['$' checkfrac(val) unit '$']},'Interpreter','latex','HorizontalAlignment','right','VerticalAlignment',alignment);
        labelnr = labelnr + 1;
    end
%Value at sm
    xval = handles.beams.(beam).S.sm;
    val = handles.beams.(beam).Mx(xval);    
    if val ~= 0
        if val < 0, alignment = 'bottom'; else alignment = 'top';end
        pos = ra + xval*t + n*imscale*val;
        handles.beams.(beam).labels.(strcat('text',num2str(labelnr))) = ...
            text(pos(1),pos(2),{' ',' ', ['$' checkfrac(val) unit '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);
        labelnr = labelnr + 1;
    end
%Value at pointloads
    xvec = handles.beams.(beam).S.spl;
    for i = 1 : numel(xvec)
        xval = xvec(i);
        if xval ~= 0
        val = handles.beams.(beam).Mx(xval);    
        if val ~= 0
            if val < 0, alignment = 'bottom'; else alignment = 'top';end
            pos = ra + xval*t + n*imscale*val;
            handles.beams.(beam).labels.(strcat('text',num2str(labelnr))) = ...
                text(pos(1),pos(2),{' ',' ', ['$' checkfrac(val) unit '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);
            labelnr = labelnr + 1;
        end
        end
    end
%Values at continuous maxima
    syms x
    xmaxmin = double(solve(diff(handles.beams.(beam).Mx(x)) == 0,x));
    for i = 1 : numel(xmaxmin)
        xval = xmaxmin(i);
        if xval ~= 0
        val = handles.beams.(beam).Mx(xval);
        dd(x) = diff(diff(handles.beams.(beam).Mx(x)));
        extrtype = dd(xval);
        if val ~= 0
            if extrtype >= 0, alignment = 'bottom'; else alignment = 'top';end
            pos = ra + xval*t + n*imscale*val;
            handles.beams.(beam).labels.(strcat('text',num2str(labelnr))) = ...
                text(pos(1),pos(2),{' ',' ', ['$' checkfrac(val) unit '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment',alignment);
            labelnr = labelnr + 1;
        end
        end
    end
    result = handles;
end