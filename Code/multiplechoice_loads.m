function h = multiplechoice_loads(Fig, h, M, actload)

set(0, 'currentfigure', Fig);

l = h.beam.l;
sh = h.S.sh;
F = cell2mat(h.beam.F(actload,1))*cosd(cell2mat(h.beam.F(actload,3)));
spl = cell2mat(h.beam.F(actload,2));
pa = pointname(h.beam.beampoints(1));
pb = pointname(h.beam.beampoints(2));
ca = h.S.newconstrainta;
cb = h.S.newconstraintb;
angleroller_a = h.points.(pa).constraintangle;
angleroller_b = h.points.(pb).constraintangle;
t = [1;0];

syms x
M(x) = M;
max = h.S.spl(actload);
Fold = false;

colorM = [0.5 0.8 1];

heightgrafic = 65/214*h.screendim(4) - 15240/107;
widthgrafic = 15/134*h.screendim(3) + 540/67;
left = h.graficpanel.Position(3)/2-widthgrafic/2;
height = h.graficpanel.Position(4);
                       
a = [1, 2, 3];
a = a(randperm(length(a)));
b = 0.09*h.screendim(4);
pos1 = height-heightgrafic*a(1)-20*a(1)-b;
pos2 = height-heightgrafic*a(2)-20*a(2)-b;
pos3 = height-heightgrafic*a(3)-20*a(3)-b;

h.multiplechoice.txt = uicontrol('Parent',h.graficpanel,...
       'Style','text',...
       'Position',[20 height-35 300 20],...
       'String','Choose the right moment line:',...
        'HorizontalAlignment','left',...
        'FontSize',12,...
        'FontName','Calibri',...
        'FontWeight','bold',...
        'Background',h.graficpanel.BackgroundColor);
   
h.multiplechoice1 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos1+heightgrafic, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);
h.multiplechoice2 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos2+heightgrafic, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);
h.multiplechoice3 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos3+heightgrafic, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);

% GRAFIC 1
        h.multiplechoice.grafic1 = axes('Parent', h.graficpanel,'units','pixels');
        h.multiplechoice.grafic1.Position = [left, pos1+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic1,'xtick',[]);
        set(h.multiplechoice.grafic1,'ytick',[]);
        set(h.multiplechoice.grafic1,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic1,'ycolor',[0.9294    0.9529    0.9882]);
        hold on;
        
        if F == 0
            F = 1;
            Fold = true;
        end
        
        if Fold
            N(x) = solvemoment(l,[zeros(2, 3); F, spl, 0; zeros(1, 3)] ,0,0,0,0,0,0,'hinge','hinge');
        elseif strcmp(ca, 'none') || (strcmp(ca,'roller') && iscollinear([-sind(angleroller_a); cosd(angleroller_a)],t))
            N(x) = M(x) + piecewise(x>=0 & x<= l, -1/3*M(l)*x/l);
        elseif strcmp(cb, 'none') || (strcmp(cb,'roller') && iscollinear([-sind(angleroller_b); cosd(angleroller_b)],t))
            N(x) = M(x) + piecewise(x>=0 & x<= l, -1/3*M(0) + 1/3*M(0)*x/l);
        elseif (strcmp(ca, 'fixed') || strcmp(ca, 'torsional spring')) && ~(strcmp(cb, 'fixed') || strcmp(cb, 'torsional spring'))
                N(x) = M(x) + piecewise(x>=0 & x<= l, -1/3*M(max) + 1/3*M(max)*x/l);
        elseif ~(strcmp(ca, 'fixed') || strcmp(ca, 'torsional spring')) && (strcmp(cb, 'fixed') || strcmp(cb, 'torsional spring')) && sh < 0
                N(x) = M(x) + piecewise(x>=0 & x<= l, -1/3*M(max)*x/l);
        elseif strcmp(ca, 'torsional spring') && strcmp(cb, 'fixed') && sh < 0
                N(x) = M(x) + piecewise(x>=0 & x<= l, -1/5*M(max) + 1/5*M(max)*x/l - 1/3*M(max)*x/l);
        elseif strcmp(cb, 'torsional spring') && strcmp(ca, 'fixed') || (strcmp(ca, 'torsional spring') && strcmp(cb, 'fixed') && sh > 0)
            if sh <= spl
                N(x) = solvemoment(l-sh,[zeros(2, 3); F, sh, 0; zeros(1, 3)] ,0,0,0,0,0,0,'hinge','hinge');
                N(x) = piecewise(x>=0 & x<sh, 0, x>=sh & x<=l, N(x-sh));
            elseif sh > spl
                coinc = randi([1, 3], 1);
                if coinc == 1                
                    N(x) = solvemoment(sh,[zeros(2, 3); F, sh, 0; zeros(1, 3)] ,0,0,0,0,0,0,'connection','free');
                    N(x) = piecewise(x>=0 & x<sh, N(x), x>=sh & x<=l, 0);
                elseif coinc == 2
                    [N2(x), Va] = solvemoment(2/3*sh,[zeros(2, 3); F, spl, 0; zeros(1, 3)] ,0,0,0,0,0,0,'hinge','hinge');
                    N1(x) = solvemoment(1/3*sh,[zeros(2, 3); Va(3), 1/3*sh, 0; zeros(1, 3)] ,0,0,0,0,0,0,'connection','free');
                    N(x) = piecewise(x>=0 & x<1/3*sh, N1(x), x>=1/3*sh & x<2/3*sh, N2(x-1/3*sh), x>=2/3*sh & x<=l, 0);
                else
                    N(x) = solvemoment(sh,[zeros(2, 3); F, spl, 0; zeros(1, 3)] ,0,0,0,0,0,0,'hinge','hinge');
                    N(x) = piecewise(x>=0 & x<sh, N(x), x>=sh & x<=l, 0);
                end
            else
                N(x) = M(x) + piecewise(x>=0 & x<= l, -1/3*M(max) + 1/3*M(max)*x/l - 1/5*M(max)*x/l);
            end
        elseif (strcmp(ca, 'torsional spring') && strcmp(ca, 'torsional spring')) || (strcmp(cb, 'fixed') && strcmp(ca, 'fixed'))
            if max < h.S.spl(actload)
                N(x) = piecewise(x>=0 & x<max, -M(0)*x/max, x>=max & x<h.S.spl(actload), -M(0) + M(0)*(x-max)/(h.S.spl(actload)-max), x>=h.S.spl(actload) & x<=l, 0);
            elseif max == h.S.spl(actload)
                N(x) = solvemoment(l,zeros(3, 3),0,0,0,0,spl,-sign(F),'connection','free');
                N(x) = piecewise(x>=0 & x<spl, N(x), x>=spl & x<=l, 0);
            else
                N(x) = M(x) + piecewise(x>=0 & x<= l, -max/l*M(max) + max/l*M(max)*x/l - (l-max)/l*M(max)*x/l);
            end
        elseif h.S.N.end == 0 && (strcmp(cb, 'fixed') || strcmp(cb, 'torsional spring'))
            if spl < sh
                coinc = randi([1, 2], 1);
                if coinc == 1
                    N(x) = solvemoment(l-sh,[zeros(2, 3); F, sh, 0; zeros(1, 3)] ,0,0,0,0,0,0,'hinge','hinge');
                    N(x) = piecewise(x>=0 & x<sh, 0, x>=sh & x<=l, N(x-sh));
                else
                    N(x) = solvemoment(sh,[zeros(2, 3); F, spl, 0; zeros(1, 3)] ,0,0,0,0,0,0,'hinge','hinge');
                    N(x) = piecewise(x>=0 & x<sh, N(x), x>=sh & x<=l, 0);
                end
            else
                [N1(x), Va] = solvemoment(sh,[zeros(2, 3); F, sh/2, 0; zeros(1, 3)] ,0,0,0,0,0,0,'hinge','hinge');
                N(x) = piecewise(x>=0 & x<sh, N1(x), x>=sh & x<=l, -Va(3)*(x-sh));
            end
        else
            N(x) = -M(x);
        end
        
        fplot(h.multiplechoice.grafic1,-N(x),[0,l],'color',colorM,'Linewidth',2); 
        fplot(h.multiplechoice.grafic1, 0, [0,l],'-k'); %line at x=0 to distinguish
        if M(0) ~= 0, plot(h.multiplechoice.grafic1, [0,0],[0,-N(0)],'color',colorM,'Linewidth',2); end
        if M(l) ~= 0, plot(h.multiplechoice.grafic1, [l,l],[-N(l),0],'color',colorM,'Linewidth',2); end
        h.mc.help1 = [];

% GRAFIC 2                
        h.multiplechoice.grafic2 = axes('Parent', h.graficpanel, 'units','pixels');
        h.multiplechoice.grafic2.Position = [left, pos2+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic2,'xtick',[]);
        set(h.multiplechoice.grafic2,'ytick',[]);
        set(h.multiplechoice.grafic2,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic2,'ycolor',[0.9294    0.9529    0.9882]);
        hold on;
        
        if Fold
            N(x) = piecewise(x>=0 & x<spl, F, x>=spl & x<=l, -F);
        elseif strcmp(ca, 'none') || (strcmp(ca,'roller') && iscollinear([-sind(angleroller_a); cosd(angleroller_a)],t))
            N(x) = M(x) + piecewise(x>=0 & x<= l, -M(l)*x/l);
        elseif strcmp(cb, 'none') || (strcmp(cb,'roller') && iscollinear([-sind(angleroller_b); cosd(angleroller_b)],t))
            N(x) = M(x) + piecewise(x>=0 & x<= l, -M(0) + M(0)*x/l);
         elseif strcmp(ca, 'torsional spring') && strcmp(cb, 'fixed') && sh < 0
                N(x) = M(x) + piecewise(x>=0 & x<= l, -1/3*M(max)*x/l);
        elseif (strcmp(cb, 'torsional spring') && strcmp(ca, 'fixed') || (strcmp(ca, 'torsional spring') && strcmp(cb, 'fixed') && sh > 0))
            if sh > spl
                coinc = randi([1, 3], 1);
                if coinc == 1
                    N(x) = solvemoment(l,zeros(3, 3) ,0,0,0,0,spl,F,'connection','free');
                elseif coinc == 2
                    N(x) = solvemoment(sh,[zeros(2, 3); F, spl, 0; zeros(1, 3)] ,0,0,0,0,0,0,'hinge','hinge');
                    N(x) = piecewise(x>=0 & x<sh, N(x), x>=sh & x<=l, 0);
                else
                    N(x) = diff(M(x));
                end
            elseif (sh - spl) <= l/50
                N(x) = diff(M(x));
            else
                N(x) = M(x) + piecewise(x>=0 & x<= l, -1/3*M(max) + 1/3*M(max)*x/l);
            end
        else
            N(x) = -diff(M(x));
            if (strcmp(cb, 'torsional spring') || strcmp(cb, 'fixed')) && h.S.N.end == 0 && randi([1, 2], 1) == 1
                [N1(x), Va, Vb] = solvemoment(sh,[zeros(2, 3); F, spl, 0; zeros(1, 3)] ,0,0,0,0,0,0,'hinge','hinge');
                N2(x) = solvemoment(l-sh,[zeros(3, 3)] ,0,0,0,0,0,Vb(3)*(l-sh),'free','connection');
                N(x) = piecewise(x>=0 & x<sh, N1(x), x>=sh & x<=l, N2(x-sh));
            end
        end
        
        if isnan(N(0)) && isnan(N(spl)) && isnan(N(l))
            N(x) = piecewise(x>=0 & x<spl, sign(F)*(l-spl)/l, x>=spl & x<=l, -sign(F)*spl/l);
        end
        
        fplot(h.multiplechoice.grafic2, -N(x),[0,l],'color',colorM,'Linewidth',2);
        fplot(h.multiplechoice.grafic2, 0, [0,l],'-k'); %line at x=0 to distinguish       
        if M(0) ~= 0, plot(h.multiplechoice.grafic2, [0,0],[0,-N(0)],'color',colorM,'Linewidth',2); end
        if M(l) ~= 0, plot(h.multiplechoice.grafic2, [l,l],[-N(l),0],'color',colorM,'Linewidth',2); end
        h.mc.help2 = [];

% GRAFIC 3        
        h.multiplechoice.grafic3 = axes('Parent', h.graficpanel, 'units','pixels');
        h.multiplechoice.grafic3.Position = [left, pos3+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic3,'xtick',[]);
        set(h.multiplechoice.grafic3,'ytick',[]);
        set(h.multiplechoice.grafic3,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic3,'ycolor',[0.9294    0.9529    0.9882]);  
        hold on;
       
        fplot(h.multiplechoice.grafic3,-(M(x)),[0,l],'color',colorM,'Linewidth',2); 
        fplot(h.multiplechoice.grafic3, 0, [0,l],'-k'); %line at x=0 to distinguish 
        if M(0) ~= 0, plot(h.multiplechoice.grafic3, [0,0],[0,-M(0)],'color',colorM,'Linewidth',2); end
        if M(l) ~= 0, plot(h.multiplechoice.grafic3, [l,l],[-M(l),0],'color',colorM,'Linewidth',2); end
            
            
end