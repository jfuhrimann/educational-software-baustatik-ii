function result = findphi(handles)
%Function finds the points of a system defined by handles which have a
%possibly nonzero angle of rotation phi. It adds the sym variable to the
%respective point.
handles.phivars = [];
pointnames = handles.pointnames;
for i = 1 : numel(pointnames)
    if numel(handles.points.(char(pointnames(i))).neighbours) >= 2 && ...
            not(strcmp(handles.points.(char(pointnames(i))).knottype,'hinge')) && ...
            not(strcmp(handles.points.(char(pointnames(i))).constrainttype,'fixed'))
        handles.points.(char(pointname(i))).hasphi = true;
        syms (phiname(handles.points.(char(pointname(i)))))
        handles.points.(char(pointname(i))).phisym = eval(phiname(handles.points.(char(pointname(i)))));
        handles.phivars = [handles.phivars eval(phiname(handles.points.(char(pointname(i)))))];
    else
        handles.points.(char(pointname(i))).hasphi = false;
    end
end
result = handles;
end