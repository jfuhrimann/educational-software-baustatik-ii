function result = newpoint(rp,handles)
%Adds a new point that is drwan by a grid to the workspace
handles.gridpointnr = handles.gridpointnr + 1;
%Set gui characteristics invisible and write: click on the points you want
if handles.gridpointnr > 1
    set(handles.togglebutton6,'Visible','on');
end
point = pointname(handles.gridpointnr);
handles.pointnr.String = num2str(handles.gridpointnr);
handles.points.(point).r = rp;
handles.points.(point).pointnr = handles.gridpointnr;
handles.points.(point).pointmarker = ...
    plot(handles.points.(point).r(1),handles.points.(point).r(2),'x-k');
uistack(handles.points.(point).pointmarker,'top');
handles.points.(point).textbox = text(handles.points.(point).r(1)+0.03*(handles.imscale)^0.8,handles.points.(point).r(2)-0.06*(handles.imscale)^0.8,strcat('$\raisebox{.5pt}{\textcircled{\raisebox{-.9pt}{',num2str(handles.points.(point).pointnr),'}}}$'),'Interpreter','latex');
handles.pointnames = fieldnames(handles.points);
handles.totalpoints = handles.gridpointnr;
set(handles.pushbutton14,'Visible','on');
set(handles.edit1,'Visible','on');
set(handles.edit1,'String',num2str(handles.points.(point).r(1)));
set(handles.edit2,'Visible','on');
set(handles.edit2,'String',num2str(handles.points.(point).r(2)));
result = handles;
end