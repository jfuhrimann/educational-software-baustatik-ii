function result = cdial_eval(object,eventdata)
handles = guidata(gcf);
image = object;
type = getappdata(image,'type');
if strcmp(type,'fixed')
    set(handles.text34,'Visible','Off');
    set(handles.edit8,'Visible','Off');
    if strcmp(handles.constraintimages.fixed.patch.Visible,'off') || strcmp(handles.constraintimages.fixed.patch.Visible,'Off')
    set(handles.constraintimages.fixed.patch,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.rigid.patch,'Visible','On');
    handles.temp.constrainttype = 'fixed';
    handles.temp.knottype = 'connection';
    else
    set(handles.constraintimages.fixed.patch,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');

    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    handles.temp = rmfield(handles.temp,'knottype');
    end
elseif strcmp(type,'pin')
    set(handles.text34,'Visible','Off');
    set(handles.edit8,'Visible','Off');
    if strcmp(handles.constraintimages.pin.patch.Visible,'off') || strcmp(handles.constraintimages.pin.patch.Visible,'Off')
    set(handles.constraintimages.pin.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(handles.neighbours.Data) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    else
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'pin';
    else
    set(handles.constraintimages.pin.patch,'Visible','off');
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');

    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.hinge_im,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    if isfield(handles.temp,'knottype')
        handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'roller')
    set(handles.edit8,'Visible','on');
    set(handles.text34,'String','angle');
    set(handles.text34,'Visible','on');
    if strcmp(handles.constraintimages.roller.patch.Visible,'off') || strcmp(handles.constraintimages.roller.patch.Visible,'Off')
    set(handles.constraintimages.roller.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(handles.neighbours.Data) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    else
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'roller';
    else
    set(handles.text34,'Visible','Off');
    set(handles.edit8,'Visible','Off');
    set(handles.constraintimages.roller.patch,'Visible','off');
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');

    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.hinge_im,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    if isfield(handles.temp,'knottype')
        handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'spring')
    if numel(handles.neighbours.Data) == 1
    set(handles.edit8,'Visible','On');
    if strcmp(handles.unit,'kN'), unit = '[1/kNm]';
    else, unit = '[l/EI]'; end
    set(handles.text34,'string',strcat('cf ',unit));
    set(handles.text34,'visible','On');
    if strcmp(handles.constraintimages.spring.patch.Visible,'off') || strcmp(handles.constraintimages.spring.patch.Visible,'Off')
    set(handles.constraintimages.spring.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.constrainttype = 'spring';
    handles.temp.knottype = 'hinge';
    else
    set(handles.text34,'Visible','Off');
    set(handles.edit8,'Visible','Off');
    set(handles.constraintimages.spring.patch,'Visible','off');
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');

    set(handles.connectionimages.hinge_im,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    handles.temp = rmfield(handles.temp,'knottype');
    end
    else
        uiwait(msgbox('Springs at points with more than one neighbour are not supported.','Error!','modal'));
    end
elseif strcmp(type,'torsional spring')
    set(handles.edit8,'Visible','on')
    if strcmp(handles.unit,'kN'), unit = '[m/kN]';
    else, unit = '[l^3/EI]'; end
    set(handles.text34,'string',strcat('cf ',unit));
    set(handles.text34,'visible','On');
    if strcmp(handles.constraintimages.torsional.patch.Visible,'off') || strcmp(handles.constraintimages.torsional.patch.Visible,'Off')
    set(handles.constraintimages.torsional.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(handles.neighbours.Data) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    else
        set(handles.connectionimages.hinge_im,'Visible','on');
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'torsional spring';
    else
    set(handles.text34,'Visible','Off');
    set(handles.edit8,'Visible','Off');
    set(handles.constraintimages.torsional.patch,'Visible','off');
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');

    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.hinge_im,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    if isfield(handles.temp,'knottype')
        handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'none')
    set(handles.text34,'Visible','Off');
    set(handles.edit8,'Visible','Off');    
    if strcmp(handles.constraintimages.none.patch.Visible,'off') || strcmp(handles.constraintimages.none.patch.Visible,'Off')
    set(handles.constraintimages.none.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    
    if numel(handles.neighbours.Data) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    else
    set(handles.connectionimages.free_im,'Visible','on');
    set(handles.connectionimages.free.patch,'Visible','on');
    handles.temp.knottype = 'free';
    end
    handles.temp.constrainttype = 'none';
    else
    
    set(handles.constraintimages.none.patch,'Visible','off');
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');

    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.hinge_im,'Visible','off');
    set(handles.connectionimages.free_im,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    set(handles.connectionimages.free.patch,'Visible','off');
    if isfield(handles.temp,'knottype')
        handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'fixed connection')
    if strcmp(handles.connectionimages.rigid.patch.Visible,'off') || strcmp(handles.connectionimages.rigid.patch.Visible,'Off')
    set(handles.connectionimages.hinge.patch,'Visible','off');
    set(handles.connectionimages.free.patch,'Visible','off');
    set(handles.connectionimages.rigid.patch,'Visible','on');
    handles.temp.knottype = 'connection';
    else
    if strcmp(handles.temp.constrainttype,'pin') || strcmp(handles.temp.constrainttype,'roller') || strcmp(handles.temp.constrainttype,'torsional spring') || strcmp(handles.temp.constrainttype,'none')
    set(handles.connectionimages.rigid.patch,'Visible','off');
    handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'hinge')
    if strcmp(handles.connectionimages.hinge.patch.Visible,'off') || strcmp(handles.connectionimages.hinge.patch.Visible,'Off')
    set(handles.connectionimages.hinge.patch,'Visible','on');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.free.patch,'Visible','off');
    handles.temp.knottype = 'hinge';
    else
    if strcmp(handles.temp.constrainttype,'pin') || strcmp(handles.temp.constrainttype,'roller') || strcmp(handles.temp.constrainttype,'torsional spring') || strcmp(handles.temp.constrainttype,'none')
    set(handles.connectionimages.hinge.patch,'Visible','off');
    handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'free')
    if strcmp(handles.connectionimages.free.patch.Visible,'off') || strcmp(handles.connectionimages.free.patch.Visible,'Off')
    set(handles.connectionimages.free.patch,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    handles.temp.knottype = 'free';
    else
    set(handles.connectionimages.free.patch,'Visible','off');
    handles.temp = rmfield(handles.temp,'knottype');
    end
end
guidata(gcf,handles)
result = handles;
end