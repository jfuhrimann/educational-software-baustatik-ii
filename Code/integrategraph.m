function h = integrategraph(Fig, h)

set(0, 'currentfigure', Fig);

width = floor(5/67*h.screendim(3) + 1030/67); 
distfromprev = 0;
distfromnext = 20;
distbetween = 0.0279851*h.screendim(3) + 7.014925;
height0 = 0.0934579*h.screendim(4) + 9.25233645;

l = h.S.l; 
colorM = [0.2 0.5 1];

syms x

pl1 = 0;
pl2 = 0;
total = h.load;
load = 0;

if h.load ~= 0 && ~h.allfuncequal0
for j=1:h.unitforcenr
    if h.S.X1 ~= 0 && h.S.X2 ~= 0
        if j == 1, X1 = h.S.X1; end
        if j == 2, X1 = h.S.X2; end 
    elseif h.S.X1 ~= 0
        X1 = h.S.X1;
    else
        X1 = h.S.X2;
    end
    
    h = write(Fig, h,'title', 'Superposition of the moment line of the primary structure with the redundant force:', ' ', true);
    fields = fieldnames(h.obj);
 
    for i=1:numel(fields)
        
        if contains(char(fields(i)),'pointload')
            load = load + 1;
            if j == 1
                pl1 = pl1 + 1;
                pl = pl1;
            else
                pl2 = pl2 + 1;
                pl = pl2;
            end
            sort = 'pointload';
        elseif contains(char(fields(i)),'distributedload')
            load = load + 1;
            sort = 'distributedload';
        elseif contains(char(fields(i)),'moment')
            load = load + 1;
            sort = 'moment';
        else
            sort = [];
        end
        
        if ~isempty(sort)
            
            % GRAPH WITH THE POINT LOAD / DISTRIBUTED LOAD / MOMENT
            if strcmp(sort,'pointload')
                h = write(Fig, h,'text', strcat('Point load', {' '}, num2str(pl), ':'), ' ', true);
                if j == 1
                    h.lastbottompos = h.lastbottompos + 20;
                    h = equation(Fig, h,strcat('\mathrm{M_{0}}\ +\ \mathbf{', h.S.nrX_1,'}\ \cdot \mathrm{', h.S.M_1,'}\ =\ \mathrm{M}'));
                else
                    h.lastbottompos = h.lastbottompos + 20;
                    h = equation(Fig, h,strcat('\mathrm{M_{0}}\ +\ \mathbf{', h.S.nrX_2,'}\ \cdot \mathrm{', h.S.M_2,'}\ =\ \mathrm{M}'));
                end
            elseif strcmp(sort,'distributedload')
                h = write(Fig, h,'text', 'Distributed load:', ' ', true);
                if j == 1
                    h.lastbottompos = h.lastbottompos + 20;
                    h = equation(Fig, h,strcat('\mathrm{M_{0}}\ +\ \mathbf{', h.S.nrX_1,'}\ \cdot \mathrm{', h.S.M_1,'}\ =\ \mathrm{M}'));
                else
                    h.lastbottompos = h.lastbottompos + 20;
                    h = equation(Fig, h,strcat('\mathrm{M_{0}}\ +\ \mathbf{', h.S.nrX_2,'}\ \cdot \mathrm{', h.S.M_2,'}\ =\ \mathrm{M}'));
                end
            elseif strcmp(sort,'moment')
                h = write(Fig, h,'text', 'Moment:', ' ', true);
                if j == 1
                    h.lastbottompos = h.lastbottompos + 20;
                    h = equation(Fig, h,strcat('\mathrm{M_{0}}\ +\ \mathbf{', h.S.nrX_1,'}\ \cdot \mathrm{', h.S.M_1,'}\ =\ \mathrm{M}'));
                else
                    h.lastbottompos = h.lastbottompos + 20;
                    h = equation(Fig, h,strcat('\mathrm{M_{0}}\ +\ \mathbf{', h.S.nrX_2,'}\ \cdot \mathrm{', h.S.M_2,'}\ =\ \mathrm{M}'));
                end
            end
            
            h.number = h.number + 1;
            h.name = strcat('firstintgraph',num2str(h.number));
            h.obj.(h.name) = copyobj(h.obj.(strcat(sort,num2str(i))), Fig);
            h.obj.(h.name).Position = [h.width/2-distbetween-3/2*width, h.lastbottompos-height0-distfromprev, width, height0];
            h.obj.(h.name).Parent = h.(h.panel);
            ylim1 = h.obj.(h.name).YLim;
            
            M(x) = h.obj.(strcat(sort,num2str(i))).Children(numel(h.obj.(strcat(sort,num2str(i))).Children)).Function;
            
            text(h.obj.(h.name), l + 1/4*l, 0, {'+'},'Interpreter','latex','HorizontalAlignment','right', 'FontSize', 16, 'FontWeight', 'bold');
            
            fields = fieldnames(h.obj);
            
            if h.obj.(h.name).Position(2) < height0+10
                    set(h.(h.panel).Children,'Visible','Off');
                    set(h.(h.panel),'Visible','Off');
                    h.panelnr = h.panelnr + 1;
                    h.newpanelnr = h.panelnr;
                    h.panel = strcat('panel',num2str(h.panelnr));
                    h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]);
                    h.lastnronpage = h.number - 1;
                    h.obj.(h.name).Parent = h.(h.panel);
                    if strcmp(sort, 'pointload') || strcmp(sort, 'distributedload') || strcmp(sort, 'moment')
                        if contains(char(fields(h.number-3)), 'text')
                            h.obj.(char(fields(h.number-3))).Parent = h.(h.panel);
                            h.obj.(char(fields(h.number-3))).Position(2) = h.height - 10 - h.obj.(char(fields(h.number-3))).Position(4);
                            set(h.obj.(char(fields(h.number-3))), 'Visible', 'On');
                            h.obj.(char(fields(h.number-2))).Position(2) = h.obj.(char(fields(h.number-3))).Position(2) - 10 - h.obj.(char(fields(h.number-2))).Position(4);
                        else
                            h.obj.(char(fields(h.number-2))).Position(2) = h.height - 10 - h.obj.(char(fields(h.number-2))).Position(4);
                        end
                        h.obj.(char(fields(h.number-2))).Parent = h.(h.panel);
                        set(h.obj.(char(fields(h.number-2))), 'Visible', 'On');
                        h.obj.(char(fields(h.number-1))).Parent = h.(h.panel);
                        h.obj.(char(fields(h.number-1))).Position(2) = h.obj.(char(fields(h.number-2))).Position(2) - 10 - h.obj.(char(fields(h.number-1))).Position(4);
                        set(h.obj.(char(fields(h.number-1))), 'Visible', 'On');
                        h.lastbottompos = h.obj.(char(fields(h.number-1))).Position(2);
                    end    
                    h.obj.(h.name).Position(2) = h.lastbottompos-height0-distfromprev;
            end
            pos1 = h.obj.(h.name).Position(2);
            set(h.obj.(h.name), 'Visible', 'Off');
            
            % GRAPH WITH THE UNIT FORCE
            h.number = h.number + 1;
            h.name = strcat('secondintgraph',num2str(h.number));
            h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels');
            h.obj.(h.name).Position = [h.width/2-width/2, pos1, width, height0];
            set(h.obj.(h.name),'xtick',[]);
            set(h.obj.(h.name),'ytick',[]);
            set(h.obj.(h.name),'xcolor',[1 1 1]);
            set(h.obj.(h.name),'ycolor',[1 1 1]);
            hold on
            if j == 1
                M1(x) = h.S.M1X1;
            else
                M1(x) = h.S.M2X2;
            end
            h.secondintgraph = fplot(h.obj.(h.name),-M1(x),[0,l],'color','blue','Linewidth',2);
            fplot(h.obj.(h.name), 0, [0,l],'-k'); %line at x=0 to distinguish
            if double(M1(0)) ~= 0
                plot(h.obj.(h.name), [0,0],[0,-double(M1(0))],'color','blue','Linewidth',2); 
                if double(M1(0)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                text(h.obj.(h.name), 0, -M1(0), {' ',['$' checkfrac(M1(0)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment',alignment);
            end
            if double(M1(l)) ~= 0
                plot(h.obj.(h.name), [l,l],[-double(M1(l)),0],'color','blue','Linewidth',2); 
                if double(M1(l)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                text(h.obj.(h.name), l, -M1(l), {' ',['$' checkfrac(M1(l)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment',alignment);        
            end
            
            if j == 1 && M1(0) ~= 0 && h.S.sh == -1
                if double(X1) > 0
                    h.obj.(h.name).YLim = ([0 double(X1)*2]);
                else
                    h.obj.(h.name).YLim = ([double(X1)*2 0]);
                end
            elseif h.S.sh == -1
                if double(X1) < 0
                    h.obj.(h.name).YLim = ([0 -double(X1)*2]);
                else
                    h.obj.(h.name).YLim = ([-double(X1)*2 0]);                    
                end
            end
            
            %graph connection at 0 and L
            if double(M1(0)) ~= 0, plot(h.obj.(h.name), [0,0],[0,-double(M1(0))],'color','blue','Linewidth',2); end
            if double(M1(l)) ~= 0, plot(h.obj.(h.name), [l,l],[-double(M1(l)),0],'color','blue','Linewidth',2); end
            
            ylim2 = h.obj.(h.name).YLim;
            text(h.obj.(h.name), l + 1/4*l, 0, {'='},'Interpreter','latex','HorizontalAlignment','right', 'FontSize', 16, 'FontWeight', 'bold');
            set(h.obj.(h.name), 'Visible', 'Off');
            
            % GRAPH WITH THE RESULTANT MOMENT LINE
            h.number = 1 + h.number;
            h.name = strcat('resultintgraph',num2str(h.number));
            h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels');
            h.obj.(h.name).Position = [h.width/2+width/2+distbetween, pos1, width, height0];
            set(h.obj.(h.name),'xtick',[]);
            set(h.obj.(h.name),'ytick',[]);
            set(h.obj.(h.name),'xcolor',[1 1 1]);
            set(h.obj.(h.name),'ycolor',[1 1 1]);
            hold on
            if strcmp(sort,'pointload')
                if j == 1, Msup(x) = h.S.diagrams01.pl(pl); 
                else Msup(x) = h.S.diagrams02.pl(pl); end
            elseif strcmp(sort,'distributedload')
                if j == 1, Msup(x) = h.S.diagrams01.dl; 
                else Msup(x) = h.S.diagrams02.dl; end
            else
                if j == 1, Msup(x) = h.S.diagrams01.m; 
                else, Msup(x) = h.S.diagrams02.m; end
            end
            
            h.resultintgraph = fplot(h.obj.(h.name),-Msup(x),[0,l],'color',colorM,'Linewidth',2);   
            
            %graph connection at 0 and L
            if double(Msup(0)) ~= 0, plot(h.obj.(h.name), [0,0],[0,-double(Msup(0))],'color',colorM,'Linewidth',2); end
            if double(Msup(l)) ~= 0, plot(h.obj.(h.name), [l,l],[-double(Msup(l)),0],'color',colorM,'Linewidth',2); end
            
            %Value at the point loads
            if strcmp(sort,'pointload')
                if h.S.spl(pl) ~= 0  && h.S.spl(pl) ~= l && double(Msup(h.S.spl(pl))) ~= 0
                    if double(Msup(h.S.spl(pl))) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                    text(h.obj.(h.name), h.S.spl(pl), -double(Msup(h.S.spl(pl))), {['$' checkfrac(double(Msup(h.S.spl(pl)))) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);
                end
                
            elseif strcmp(sort,'distributedload')
                %Value at a
                if h.S.a ~= 0
                    if double(Msup(h.S.a)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                    text(h.obj.(h.name), h.S.a, -Msup(h.S.a), {['$' checkfrac(Msup(h.S.a)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','right','VerticalAlignment',alignment);
                end

                %Value at b
                if h.S.b ~= 0 && h.S.b ~= l
                    if double(Msup(h.S.b)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                    text(h.obj.(h.name), h.S.b, -Msup(h.S.b), {['$' checkfrac(Msup(h.S.b)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment',alignment);
                end
                                
                %values at continuous maxima
                xmaxmin = double(solve(diff(Msup(x)) == 0));
                for k = 1 : numel(xmaxmin)
                    dd(x) = diff(diff(Msup));
                    sign = dd(xmaxmin(k));
                    if xmaxmin(k) ~= 0   && xmaxmin(k) ~= l && Msup(xmaxmin(k)) ~= 0
                        if sign >= 0, alignment = 'bottom'; else, alignment = 'top'; end
                                text(h.obj.(h.name), xmaxmin(k), -Msup(xmaxmin(k)), {' ',['$' checkfrac(Msup(xmaxmin(k))) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment',alignment);
                    end
                end
                
            else
                %Value at sm
                if h.S.sm ~= 0 && h.S.sm ~= l
                        if double(Msup(h.S.sm)) < 0
                            text(h.obj.(h.name), h.S.sm, -Msup(h.S.sm) - h.S.Mvalue, {' ',['$' checkfrac(Msup(h.S.sm) + h.S.Mvalue) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','top');
                            text(h.obj.(h.name), h.S.sm, -Msup(h.S.sm), {' ',['$' checkfrac(-Msup(h.S.sm)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','bottom');        
                        else
                            text(h.obj.(h.name), h.S.sm, -Msup(h.S.sm), {' ',['$' checkfrac(Msup(h.S.sm)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','top');                
                            text(h.obj.(h.name), h.S.sm, -Msup(h.S.sm) - h.S.Mvalue, {' ',['$' checkfrac(Msup(h.S.sm) + h.S.Mvalue) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','bottom');                
                        end
                end
            end
            
            %values at 0 and L
            if double(Msup(0)) ~= 0
                if double(Msup(0)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                    text(h.obj.(h.name), 0, -Msup(0), {['$' checkfrac(Msup(0)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment',alignment);
            end
            if double(Msup(l)) ~= 0
                if double(Msup(l)) < 0, alignment = 'bottom'; else, alignment = 'top'; end
                    text(h.obj.(h.name), l, -Msup(l), {['$' checkfrac(Msup(l)) '\ ' h.unitM '$']},'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment',alignment);
            end

            fplot(h.obj.(h.name), 0, [0,l],'-k'); %line at x=0 to distinguish
            ylim3 = h.obj.(h.name).YLim;
            
            %aligne all plots
            norm1 = h.obj.(h.name).Position(4) / (ylim1(2) - ylim1(1));
            norm2 = h.obj.(h.name).Position(4) / (ylim2(2) - ylim2(1));
            norm3 = h.obj.(h.name).Position(4) / (ylim3(2) - ylim3(1));
            
            pos = [norm1*ylim1(2); norm2*ylim2(2); norm3*ylim3(2)];
            posmax = max(pos);
            posmax = pos(pos == posmax);
            whichgraph = find(pos==posmax(1),1,'last');
            
            if whichgraph == 1
                h.obj.(h.name).Position(2) = h.obj.(strcat('firstintgraph',num2str(h.number-2))).Position(2) - (norm1*ylim1(1) - norm3*ylim3(1));
                h.obj.(strcat('secondintgraph',num2str(h.number-1))).Position(2) = h.obj.(strcat('firstintgraph',num2str(h.number-2))).Position(2) - (norm1*ylim1(1) - norm2*ylim2(1));
            elseif whichgraph == 2
                h.obj.(strcat('firstintgraph',num2str(h.number-2))).Position(2) = h.obj.(strcat('secondintgraph',num2str(h.number-1))).Position(2) - (norm2*ylim2(1) - norm1*ylim1(1));
                h.obj.(h.name).Position(2) = h.obj.(strcat('secondintgraph',num2str(h.number-1))).Position(2) - (norm2*ylim2(1) - norm3*ylim3(1));
            else %if whichgraph == 3
                h.obj.(strcat('firstintgraph',num2str(h.number-2))).Position(2) = h.obj.(h.name).Position(2) - (norm3*ylim3(1) - norm1*ylim1(1));
                h.obj.(strcat('secondintgraph',num2str(h.number-1))).Position(2) = h.obj.(h.name).Position(2) - (norm3*ylim3(1) - norm2*ylim2(1));
            end
            
            pos = [h.obj.(strcat('firstintgraph',num2str(h.number-2))).Position(2); h.obj.(strcat('secondintgraph',num2str(h.number-1))).Position(2); h.obj.(h.name).Position(2)];
            posmin = min(pos);
            posmin = pos(pos == posmin);
            whichgraph = find(pos==posmin(1),1,'last');
            
            if whichgraph == 1
                h.lastbottompos = h.obj.(strcat('firstintgraph',num2str(h.number-2))).Position(2) - distfromnext;
            elseif whichgraph == 2
                h.lastbottompos = h.obj.(strcat('secondintgraph',num2str(h.number-1))).Position(2) - distfromnext;
            else                    
                h.lastbottompos = h.obj.(h.name).Position(2) - distfromnext;
            end
            
            set(h.obj.(strcat('firstintgraph',num2str(h.number-2))), 'Visible', 'On');
            set(h.obj.(strcat('secondintgraph',num2str(h.number-1))), 'Visible', 'On');
            set(h.obj.(h.name), 'Visible', 'On');
            
            if h.interactive
                pause(h.pause);
            else
                pause(0.2);
            end
        end
        
        if load == total
            if h.unitforcenr == 2
                load = 0;
            end
            break;
        end
    end
end
end
end
        