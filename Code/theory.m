function h = theory(Fig, h, array)
%Insert an array with elements:
%[{'This is a title...'} {'title'} {1};
%{'This is a text...'} {'text'} {1};
%{'$1+2=3$'} {'equation'} {1};
%{'texttexttext'} {'text'} {1};
%{'figurename'} {'image'} {[height]};
%etc.]

if h.theory
    if not(isfield(h,'recursivetheory'))
        h.recursivetheory = false;
        set(0, 'currentfigure', Fig);
    end
    
    
if h.lastbottompos < 300
    set(h.(h.panel).Children,'Visible','Off');
    set(h.(h.panel),'Visible','Off');
    h.panelnr = h.panelnr + 1;
    h.panel = strcat('panel',num2str(h.panelnr));
    h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]);%
    h.lastnronpage = h.number - 1;
    h.lastbottompos = h.height - 10;
end

changepage = false;
lineheight = 18;
eqnheight = 30;
bgcolor = [0.9216    0.9686    1.0000];
maxlength = floor(5/134*h.screendim(3) + 1520/67);
h.number = 1 + h.number;
h.name = strcat('theory',num2str(h.number));
height = 0;
width = h.width - 40;
% numberofchar = numel(str);
% numberoftexts = ceil(numberofchar/maxlength);
% numberoftexts_def = 1;
% startpos = 1;
% partpos = 0;
% dollar = 0;
% for i = 1 : numberoftexts
%     if numel(str) > startpos + maxlength-1, stop = startpos + maxlength-1;
%     else stop = numel(str);
%     end
%     foundnew = false;
%     for j = startpos : stop
%         if strcmp(str(j),'$') && dollar == 0,dollar = 1;firstdollar = j;
%         elseif strcmp(str(j),'$') && dollar == 1, dollar = 0;firstdollar = 0; end
%     end
%     for j = startpos + maxlength : numel(str)
%         foundnew = false;
%         if strcmp(str(j),'$') && dollar == 0,dollar = 1;firstdollar = j;
%         elseif strcmp(str(j),'$') && dollar == 1, dollar = 0; end
%         if (strcmp(str(j),' ') || strcmp(str(j),'-') || j == numberofchar)  && dollar == 0
%         partpos = j;
%         if partpos - startpos > 100 && firstdollar ~= 0
%             partpos = firstdollar - 1;
%         end
%         foundnew = true;
%         firstdollar = 0;
%         break;
%     end
%     end
%     if not(isempty(str(startpos : partpos))) && foundnew
%         if partpos == numberofchar, break; end
%         numberoftexts_def = numberoftexts_def + 1;
%         startpos = partpos + 1;
%     else
%         if numberoftexts_def == 0
%             numberoftexts_def = 1; 
%         end
%     end
%  end
% height = lineheight*(numberoftexts_def + 2);
% distfromprev = 10;
% distfromnext = 10;
% h.obj.(h.name).Position = [h.width/2-width/2, h.lastbottompos - distfromprev - height, width, height];
% h.obj.(h.name).XLim = [0 1];
% h.obj.(h.name).YLim = [0 numberoftexts_def + 2];

% t = text(0.05, numberoftexts_def +1,char(array(1,1)),'Interpreter','tex','HorizontalAlignment','left','VerticalAlignment','middle','FontSize',10,'BackgroundColor','none', 'FontWeight', 'bold');
% if numberoftexts_def > 1
% startpos = 1;
% for i = 2 : numberoftexts + 1
%     if numel(str) > startpos + maxlength-1, stop = startpos + maxlength-1;
%     else stop = numel(str); partpos = stop;
%     end
%     for j = startpos : stop
%         if strcmp(str(j),'$') && dollar == 0,dollar = 1;firstdollar = j;
%         elseif strcmp(str(j),'$') && dollar == 1, dollar = 0;firstdollar = 0; end
%     end
%     for j = startpos + maxlength : numel(str)
%         if strcmp(str(j),'$') && dollar == 0,dollar = 1;firstdollar = j;
%         elseif strcmp(str(j),'$') && dollar == 1, dollar = 0; end
%     if (strcmp(str(j),' ') || strcmp(str(j),'-') || j == numberofchar) && dollar == 0
%         partpos = j;
%         if partpos - startpos > 100 && firstdollar ~= 0
%             partpos = firstdollar - 1;
%         end
%         firstdollar = 0;
%         break;
%     end
%     end
%     if not(isempty(str(startpos : partpos)))
%         text(0.05, numberoftexts_def + 1.7 - i,str(startpos : partpos),'Interpreter','tex','HorizontalAlignment','left','VerticalAlignment','middle','FontSize',9,'BackgroundColor','none');
%         startpos = partpos + 1;
%     end
%  end
%  else
%         text(0.05, .5, str,'Interpreter','tex','HorizontalAlignment','left','VerticalAlignment','middle','FontSize',9,'BackgroundColor','none');
% end

% Display the next elements
    distfromprev = 10;
    distfromnext = 10;
for k = 1 : numel(array(:,1))
    if isfield(h.obj,h.name)
    toplim = h.obj.(h.name).YLim(1);
    end
    
    %Element is an equation
    if strcmp(char(array(k,2)),'equation')
        if h.recursivetheory
            newheight = height + 1.5*lineheight;
            height = newheight;
            h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels','Color',bgcolor,'YLim',[0,0.5],'XLim',[0,1]);
            hold on
            toplim = h.obj.(h.name).YLim(1);
            h.obj.(h.name).Position = [h.width/2-width/2, h.lastbottompos - distfromprev - height, width, height];
            h.obj.(h.name).XLim = [0 1];
            h.obj.(h.name).YLim(1) = h.obj.(h.name).YLim(1) - 1.5;
            changepage = false;
            h.recursivetheory = false;
        else
            h.obj.(h.name).Position(2) = h.obj.(h.name).Position(2) - eqnheight;
            h.obj.(h.name).Position(4) = h.obj.(h.name).Position(4) + eqnheight;
            h.obj.(h.name).YLim(1) = toplim-eqnheight/lineheight;
            newheight = height + eqnheight;
        end
    if h.lastbottompos - distfromprev - newheight > 10 || h.recursivetheory
        height = newheight;
        if not(isfield(h.obj,h.name))
            h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels','Color',bgcolor,'YLim',[0,0.5],'XLim',[0,1]);
            hold on
            toplim = h.obj.(h.name).YLim(1);
        end
    text(0.5,toplim-0.5*eqnheight/lineheight , char(array(k,1)),'Interpreter','latex','HorizontalAlignment','center','VerticalAlignment','middle','FontSize',10,'BackgroundColor','none');
    changepage = false;
    h.recursivetheory = false;
    else
        changepage = true;
        arraynumber = k;
        break;
    end
    %Element is a text
    elseif strcmp(char(array(k,2)),'text')
    str2 = char(array(k,1));
    numberofchar = numel(str2);
    if count(str2, '\Rightarrow'), numberofchar = numberofchar - 9*count(str2, '\Rightarrow'); end
    if count(str2, '\Leftarrow'), numberofchar = numberofchar - 9*count(str2, '\Leftarrow'); end
    if count(str2, '_{'), numberofchar = numberofchar - 4*count(str2, '_{'); end
    if count(str2, '\bf'), numberofchar = numberofchar - 2*count(str2, '\bf'); end
    if count(str2, '\rm'), numberofchar = numberofchar - 3*count(str2, '\rm'); end
    if count(str2, '\sum'), numberofchar = numberofchar - 3*count(str2, '\sum'); end
    numberoftexts = ceil(numberofchar/maxlength);
    numberoftexts_def = 1;
    startpos = 1;
    partpos = 0;
    dollar = 0;
    firstdollar = 0;
    for i = 1 : numberoftexts
        if numel(str2) > startpos + maxlength-1, stop = startpos + maxlength-1;
        else stop = numel(str2);
        end
        foundnew = false;
        for j = startpos : stop
            if strcmp(str2(j),'$') && dollar == 0,dollar = 1;firstdollar = j;
            elseif strcmp(str2(j),'$') && dollar == 1, dollar = 0;firstdollar = 0; end
        end
        for j = startpos + maxlength : numel(str2)
            foundnew = false;
            if strcmp(str2(j),'$') && dollar == 0,dollar = 1;firstdollar = j;
            elseif strcmp(str2(j),'$') && dollar == 1, dollar = 0; end
            if (strcmp(str2(j),' ') || strcmp(str2(j),'-') || j == numberofchar)  && dollar == 0
            partpos = j;
            if partpos - startpos > 100 && firstdollar ~= 0
                partpos = firstdollar - 1;
            end
            foundnew = true;
            firstdollar = 0;
            break;
            end
        end
        if not(isempty(str2(startpos : partpos))) && foundnew
            if partpos == numberofchar, break; end
            numberoftexts_def = numberoftexts_def + 1;
            startpos = partpos + 1;
        else
            if numberoftexts_def == 0
                numberoftexts_def = 1; 
            end
        end
    end
    
    newheight = height + lineheight*(numberoftexts_def);
    if (h.lastbottompos - distfromprev - newheight > 10 && ~(count(char(array(k,1)),'\bf') == 1 && count(char(array(k,1)),'\rm') == 0)) ||...
            (h.lastbottompos - distfromprev - newheight > 80 && count(char(array(k,1)),'\bf') == 1 && count(char(array(k,1)),'\rm') == 0) || h.recursivetheory
    if not(isfield(h.obj,h.name))
        h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels','Color',bgcolor,'YLim',[0,0.5],'XLim',[0,1]);
        hold on
        toplim = h.obj.(h.name).YLim(1);
    end
    height = newheight;
    h.obj.(h.name).Position = [h.width/2-width/2, h.lastbottompos - distfromprev - height, width, height];
    h.obj.(h.name).XLim = [0 1];
    h.obj.(h.name).YLim(1) = h.obj.(h.name).YLim(1) - numberoftexts_def;
    changepage = false;

    if numberoftexts_def > 1
    startpos = 1;
    for i = 1 : numberoftexts
        if numel(str2) > startpos + maxlength-1, stop = startpos + maxlength-1;
        else stop = numel(str2); partpos = stop;
        end
        for j = startpos : stop
            if strcmp(str2(j),'$') && dollar == 0,dollar = 1;firstdollar = j;
            elseif strcmp(str2(j),'$') && dollar == 1, dollar = 0;firstdollar = 0; end
        end
        for j = startpos + maxlength : numel(str2)
            if strcmp(str2(j),'$') && dollar == 0,dollar = 1;firstdollar = j;
            elseif strcmp(str2(j),'$') && dollar == 1, dollar = 0; end
        if (strcmp(str2(j),' ') || strcmp(str2(j),'-') || j == numberofchar) && dollar == 0
            partpos = j;
            if partpos - startpos > 100 && firstdollar ~= 0
                partpos = firstdollar - 1;
            end
            firstdollar = 0;
            break;
        end
        end
        if not(isempty(str2(startpos : partpos)))
            text(0.05, toplim - 0.5 - i + 1,str2(startpos : partpos),'Interpreter','tex','HorizontalAlignment','left','VerticalAlignment','middle','FontSize',9,'BackgroundColor','none');
            startpos = partpos + 1;
        end
    end
    else
        text(0.05, toplim - 0.5, str2,'Interpreter','tex','HorizontalAlignment','left','VerticalAlignment','middle','FontSize',9,'BackgroundColor','none');
    end
    h.recursivetheory = false;
    else
        changepage = true;
        arraynumber = k;
        break;
    end
    %Element is an image
    elseif strcmp(char(array(k,2)),'image')
        image_data = imread(char(array(k,1)));
        im_h = numel(image_data(:,1,1));
        im_w = numel(image_data(1,:,1));
        aspect_ratio = im_h/im_w;
        setheight = cell2mat(array(k,3));
        setheightpix = setheight*lineheight;
        setwidthpix = setheightpix/aspect_ratio;
        setwidth = setwidthpix/width;
        newheight = height + setheightpix + 2*lineheight;
        if h.lastbottompos - distfromprev - newheight > 10 || h.recursivetheory
        if not(isfield(h.obj,h.name))
            h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels','Color',bgcolor,'YLim',[0,0.5],'XLim',[0,1]);
            hold on
            toplim = h.obj.(h.name).YLim(1);
        end
            height = newheight;
        h.obj.(h.name).Position = [h.width/2-width/2, h.lastbottompos - distfromprev - height, width, height];
        h.obj.(h.name).XLim = [0 1];
        h.obj.(h.name).YLim(1) = h.obj.(h.name).YLim(1) - setheight - 2;
        imagesc([0.5-setwidth/2 0.5+setwidth/2],[toplim - 1, toplim-setheight-1],image_data);
        changepage = false;
        h.recursivetheory = false;
        else
            changepage = true;
            arraynumber = k;
            break;
        end
    %Element is a title
    elseif strcmp(char(array(k,2)),'title')
        newheight = height + 1.5*lineheight;
        if h.lastbottompos - distfromprev - newheight > 150 || h.recursivetheory 
        height = newheight;
        if not(isfield(h.obj,h.name))
            h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels','Color',bgcolor,'YLim',[0,0.5],'XLim',[0,1]);
            hold on
            toplim = h.obj.(h.name).YLim(1);
        end
        text(0.05,toplim-0.5,char(array(1,1)),'Interpreter','tex','HorizontalAlignment','left','VerticalAlignment','middle','FontSize',10,'BackgroundColor','none', 'FontWeight', 'bold');
        h.obj.(h.name).Position = [h.width/2-width/2, h.lastbottompos - distfromprev - height, width, height];
        h.obj.(h.name).XLim = [0 1];
        h.obj.(h.name).YLim(1) = h.obj.(h.name).YLim(1) - 1.5;
        changepage = false;
        h.recursivetheory = false;
        else
            changepage = true;
            arraynumber = k;
            break;
        end
    end
    if k == numel(array(:,1))
        height = height + lineheight/2;
        h.obj.(h.name).Position = [h.width/2-width/2, h.lastbottompos - distfromprev - height, width, height];
        h.obj.(h.name).XLim = [0 1];
        h.obj.(h.name).YLim(1) = h.obj.(h.name).YLim(1) - 1/2;
    end 
end
    
h.recursivetheory = changepage;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
if isfield(h.obj,h.name)
    set(h.obj.(h.name),'xtick',[]);
    set(h.obj.(h.name),'ytick',[]);
    set(h.obj.(h.name),'xcolor',bgcolor);
    set(h.obj.(h.name),'ycolor',bgcolor);

    h.lastbottompos = h.obj.(h.name).Position(2) - distfromnext;

    if h.interactive
        pause(2);
    else
        pause(0.2);
    end
end

if changepage
    if arraynumber == 1
        h.number = h.number-1;
    end
    
    set(h.(h.panel).Children,'Visible','Off');
    set(h.(h.panel),'Visible','Off');
    h.panelnr = h.panelnr + 1;
    h.panel = strcat('panel',num2str(h.panelnr));
    h.(h.panel) = uipanel('units','pixels','Position',[0 0 h.width+2 h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]);%
    h.lastnronpage = h.number - 1;
    h.lastbottompos = h.height - 10;
        
    h = theory(Fig, h,array(arraynumber : numel(array(:,1)),1:3));
    h.recursivetheory = false;
end
end
end
