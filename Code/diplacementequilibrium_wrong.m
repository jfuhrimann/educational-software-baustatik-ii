function h = diplacementequilibrium_wrong(h,equation,var,where)
%This function generates the multiple choice dialog with wrong beam end
%moments.
h.graficpanel.Position(4) = h.height - 200;
heightgrafic = h.graficpanel.Position(4)/7;
widthgrafic = 180;
left = h.graficpanel.Position(3)/2-widthgrafic/2;
height = h.graficpanel.Position(4);
set(h.questionpanel.Children,'Visible','Off');          
a = [1, 2, 3];
a = a(randperm(length(a)));
pos1 = height-heightgrafic*a(1)-20*a(1)-90;
pos2 = height-heightgrafic*a(2)-20*a(2)-90;
pos3 = height-heightgrafic*a(3)-20*a(3)-90;
h.multiplechoice.txt = axes('Parent', h.graficpanel,'units','pixels','Position',[20 height-35 300 20],'Color',h.graficpanel.BackgroundColor,'XLim',[0 1],'YLim',[0,1],'xtick',[],'ytick',[],'xcolor','w','ycolor','w');
h.multiplechoice.text.str = text(0,0,strcat('Displacement equilibrium for $', latexpsi(char(string(var))),'$'),'interpreter','latex','VerticalAlignment','bottom');

h.multiplechoice1 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos1+heightgrafic-10, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);
h.multiplechoice2 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos2+heightgrafic-10, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);
h.multiplechoice3 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos3+heightgrafic-10, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);

%Labelling the multiplechoicebuttons
h.multiplechoice.grafic1 = axes('XLim',[0 1],'YLim',[0 1],'Color',h.graficpanel.BackgroundColor);
h.multiplechoice.grafic1.Parent = h.graficpanel;
h.multiplechoice.grafic1.Units = 'pixels';
h.multiplechoice.grafic1.Position = [left, pos1+heightgrafic/2, widthgrafic, heightgrafic];
set(h.multiplechoice.grafic1,'xtick',[]);
set(h.multiplechoice.grafic1,'ytick',[]);
set(h.multiplechoice.grafic1,'xcolor',h.graficpanel.BackgroundColor);
set(h.multiplechoice.grafic1,'ycolor',h.graficpanel.BackgroundColor);
text(0,.5,strcat('Equation $(',num2str(a(1)),')$'),'Parent',h.multiplechoice.grafic1,'Interpreter','Latex');

h.multiplechoice.grafic2 = axes('XLim',[0 1],'YLim',[0 1],'Color',h.graficpanel.BackgroundColor);
h.multiplechoice.grafic2.Parent = h.graficpanel;
h.multiplechoice.grafic2.Units = 'pixels';
h.multiplechoice.grafic2.Position = [left, pos2+heightgrafic/2, widthgrafic, heightgrafic];
set(h.multiplechoice.grafic2,'xtick',[]);
set(h.multiplechoice.grafic2,'ytick',[]);
set(h.multiplechoice.grafic2,'xcolor',h.graficpanel.BackgroundColor);
set(h.multiplechoice.grafic2,'ycolor',h.graficpanel.BackgroundColor);
text(0,.5,strcat('Equation $(',num2str(a(2)),')$'),'Parent',h.multiplechoice.grafic2,'Interpreter','Latex');

h.multiplechoice.grafic3 = axes('XLim',[0 1],'YLim',[0 1],'Color',h.graficpanel.BackgroundColor);
h.multiplechoice.grafic3.Parent = h.graficpanel;
h.multiplechoice.grafic3.Units = 'pixels';
h.multiplechoice.grafic3.Position = [left, pos3+heightgrafic/2, widthgrafic, heightgrafic];
set(h.multiplechoice.grafic3,'xtick',[]);
set(h.multiplechoice.grafic3,'ytick',[]);
set(h.multiplechoice.grafic3,'xcolor',h.graficpanel.BackgroundColor);
set(h.multiplechoice.grafic3,'ycolor',h.graficpanel.BackgroundColor);
text(0,.5,strcat('Equation $(',num2str(a(3)),')$'),'Parent',h.multiplechoice.grafic3,'Interpreter','Latex');

%Generating a panel to display the equations:
%How many line splits in the equation?
str = latex_equationconverter(equation);
numberofchar = numel(str);
numberoftexts = ceil(numberofchar/100);
    numberoftexts_def = 1;
    startpos = 1;
    partpos = 0;
    rounded = 0;
    curly = 0;
    for i = 1 : numberoftexts
        if numel(str) > startpos + 99, stop = startpos + 99;
        else stop = numel(str);
        end
        foundnew = false;
        for j = startpos : stop
            if strcmp(str(j),'{'),curly = curly + 1;
            elseif strcmp(str(j),'('),rounded = rounded + 1;
            elseif strcmp(str(j),'}'),curly = curly - 1;
            elseif strcmp(str(j),')'),rounded = rounded - 1;
            end
        end
        for j = startpos + 100 : numel(str)
            foundnew = false;
            if strcmp(str(j),'{'),curly = curly + 1;
            elseif strcmp(str(j),'('),rounded = rounded + 1;
            elseif strcmp(str(j),'}'),curly = curly - 1;
            elseif strcmp(str(j),')'),rounded = rounded - 1;
            end
        if (strcmp(str(j),'+') || strcmp(str(j),'-') || j == numberofchar)  && rounded == 0 && curly == 0
            partpos = j;
            foundnew = true;
            break;
        end
        end
        if not(isempty(str(startpos : partpos))) && foundnew
            if partpos == numberofchar, break; end
            numberoftexts_def = numberoftexts_def + 1;
            startpos = partpos + 1;
        else
            if numberoftexts_def == 0
                numberoftexts_def = 1; 
            end
        end
    end
    
if numberoftexts_def == 1
    eqnheight = 300;
else
    eqnheight = 3*60*(numberoftexts_def);
end

% eqnheight = 300;
if h.lastbottompos < eqnheight + 40
    eqnpos = [20,40,h.width-40,eqnheight];
else
    eqnpos = [20,h.lastbottompos-eqnheight,h.width-40,eqnheight];
end
h.equationpanel = uipanel('Parent',h.(h.panel),'Visible','Off','units','pixels','Position',eqnpos,'BackgroundColor',[0.9882    0.9922    1.0000]); %[ 0.9216    0.9686    1.0000]); %
eqsingleheight = 60;

for i = 1 : numberoftexts_def
poseq1(i) = eqnheight - 40 - eqsingleheight*((numberoftexts_def*a(1)-numberoftexts_def)+(i-1)+1);
poseq2(i) = eqnheight - 40 - eqsingleheight*((numberoftexts_def*a(2)-numberoftexts_def)+(i-1)+1);
poseq3(i) = eqnheight - 40 - eqsingleheight*((numberoftexts_def*a(3)-numberoftexts_def)+(i-1)+1);
end

eqns = [wrong_equation_generator(equation);equation];

for i = 1 : numberoftexts_def
axes1(i) = axes('Parent',h.equationpanel,'units','pixels','Position',[10,poseq1(i),h.width-60,eqsingleheight],'XLim',[0 1],'YLim',[0 1],'xtick',[],'ytick',[],'xcolor','w','ycolor','w','color',h.graficpanel.BackgroundColor);
axes2(i) = axes('Parent',h.equationpanel,'units','pixels','Position',[10,poseq2(i),h.width-60,eqsingleheight],'XLim',[0 1],'YLim',[0 1],'xtick',[],'ytick',[],'xcolor','w','ycolor','w','color',h.graficpanel.BackgroundColor);
axes3(i) = axes('Parent',h.equationpanel,'units','pixels','Position',[10,poseq3(i),h.width-60,eqsingleheight],'XLim',[0 1],'YLim',[0 1],'xtick',[],'ytick',[],'xcolor','w','ycolor','w','color',h.graficpanel.BackgroundColor);
end
h.mc.help1 = [];
h.mc.help2 = [];
for i = 1 : 3
    str = latex_equationconverter(eqns(i));
    if numberoftexts_def > 1
    startpos = 1;
    curly = 0;
    rounded = 0;
    for k = 1 : numberoftexts
        if numel(str) > startpos + 79, stop = startpos + 79;
        else stop = numel(str); partpos = stop;
        end
        for j = startpos : stop
            if strcmp(str(j),'{'),curly = curly + 1;
            elseif strcmp(str(j),'('),rounded = rounded + 1;
            elseif strcmp(str(j),'}'),curly = curly - 1;
            elseif strcmp(str(j),')'),rounded = rounded - 1;
            end
        end
        for j = startpos + 80 : numel(str)
        if strcmp(str(j),'{'),curly = curly + 1;
        elseif strcmp(str(j),'('),rounded = rounded + 1;
        elseif strcmp(str(j),'}'),curly = curly - 1;
        elseif strcmp(str(j),')'),rounded = rounded - 1;
        end
        if (strcmp(str(j),'+') || strcmp(str(j),'-') || j == numberofchar) && rounded == 0 && curly == 0
            partpos = j;
            break;
        end
        end
        if not(isempty(str(startpos : partpos)))
            dispstr(k) = {str(startpos : partpos)};
            startpos = partpos + 1;
        end
    end
    else
        dispstr = {str};
    end
    for j = 1 : numberoftexts_def
    currentax = eval(strcat('axes',num2str(i)));
    currentax = currentax(j);
    if j == 1
        currentstr = strcat('$(',num2str(a(i)),')\qquad \delta W_{',where,'} = \delta W_{',where,'}+',char(dispstr(j)),'$');
    else
        currentstr = strcat('$\qquad\qquad{}',char(dispstr(j)),'$');
    end
    text(0.001, 0.5, currentstr,'Parent',currentax,'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','middle','FontSize',11);
    end
end
end