% function result = cdial_init(handles)
%Initialises the dial-window for the constraints
% axes(handles.axes2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure('units','characters')
handles.axes2 = axes('Units','characters','Position',[0 0 60 6],'xtick',[],'ytick',[],'color',[.98,.99,1]);
handles.axes2.XLim = [0,60];
handles.axes2.YLim = [0,6];
handles.neighbours.data = [2,3];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hold on
line([.25,.25],[0,6],'LineWidth',2,'Color',[.3,.75,.93]);
line([59.75,59.75],[0,6],'LineWidth',2,'Color',[.3,.75,.93]);
line([0,60],[0.1,0.1],'LineWidth',2,'Color',[.3,.75,.93]);
line([0,60],[5.9,5.9],'LineWidth',2,'Color',[.3,.75,.93]);
line([0,60],[3,3],'LineWidth',2,'Color',[.3,.75,.93]);
fixed_data = imread('small_fixed.PNG');
roller_data = imread('small_roller.PNG');
pin_data = imread('small_pin.PNG');
spring_data = imread('small_spring.PNG');
torsional_data = imread('small_torsional.PNG');
none_data = imread('small_none.PNG');
rigid_data = imread('small_rigid.PNG');
hinge_data = imread('small_hinge.PNG');
free_data = imread('small_free.PNG');

handles.constraintimages.fixed_im = image([0.5,9.5],[5.7,3.3],fixed_data);
setappdata(handles.constraintimages.fixed_im,'type','fixed');
set(handles.constraintimages.fixed_im, 'buttonDownFcn',@cdial_eval);
handles.constraintimages.fixed.patch = patch([0.5,9.5,9.5,0.5],[3.3,3.3,5.7,5.7],'b');
set(handles.constraintimages.fixed.patch,'FaceColor','none');
set(handles.constraintimages.fixed.patch,'EdgeColor','green');
set(handles.constraintimages.fixed.patch,'LineWidth',2);
set(handles.constraintimages.fixed.patch,'Visible','off');

handles.constraintimages.roller_im = image([11.5,19.5],[5.7,3.3],roller_data);
setappdata(handles.constraintimages.roller_im,'type','roller');
set(handles.constraintimages.roller_im, 'buttonDownFcn',@cdial_eval);
handles.constraintimages.roller.patch = patch([11.5,19.5,19.5,11.5],[3.3,3.3,5.7,5.7],'b');
set(handles.constraintimages.roller.patch,'FaceColor','none');
set(handles.constraintimages.roller.patch,'EdgeColor','green');
set(handles.constraintimages.roller.patch,'LineWidth',2);
set(handles.constraintimages.roller.patch,'Visible','off');

handles.constraintimages.pin_im = image([21.5,29.5],[5.7,3.3],pin_data);
setappdata(handles.constraintimages.pin_im,'type','pin');
set(handles.constraintimages.pin_im, 'buttonDownFcn',@cdial_eval);
handles.constraintimages.pin.patch = patch([21.5,29.5,29.5,21.5],[3.3,3.3,5.7,5.7],'b');
set(handles.constraintimages.pin.patch,'FaceColor','none');
set(handles.constraintimages.pin.patch,'EdgeColor','green');
set(handles.constraintimages.pin.patch,'LineWidth',2);
set(handles.constraintimages.pin.patch,'Visible','off');

handles.constraintimages.spring_im = image([31.5,39.5],[5.7,3.3],spring_data);
setappdata(handles.constraintimages.spring_im,'type','spring');
set(handles.constraintimages.spring_im, 'buttonDownFcn',@cdial_eval);
handles.constraintimages.spring.patch = patch([31.5,39.5,39.5,31.5],[3.3,3.3,5.7,5.7],'b');
set(handles.constraintimages.spring.patch,'FaceColor','none');
set(handles.constraintimages.spring.patch,'EdgeColor','green');
set(handles.constraintimages.spring.patch,'LineWidth',2);
set(handles.constraintimages.spring.patch,'Visible','off');

handles.constraintimages.torsional_im = image([41.5,49.5],[5.7,3.3],torsional_data);
setappdata(handles.constraintimages.torsional_im,'type','torsional spring');
set(handles.constraintimages.torsional_im, 'buttonDownFcn',@cdial_eval);
handles.constraintimages.torsional.patch = patch([41.5,49.5,49.5,41.5],[3.3,3.3,5.7,5.7],'b');
set(handles.constraintimages.torsional.patch,'FaceColor','none');
set(handles.constraintimages.torsional.patch,'EdgeColor','green');
set(handles.constraintimages.torsional.patch,'LineWidth',2);
set(handles.constraintimages.torsional.patch,'Visible','off');

handles.constraintimages.none_im = image([51.5,59.5],[5.7,3.3],none_data);
setappdata(handles.constraintimages.none_im,'type','none');
set(handles.constraintimages.none_im, 'buttonDownFcn',@cdial_eval);
handles.constraintimages.none.patch = patch([51.5,59.5,59.5,51.5],[3.3,3.3,5.7,5.7],'b');
set(handles.constraintimages.none.patch,'FaceColor','none');
set(handles.constraintimages.none.patch,'EdgeColor','green');
set(handles.constraintimages.none.patch,'LineWidth',2);
set(handles.constraintimages.none.patch,'Visible','off');

handles.connectionimages.rigid_im = image([5,20],[2.7,0.3],rigid_data);
setappdata(handles.connectionimages.rigid_im,'type','fixed connection');
set(handles.connectionimages.rigid_im, 'buttonDownFcn',@cdial_eval);
set(handles.connectionimages.rigid_im, 'Visible','off');
handles.connectionimages.rigid.patch = patch([5,20,20,5],[2.7,2.7,0.3,0.3],'b');
set(handles.connectionimages.rigid.patch,'FaceColor','none');
set(handles.connectionimages.rigid.patch,'EdgeColor','green');
set(handles.connectionimages.rigid.patch,'LineWidth',2);
set(handles.connectionimages.rigid.patch,'Visible','off');

handles.connectionimages.hinge_im = image([22.5,37.5],[2.7,0.3],hinge_data);
setappdata(handles.connectionimages.hinge_im,'type','hinge');
set(handles.connectionimages.hinge_im, 'buttonDownFcn',@cdial_eval);
set(handles.connectionimages.hinge_im, 'Visible','off');
handles.connectionimages.hinge.patch = patch([22.5,37.5,37.5,22.5],[2.7,2.7,0.3,0.3],'b');
set(handles.connectionimages.hinge.patch,'FaceColor','none');
set(handles.connectionimages.hinge.patch,'EdgeColor','green');
set(handles.connectionimages.hinge.patch,'LineWidth',2);
set(handles.connectionimages.hinge.patch,'Visible','off');

handles.connectionimages.free_im = image([40,55],[2.7,0.3],free_data);
setappdata(handles.connectionimages.free_im,'type','free');
set(handles.connectionimages.free_im, 'buttonDownFcn',@cdial_eval);
set(handles.connectionimages.free_im, 'Visible','off');
handles.connectionimages.free.patch = patch([40,55,55,40],[2.7,2.7,0.3,0.3],'b');
set(handles.connectionimages.free.patch,'FaceColor','none');
set(handles.connectionimages.free.patch,'EdgeColor','green');
set(handles.connectionimages.free.patch,'LineWidth',2);
set(handles.connectionimages.free.patch,'Visible','off');


%%%%%%%%%%%%%%%%%%%%
guidata(gcf,handles)
% result = handles;
% end

function result = cdial_eval(object,eventdata)
handles = guidata(gcf);
image = object;
type = getappdata(image,'type');
if strcmp(type,'fixed')
    if strcmp(handles.constraintimages.fixed.patch.Visible,'off') || strcmp(handles.constraintimages.fixed.patch.Visible,'Off')
    set(handles.constraintimages.fixed.patch,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.rigid.patch,'Visible','On');
    handles.temp.constrainttype = 'fixed';
    handles.temp.knottype = 'connection';
    else
    set(handles.constraintimages.fixed.patch,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');

    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    handles.temp = rmfield(handles.temp,'knottype');
    end
elseif strcmp(type,'pin')
    if strcmp(handles.constraintimages.pin.patch.Visible,'off') || strcmp(handles.constraintimages.pin.patch.Visible,'Off')
    set(handles.constraintimages.pin.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(handles.neighbours.data) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    else
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'pin';
    else
    set(handles.constraintimages.pin.patch,'Visible','off');
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');

    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.hinge_im,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    if isfield(handles.temp,'knottype')
        handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'roller')
    %ANGLE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if strcmp(handles.constraintimages.roller.patch.Visible,'off') || strcmp(handles.constraintimages.roller.patch.Visible,'Off')
    set(handles.constraintimages.roller.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(handles.neighbours.data) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    else
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'roller';
    else   
    set(handles.constraintimages.roller.patch,'Visible','off');
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');

    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.hinge_im,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    if isfield(handles.temp,'knottype')
        handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'spring')
    %RESILIENCE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if numel(handles.neighbours.data) == 1
    if strcmp(handles.constraintimages.spring.patch.Visible,'off') || strcmp(handles.constraintimages.spring.patch.Visible,'Off')
    set(handles.constraintimages.spring.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.constrainttype = 'spring';
    handles.temp.knottype = 'hinge';
    else   
    set(handles.constraintimages.spring.patch,'Visible','off');
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');

    set(handles.connectionimages.hinge_im,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    handles.temp = rmfield(handles.temp,'knottype');
    end
    else
        msgbox('Springs at points with more than one neighbour are not supported.','Error!','error');
    end
elseif strcmp(type,'torsional spring')
    %RESILIENCE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if strcmp(handles.constraintimages.torsional.patch.Visible,'off') || strcmp(handles.constraintimages.torsional.patch.Visible,'Off')
    set(handles.constraintimages.torsional.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(handles.neighbours.data) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    else
        set(handles.connectionimages.hinge_im,'Visible','on');
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'torsional spring';
    else
    
    set(handles.constraintimages.torsional.patch,'Visible','off');
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');

    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.hinge_im,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    if isfield(handles.temp,'knottype')
        handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'none')
    if strcmp(handles.constraintimages.none.patch.Visible,'off') || strcmp(handles.constraintimages.none.patch.Visible,'Off')
    set(handles.constraintimages.none.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    
    if numel(handles.neighbours.data) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    else
    set(handles.connectionimages.free_im,'Visible','on');
    set(handles.connectionimages.free.patch,'Visible','on');
    handles.temp.knottype = 'free';
    end
    handles.temp.constrainttype = 'none';
    else
    
    set(handles.constraintimages.none.patch,'Visible','off');
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');

    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.hinge_im,'Visible','off');
    set(handles.connectionimages.free_im,'Visible','off');
    handles.temp = rmfield(handles.temp,'constrainttype');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    set(handles.connectionimages.free.patch,'Visible','off');
    if isfield(handles.temp,'knottype')
        handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'fixed connection')
    if strcmp(handles.connectionimages.rigid.patch.Visible,'off') || strcmp(handles.connectionimages.rigid.patch.Visible,'Off')
    set(handles.connectionimages.hinge.patch,'Visible','off');
    set(handles.connectionimages.free.patch,'Visible','off');
    set(handles.connectionimages.rigid.patch,'Visible','on');
    handles.temp.knottype = 'connection';
    else
    if strcmp(handles.temp.constrainttype,'pin') || strcmp(handles.temp.constrainttype,'roller') || strcmp(handles.temp.constrainttype,'torsional spring') || strcmp(handles.temp.constrainttype,'none')
    set(handles.connectionimages.rigid.patch,'Visible','off');
    handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'hinge')
    if strcmp(handles.connectionimages.hinge.patch.Visible,'off') || strcmp(handles.connectionimages.hinge.patch.Visible,'Off')
    set(handles.connectionimages.hinge.patch,'Visible','on');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.free.patch,'Visible','off');
    handles.temp.knottype = 'hinge';
    else
    if strcmp(handles.temp.constrainttype,'pin') || strcmp(handles.temp.constrainttype,'roller') || strcmp(handles.temp.constrainttype,'torsional spring') || strcmp(handles.temp.constrainttype,'none')
    set(handles.connectionimages.hinge.patch,'Visible','off');
    handles.temp = rmfield(handles.temp,'knottype');
    end
    end
elseif strcmp(type,'free')
    if strcmp(handles.connectionimages.free.patch.Visible,'off') || strcmp(handles.connectionimages.free.patch.Visible,'Off')
    set(handles.connectionimages.free.patch,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    handles.temp.knottype = 'free';
    else
    set(handles.connectionimages.free.patch,'Visible','off');
    handles.temp = rmfield(handles.temp,'knottype');
    end
end
%%%%%%%%%%%%%%%%%%%%%
guidata(gcf,handles)
%%%%%%%%%%%%%%%%%%%%%
result = handles;
end


function result = cdial_set(constrainttype,knottype,neighbours,handles)
% Function only considers allowed combinations. Neighbours must be a vector
% containing the point numbers of the neighbours to the current point.
%Resetting the interface to original state:
    set(handles.constraintimages.fixed_im,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','on');
    set(handles.constraintimages.roller_im,'Visible','on');
    set(handles.constraintimages.spring_im,'Visible','on');
    set(handles.constraintimages.torsional_im,'Visible','on');
    set(handles.constraintimages.none_im,'Visible','on');
    set(handles.constraintimages.fixed.patch,'Visible','off');
    set(handles.constraintimages.pin.patch,'Visible','off');
    set(handles.constraintimages.roller.patch,'Visible','off');
    set(handles.constraintimages.spring.patch,'Visible','off');
    set(handles.constraintimages.torsional.patch,'Visible','off');
    set(handles.constraintimages.none.patch,'Visible','off');
    set(handles.connectionimages.rigid_im,'Visible','off');
    set(handles.connectionimages.hinge_im,'Visible','off');
    set(handles.connectionimages.free_im,'Visible','off');
    set(handles.connectionimages.rigid.patch,'Visible','off');
    set(handles.connectionimages.hinge.patch,'Visible','off');
    set(handles.connectionimages.free.patch,'Visible','off');    

%Evualuating the input args
if strcmp(constrainttype,'fixed')
    set(handles.constraintimages.fixed.patch,'Visible','on');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.rigid.patch,'Visible','On');
    handles.temp.constrainttype = 'fixed';
    handles.temp.knottype = 'connection';
elseif strcmp(constrainttype,'pin')
    set(handles.constraintimages.pin.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(neighbours) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    if strcmp(knottype,'hinge')
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    else
        set(handles.connectionimages.rigid.patch,'Visible','on');
        handles.temp.knottype = 'connection';
    end
    else
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'pin';
elseif strcmp(constrainttype,'roller')
    set(handles.constraintimages.roller.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(neighbours) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    if strcmp(knottype,'hinge')
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    else
        set(handles.connectionimages.rigid.patch,'Visible','on');
        handles.temp.knottype = 'connection';
    end
    else
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'roller';
elseif strcmp(constrainttype,'spring')
    set(handles.constraintimages.spring.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    set(handles.connectionimages.hinge_im,'Visible','on');
    set(handles.connectionimages.hinge.patch,'Visible','on');
    handles.temp.constrainttype = 'spring';
    handles.temp.knottype = 'hinge';
elseif strcmp(constrainttype,'torsional spring')
    set(handles.constraintimages.torsional.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.none_im,'Visible','off');
    
    if numel(neighbours) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    if strcmp(knottype,'hinge')
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    else
        set(handles.connectionimages.rigid.patch,'Visible','on');
        handles.temp.knottype = 'connection';
    end
    else
        set(handles.connectionimages.hinge_im,'Visible','on');
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    end
    handles.temp.constrainttype = 'torsional spring';
elseif strcmp(constrainttype,'none')
    set(handles.constraintimages.none.patch,'Visible','on');
    set(handles.constraintimages.fixed_im,'Visible','off');
    set(handles.constraintimages.pin_im,'Visible','off');
    set(handles.constraintimages.roller_im,'Visible','off');
    set(handles.constraintimages.spring_im,'Visible','off');
    set(handles.constraintimages.torsional_im,'Visible','off');
    
    if numel(neighbours) > 1
    set(handles.connectionimages.rigid_im,'Visible','on');
    set(handles.connectionimages.hinge_im,'Visible','on');
    if strcmp(knottype,'hinge')
        set(handles.connectionimages.hinge.patch,'Visible','on');
        handles.temp.knottype = 'hinge';
    else
        set(handles.connectionimages.rigid.patch,'Visible','on');
        handles.temp.knottype = 'connection';
    end
    else
    set(handles.connectionimages.free_im,'Visible','on');
    set(handles.connectionimages.free.patch,'Visible','on');
    handles.temp.knottype = 'free';
    end
    handles.temp.constrainttype = 'none';
end
result = handles;
end