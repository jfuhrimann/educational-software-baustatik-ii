function h = beam_image_FM(Fig, h, primarystructure, unitforce, pointload, distributedload, moment)
%This function plots the image of the beam with the respective statical
%constraints. The beam will be plotted horizontally with its respective
%mattering
%primarystructure: true=do static determinated system, false=remain with the given system
%unitforce: true=plot a unit force on the primarystructure, false=plot
%forces on the primarystructure

set(0, 'currentfigure', Fig);

l = h.beam.l;
pa = pointname(h.beam.beampoints(1));
pb = pointname(h.beam.beampoints(2));
ca = h.S.newconstrainta;
cb = h.S.newconstraintb;
kna = h.S.newknottypea;
knb = h.S.newknottypeb;
neighbours_a = h.points.(pa).neighbours;
neighbours_b = h.points.(pb).neighbours;
angleroller_a = h.points.(pa).constraintangle;
angleroller_b = h.points.(pb).constraintangle;
ra = [0;0];
rb = [l;0];
if acosd(dot(h.beam.n, [1;0])/(norm(h.beam.n)*norm([1;0]))) > 180
    n = [0; 1];
else
    n = [0;-1];
end
t = [1;0];
sh = h.beam.hinge;
imscale = 3*l;

h.number = 1 + h.number;
h.name = strcat('beamimage',num2str(h.number));
distfromprev = 0;
distfromnext = 15; 
h.obj.(h.name) = axes('Parent', h.(h.panel),'units','pixels','position', [h.width/2-365/2, h.lastbottompos - distfromprev - 200, 365, 200]);
hold on
axis equal
if strcmp(h.unit,'kN/m')
    set(h.obj.(h.name),'xtick',[0, l/4, l/2, 3*l/4, l]);
    l1 = strcat('$\frac{L_{tot}}{4}$'); 
    l2 = strcat('$\frac{L_{tot}}{2}$'); 
    l3 = strcat('$\frac{3*L_{tot}}{4}$'); 
    set(h.obj.(h.name), 'TickLabelInterpreter', 'latex', 'XTickLabel', {'$0$',l1,l2,l3,'$\1$'})
else
    set(h.obj.(h.name),'xtick',[0, l/4, l/2, 3*l/4, l]);
    set(h.obj.(h.name), 'TickLabelInterpreter', 'latex', 'XTickLabel', {'$0$','$\frac{1}{4} L_{tot}$','$\frac{1}{2} L_{tot}$','$\frac{3}{4} L_{tot}$','$\L_{tot}$'})
end
    set(h.obj.(h.name),'ytick',[]);
    set(h.obj.(h.name),'xcolor',[0.6471    0.6471    0.6471]);
    set(h.obj.(h.name),'ycolor',[1 1 1]);

% change to primarystructure
if primarystructure && h.S.N.end > 0
    if (strcmp(cb,'fixed') || strcmp(cb,'torsional spring')) && (strcmp(ca,'fixed') || strcmp(ca,'torsional spring')) && sh<0
        kna = 'hinge';     
        knb = 'hinge';  
    elseif (strcmp(cb,'fixed') || strcmp(cb,'torsional spring'))
        if ~(strcmp(ca,'roller') && iscollinear([-sind(angleroller_a); cosd(angleroller_a)],t)) && ~strcmp(ca,'none')
            knb = 'hinge';          
        end  
    elseif (strcmp(ca,'fixed') || strcmp(ca,'torsional spring'))
        if ~(strcmp(cb,'roller') && iscollinear([-sind(angleroller_b); cosd(angleroller_b)],t)) && ~strcmp(cb,'none')
            kna = 'hinge';
        end
    end
end
% Plot the points:
% Point a
    if strcmp(kna,'hinge')
    pointsymbol = 'o';
    else pointsymbol = 'x';
    end
    h.points.(pa).pointmarker2 = ...
    plot(ra(1),ra(2),strcat(pointsymbol,'-k'),'MarkerFaceColor','w');
    uistack(h.points.(pa).pointmarker2,'top');
    if h.beam.beampoints(1) < 9
        textbox_a = text(-0.07*imscale,0,strcat('$\raisebox{.5pt}{\textcircled{\raisebox{-.9pt} {',num2str(h.beam.beampoints(1)),'}}}$'), 'Interpreter', 'latex');
    else
        plot(-0.058*imscale,0, 'ok', 'MarkerSize', 15); grid on; hold on;
        textbox_a = text(-0.07*imscale,0,strcat('${',num2str(h.beam.beampoints(1)),'}$'), 'Interpreter', 'latex');
    end
    uistack(textbox_a,'top');
    
%Point b
    if strcmp(knb,'hinge')
    pointsymbol = 'o';
    else pointsymbol = 'x';
    end
    h.points.(pb).pointmarker2 = ...
    plot(rb(1),rb(2),strcat(pointsymbol,'-k'),'MarkerFaceColor','w');
    uistack(h.points.(pb).pointmarker2,'top');
    if h.beam.beampoints(2) < 9
        textbox_b = text(l + 0.07*imscale,0,strcat('$\raisebox{.5pt}{\textcircled{\raisebox{-.9pt} {',num2str(h.beam.beampoints(2)),'}}}$'), 'Interpreter', 'latex');
    else
        plot(l+0.082*imscale,0, 'ok', 'MarkerSize', 15); grid on; hold on;
        textbox_b = text(l + 0.07*imscale,0,strcat('${',num2str(h.beam.beampoints(2)),'}$'), 'Interpreter', 'latex');
    end
    uistack(textbox_b,'top');
    
if ~unitforce
% Plot the loads
%DISTRIBUTED LOAD
if distributedload

    if not(h.beam.q1(1) == 0 && h.beam.q2(1) == 0)
        if strcmp(h.unit,'kN'), unit = 'kN/m';
        elseif strcmp(h.unit,'q'), unit = 'q';
        elseif strcmp(h.unit,'Q'), unit = 'Q/l';
        else unit = 'M/l^2'; end
    end
    
qa = h.beam.q1(1);
qb = h.beam.q2(1);

if or(qa~=0, qb~=0)
    sa=h.beam.q1(2);
    sb=h.beam.q2(2);

    rb=ra+sb*t;
    ra=ra+sa*t;

    rab = rb-ra;    % vector connecting the two ends of the beam

    l  = norm(rab); % length of the beam computed as the norm of the vector connecting its endpoints

    %%Definition of the applied trapezoidal load
    % Value of the distributed trapezoidal node at a point s defined as an
    % inline function

    q = @(s) qa + (qb-qa)*s/l;

    % Computation of the load at different points along the beam
    % An interval of length l/20 is used to define the points

    m = 9*(sb-sa);
    si = 0:(l/m*3/8*l):l;  % Length along the beam
    if si(numel(si)) ~= l
        si = [si, l];
    end
    qi = q(si);     % Evaluation of the load using the defined inline function

    %%Plotting of loads

    % Definition of the scale used for plotting loads
    scale1 = 0.15*l/max(abs(qi));

    % Unit vectors tangent and normal to the beam
    
    if iscollinear(h.beam.qdirection,h.beam.n) && abs(acosvec(h.beam.qdirection,h.beam.n)) ~= 180
        nq = n;
    else
        a = acosvec(h.beam.qdirection,h.beam.n);
        nq = [cosd(a) -sind(a);sind(a) cosd(a)]*n;
    end

    if (qa>=0) && (qb>=0)
            rqt = ra + t*si - 0.15*scale1*nq - scale1*nq*abs(qi); % points defining the top of the distributed load
            rqb = ra + t*si - 0.15*scale1*nq; % points defining the bottom of the distributed load (used for plotting the arrows)

            patch_q = [ra-0.15*scale1*nq rqt rb-0.15*scale1*nq]; % Coordinates of points defining the patch used to visualize the load

            % Plotting of the load.

            % Plot a patch representing the load
            h.beam.qpatch1 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(h.beam.qpatch1,'bottom');
            h.beam.qquiver1 = quiver(rqt(1,:),rqt(2,:),scale1*nq(1)*abs(qi),scale1*nq(2)*abs(qi),0,'r');
            if qa ~= 0, h.beam.qtext1 = text(rqt(1,1)+0.005*imscale,rqt(2,1)+0.03*imscale,{['$' num2str(qa) '\ ' unit '$']},'Interpreter','latex','HorizontalAlignment','right','VerticalAlignment','top'); end
            if qb ~= 0, h.beam.qtext2 = text(rqt(1,numel(si))+0.005*imscale,rqt(2,numel(si))+0.03*imscale,{['$' num2str(qb) '\ ' unit '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','top'); end
            h.beam.qtext1.Color = 'red';
            h.beam.qtext2.Color = 'red';

    elseif (qa<0) && (qb<0)
            rqt = ra + t*si + 0.15*scale1*nq + scale1*nq*abs(qi); % points defining the top of the distributed load
            rqb = ra + t*si + 0.15*scale1*nq; % points defining the bottom of the distributed load (used for plotting the arrows)

            patch_q = [ra+0.15*scale1*nq rqt rb+0.15*scale1*nq]; % Coordinates of points defining the patch used to visualize the load

            % Plotting of the load.

            % Plot a patch representing the load
            h.beam.qpatch1 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(h.beam.qpatch1,'bottom');
            h.beam.qquiver1 = quiver(rqt(1,:),rqt(2,:),scale1*-nq(1)*abs(qi),scale1*-nq(2)*abs(qi),0,'r');
            if qa ~= 0, h.beam.qtext1 = text(rqt(1,1)-0.005*imscale,rqt(2,1)-0.03*imscale,{['$' num2str(qa) '\ ' unit '$']},'Interpreter','latex','HorizontalAlignment','right','VerticalAlignment','bottom'); end
            if qb ~= 0, h.beam.qtext2 = text(rqt(1,numel(si))-0.005*imscale,rqt(2,numel(si))-0.03*imscale,{['$' num2str(qb) '\ ' unit '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','bottom'); end
            h.beam.qtext1.Color = 'red';
            h.beam.qtext2.Color = 'red';

    else
        if qa<0
        %select only positive loads and set all negatives loads to 0
            qi1=qi;

            rqt = ra + t*si(qi1<0) +0.15*scale1*nq + scale1*nq*abs(qi1(qi1<0)); % points defining the top of the distributed load
            rqb = ra + t*si(qi1<0) + 0.15*scale1*nq; % points defining the bottom of the distributed load (used for plotting the arrows)
            
            rbneu=ra+t*si(numel(qi1(qi1<0)));
            patch_q = [ra+0.15*scale1*nq rqt rbneu+0.15*scale1*nq]; % Coordinates of points defining the patch used to visualize the load
            
            % Plotting of the load.

            % Plot a patch representing the load
            h.beam.qpatch1 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(h.beam.qpatch1,'bottom');
            h.beam.qquiver1 = quiver(rqt(1,:),rqt(2,:),scale1*-nq(1)*abs(qi1(qi1<0)),scale1*-nq(2)*abs(qi1(qi1<0)),0,'r');
            if qa ~= 0, h.beam.qtext1 = text(rqt(1,1)-0.005*imscale,rqt(2,1)-0.03*imscale,{['$' num2str(qa) '\ ' unit '$']},'Interpreter','latex','HorizontalAlignment','right','VerticalAlignment','bottom'); end
            h.beam.qtext1.Color = 'red'; 

        %select only negative loads and set all positive loads to 0    
            qi2=qi;

            raneu = rb - t*si(numel(qi2(qi2>=0)));
            rqt = ra + t*si(qi2>=0) - 0.15*scale1*nq - scale1*nq*abs(qi2(qi2>=0)); % points defining the top of the distributed load
            rqb = ra + t*si(qi2>=0) - 0.15*scale1*nq; % points defining the bottom of the distributed load (used for plotting the arrows)
            
            patch_q = [raneu-0.15*scale1*nq rqt rb-0.15*scale1*nq]; % Coordinates of points defining the patch used to visualize the load

            % Plotting of the load
            
            % Plot a patch representing the load
            h.beam.qpatch2 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(h.beam.qpatch2,'bottom');
            h.beam.qquiver2 = quiver(rqt(1,:),rqt(2,:),scale1*nq(1)*abs(qi2(qi2>=0)),scale1*nq(2)*abs(qi2(qi2>=0)),0,'r');
            if qb ~= 0, h.beam.qtext2 = text(rqt(1,numel(rqt(1,:)))+0.005*imscale,rqt(2,numel(rqt(1,:)))+0.03*imscale,{['$' num2str(qb) '\ ' unit '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','top'); end
            h.beam.qtext2.Color = 'red';
            
        else %if qb<0
         %select only positive loads and set all negative loads to 0
            qi1=qi;

            rqt = ra + t*si(qi1>=0) - 0.15*scale1*nq - scale1*nq*abs(qi1(qi1>=0)); % points defining the top of the distributed load
            rqb = ra + t*si(qi1>=0) - 0.15*scale1*nq; % points defining the bottom of the distributed load (used for plotting the arrows)

            rbneu=ra+t*si(numel(qi1(qi1>=0)));
            patch_q = [ra-0.15*scale1*nq rqt rbneu-0.15*scale1*nq]; % Coordinates of points defining the patch used to visualize the load

            % Plotting of the load.

            % Plot a patch representing the load
            h.beam.qpatch1 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(h.beam.qpatch1,'bottom');
            h.beam.qquiver1 = quiver(rqt(1,:),rqt(2,:),scale1*nq(1)*abs(qi1(qi1>=0)),scale1*nq(2)*abs(qi1(qi1>=0)),0,'r');
            if qa ~= 0, h.beam.qtext1 = text(rqt(1,1)+0.005*imscale,rqt(2,1)+0.03*imscale,{['$' num2str(qa) '\ ' unit '$']},'Interpreter','latex','HorizontalAlignment','right','VerticalAlignment','top'); end
            h.beam.qtext1.Color = 'red';

        %select only negative loads and set all positive loads to 0    
            qi2=qi;

            raneu = rb - t*si(numel(qi2(qi2<0)));
            rqt = ra + t*si(qi2<0) + 0.15*scale1*nq + scale1*nq*abs(qi2(qi2<0)); % points defining the top of the distributed load
            rqb = ra + t*si(qi2<0) + 0.15*scale1*nq; % points defining the bottom of the distributed load (used for plotting the arrows)

            patch_q = [raneu+0.15*scale1*nq rqt rb+0.15*scale1*nq]; % Coordinates of points defining the patch used to visualize the load

            % Plotting of the load.

            % Plot a patch representing the load
            h.beam.qpatch2 = patch(patch_q(1,:),patch_q(2,:),[1 1 1],'EdgeColor','r');
            uistack(h.beam.qpatch2,'bottom');
            h.beam.qquiver2 = quiver(rqt(1,:),rqt(2,:),scale1*-nq(1)*abs(qi2(qi2<0)),scale1*-nq(2)*abs(qi2(qi2<0)),0,'r');
            if qb ~= 0, h.beam.qtext2 = text(rqt(1,numel(rqt(1,:)))-0.005*imscale,rqt(2,numel(rqt(1,:)))-0.03*imscale,{['$' num2str(qb) '\ ' unit '$']},'Interpreter','latex','HorizontalAlignment','left','VerticalAlignment','bottom'); end
            h.beam.qtext2.Color = 'red';
        end
    end
end
end

ra = [0;0];
%POINT LOAD
if pointload
    if strcmp(h.unit,'kN'), unit = 'kN';
    elseif strcmp(h.unit,'q'), unit = 'qL';
    elseif strcmp(h.unit,'Q'), unit = 'Q';
    else unit = 'M/l'; end

Fvalue = cell2mat(h.beam.F(:,1));

if Fvalue ~= 0
    
    Fs = cell2mat(h.beam.F(:,2));
%     if sh > 0
%         Fs = Fs + sh;
%     end
    Fangle = cell2mat(h.beam.F(:,3));
    total = numel(Fvalue(:,1));

    Fmax = max(Fvalue);

    scale = 0.1*imscale/Fmax;

    for i=1:total
            a = Fangle(i);
            nf =[cosd(a), -sind(a) ; sind(a) , cosd(a)]*n;

        Fxy = ra + t*Fs(i)-nf*scale*Fvalue(i);
        uv = nf*scale*Fvalue(i);

        h.beam.loads.Fimg(i) = quiver(Fxy(1),Fxy(2),uv(1),uv(2),'r','LineWidth',1);
        h.beam.loads.Ftext(i) = text(Fxy(1)+0.003*imscale,Fxy(2)+0.02*imscale,{['$' num2str(Fvalue(i)) '\ ' unit '$']},'Interpreter','latex');
        h.beam.loads.Ftext(i).Color = 'red';
    end
end
end
end

%MOMENT

if strcmp(h.unit,'kN'), unit = 'kNm';
elseif strcmp(h.unit,'q'), unit = 'ql^2';
elseif strcmp(h.unit,'Q'), unit = 'Ql';
else unit = 'M'; 
end

if ~unitforce && h.beam.M(1) ~= 0 && moment
        M = h.beam.M(1);
        s = h.beam.M(2);
elseif primarystructure && unitforce
    % define M1 / M2 on the primary structure
    s = [l; 0];
        if (strcmp(cb,'fixed') || strcmp(cb,'torsional spring')) && (strcmp(ca,'fixed') || strcmp(ca,'torsional spring')) && sh<0
            M = [1; 1];
        elseif (strcmp(cb,'fixed') || strcmp(cb,'torsional spring')) && sh<0
            if ~(strcmp(ca,'roller') && iscollinear([-sind(angleroller_a); cosd(angleroller_a)],t)) && ~strcmp(ca,'none')      
                M = [1; 0];
            else
                M = 0;
            end
        elseif (strcmp(cb,'fixed') || strcmp(cb,'torsional spring')) && (strcmp(ca,'fixed') || strcmp(ca,'torsional spring')) && sh>0
            M = [1; 0];
        elseif (strcmp(ca,'fixed') || strcmp(ca,'torsional spring')) && sh<0
            if ~(strcmp(cb,'roller') && iscollinear([-sind(angleroller_b); cosd(angleroller_b)],t)) && ~strcmp(cb,'none')      
                M = [0; 1];
            else
                M = 0;
            end
        else
            M = 0;
        end
else
    M = 0;
end

if any(M) ~= 0 
    for i=1:numel(M)
        if M(i) ~= 0
        if unitforce
            if i == 1
                th = linspace( pi/2, -pi/2, 100); 
            else
                th = linspace( pi/2, 3*pi/2, 100);           
            end
        else
            th = linspace(pi/2, -pi, 100);
        end
        R = 0.03*imscale;
        x = R*cos(th) + ra(1) + t(1)*s(i);
        y = R*sin(th) + ra(2) + t(2)*s(i);
        h.beam.Mstar = plot(ra(1)+t(1)*s(i),ra(2)+t(2)*s(i),'*');
        h.beam.Mimg = plot(x,y,'LineWidth',1.5);
        if unitforce
            if i == 1
                if h.S.N.end == 1
                    h.beam.Mtext = text(ra(1) + t(1)*s(i) + t(1)*2*R + R*n(1),ra(2) + t(2)*0.6*s(i) - R*n(2), {strcat('$',h.S.nrX_1,' = 1$')},'Interpreter','latex');
                else
                    h.beam.Mtext = text(ra(1) + t(1)*s(i) + t(1)*2*R + R*n(1),ra(2) + t(2)*0.6*s(i) - R*n(2), {strcat('$',h.S.nrX_2,' = 1$')},'Interpreter','latex');
                end
            elseif i == 2
                h.beam.Mtext = text(ra(1) - t(1)*s(i) - t(1)*4*R - R*n(1),ra(2) + t(2)*0.6*s(i) - R*n(2), {strcat('$',h.S.nrX_1,' = 1$')},'Interpreter','latex');
            end
        else
            h.beam.Mtext = text(ra(1) + t(1)*s(i) - t(1)*2*R - R*n(1),ra(2) + t(2)*0.6*s(i) - R*n(2), {['$' num2str(M(i)) '\ ' unit '$']},'Interpreter','latex');
        end
        if unitforce
            h.beam.Mtext.Color = 'blue';
            h.beam.Mstar.Color = 'blue';
            h.beam.Mimg.Color = 'blue';
        else
            h.beam.Mtext.Color = 'red';
            h.beam.Mstar.Color = 'red';
            h.beam.Mimg.Color = 'red';
        end
        if unitforce
            if i == 1
                mpoly = ra +t*s(i)+[0;R];
                spoly = mpoly + 0.5*R*[1/sqrt(2);1/sqrt(2)];
                epoly = mpoly + 0.5*R*[1/sqrt(2);-1/sqrt(2)];
                x=[spoly(1),mpoly(1),epoly(1)];
                y=[spoly(2),mpoly(2),epoly(2)];
                Mpoly = plot(x,y,'LineWidth',1.5);
            else
                mpoly = ra +t*s(i)+[0;-R];
                spoly = mpoly - 0.5*R*[1/sqrt(2);1/sqrt(2)];
                epoly = mpoly - 0.5*R*[1/sqrt(2);-1/sqrt(2)];
                x=[spoly(1),mpoly(1),epoly(1)];
                y=[spoly(2),mpoly(2),epoly(2)];
                Mpoly = plot(x,y,'LineWidth',1.5);
            end
        else
            if sign(M(i)) == 1 %M>0
                mpoly = ra +t*s(i)+[0;R];
                spoly = mpoly + 0.5*R*[1/sqrt(2);1/sqrt(2)];
                epoly = mpoly + 0.5*R*[1/sqrt(2);-1/sqrt(2)];
                x=[spoly(1),mpoly(1),epoly(1)];
                y=[spoly(2),mpoly(2),epoly(2)];
                Mpoly = plot(x,y,'LineWidth',1.5);
            elseif sign(M(i)) ==-1 %M<0
                mpoly = ra + t*s(i) + [-R;0];
                spoly = mpoly + 0.5*R*[1/sqrt(2);-1/sqrt(2)];
                epoly = mpoly + 0.5*R*[-1/sqrt(2);-1/sqrt(2)];
                x=[spoly(1),mpoly(1),epoly(1)];
                y=[spoly(2),mpoly(2),epoly(2)];
                Mpoly = plot(x,y,'LineWidth',1.5);
            end
        end
        if ~unitforce
            Mpoly.Color = 'red';
        else
            Mpoly.Color = 'blue';
        end
        end
    end    
end

l = h.beam.l;
ra = [0;0];
rb = [l;0];
% Plot the beam
    beamaxis = plot([0 l],[0 0],'-k','LineWidth',1); 
    uistack(beamaxis,'bottom');

% Plot the dashes below the beam
    dashes = plot([0 l]+n(1)*0.005*imscale, [0 0]+n(2)*0.005*imscale,'--k');
    uistack(dashes,'bottom');

% Plot the hinge
    if sh ~= -1
        uistack(plot(t(1)*sh,0,'o-k','MarkerFaceColor','w'),'top');
    end

% Plot the constraints
%Checking pa
    if numel(neighbours_a) > 1
        if not(strcmp(kna,'hinge'))
            leftconstraint = 'fixed';
        else
            leftconstraint = 'hinge';
        end
    else
        leftconstraint = ca;
    end
%Checking pb
    if numel(neighbours_b) > 1
        if not(strcmp(knb,'hinge'))
            rightconstraint = 'fixed';
        else
            rightconstraint = 'hinge';
        end
    else rightconstraint = cb;
    end
    
% change to primarystructure
if primarystructure && h.S.N.end > 0
    if (strcmp(cb,'fixed') || strcmp(cb,'torsional spring')) && (strcmp(ca,'fixed') || strcmp(ca,'torsional spring')) && sh<0
        leftconstraint = 'pin'; 
        rightconstraint = 'pin';
    elseif (strcmp(cb,'fixed') || strcmp(cb,'torsional spring'))
        if ~(strcmp(ca,'roller') && iscollinear([-sind(angleroller_a); cosd(angleroller_a)],t)) && ~strcmp(ca,'none')
            rightconstraint = 'pin';
        end
    elseif (strcmp(ca,'fixed') || strcmp(ca,'torsional spring'))
        if ~(strcmp(cb,'roller') && iscollinear([-sind(angleroller_b); cosd(angleroller_b)],t)) && ~strcmp(cb,'none')
            leftconstraint = 'pin';
        end
    end
end

%Positions and angles
    leftpos = [0;0];
    rightpos = [l;0];
    leftangle = h.points.(pa).constraintangle;
    rightangle = h.points.(pb).constraintangle;
%Import images and get their angles of rotation for left image
    if not(strcmp(leftconstraint,'none'))        
        figleft = imread(strcat(leftconstraint,'.png'));
        if strcmp(leftconstraint,'roller')
            if numel(neighbours_a) == 1 && leftangle == 1000
                leftangle = 0;
            else
                leftangle = leftangle - 90;
            end
        elseif strcmp(leftconstraint, 'spring')
            leftangle = 0;
        elseif strcmp(leftconstraint, 'fixed')
            leftangle = 180;
        else leftangle = 0;
        end
        if strcmp(leftconstraint, 'fixed')
            figscale = 0.03*imscale;
        elseif strcmp(leftconstraint, 'roller')
            figscale = 0.035*imscale;
        else
            figscale = 0.05*imscale;
        end
        figleft = imrotate_white(figleft, leftangle);
        h.beam.leftim2 = ...
            imagesc([leftpos(1)+figscale, leftpos(1)-figscale], [leftpos(2)+figscale,leftpos(2)-figscale], figleft);
        uistack(h.beam.leftim2,'bottom');
    end

%Same for right image
        if not(strcmp(rightconstraint,'none'))
            figright = imread(strcat(rightconstraint,'.png'));
        if strcmp(rightconstraint,'roller')
            if numel(neighbours_b) == 1 && rightangle == 1000
                rightangle = 0;
            else
                rightangle = rightangle - 90;
            end
        elseif strcmp(rightconstraint, 'spring')
            rightangle = 0;
        elseif strcmp(rightconstraint,'fixed')
            rightangle = 0;
        else rightangle = 0;
        end
        figright = imrotate_white(figright,-rightangle);
        if strcmp(rightconstraint, 'fixed')
            figscale = 0.03*imscale;
        elseif strcmp(rightconstraint, 'roller')
            figscale = 0.035*imscale;
        else
            figscale = 0.05*imscale;
        end
        if strcmp(rightconstraint, 'torsional spring')
            h.beam.rightim2 = ...
            imagesc([rightpos(1)-figscale, rightpos(1)+figscale], [rightpos(2)+figscale,rightpos(2)-figscale], figright);
        else
            h.beam.rightim2 = ...
                imagesc([rightpos(1)+figscale, rightpos(1)-figscale], [rightpos(2)+figscale,rightpos(2)-figscale], figright);
        end
        uistack(h.beam.rightim2,'bottom');
        end
        
            
% Plot L_tot = ....
    ylimgraph = ylim;
    textLtot = text(l + 0.07*imscale,ylimgraph(2),strcat('$L_{tot} =', num2str(h.S.l), h.length, '$'), 'Interpreter', 'latex', 'VerticalAlignment', 'top', 'Color', [52, 102, 145]/255);
       
        if primarystructure || pointload || distributedload || moment
            h.obj.(h.name).Position(4) = 150;
            h.obj.(h.name).Position(2) = h.obj.(h.name).Position(2) + 50;
        end
        h.lastbottompos = h.obj.(h.name).Position(2)  - distfromnext;

if h.interactive
    pause(h.pause);
else
    pause(0.7);
end
end