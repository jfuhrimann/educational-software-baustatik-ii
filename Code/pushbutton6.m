function result = pushbutton6(handles)
handles.pushbutton6.Enable = 'off';
if handles.changebeam
    %Find the beam that has been changed
    for i = 1 : numel(handles.beamnames)
        beam = char(handles.beamnames(i));
        if handles.beams.(beam).changethisbeam
            if isfield(handles.beams.(beam),'loads')
                loadnames = fieldnames(handles.beams.(beam).loads);
                    for j = 1 : numel(loadnames)
                        delete(handles.beams.(beam).loads.(char(loadnames(j))));
                    end
                    handles.beams.(beam) = rmfield(handles.beams.(beam),'loads');
            end
            delete(handles.beams.(beam).image);
            delete(handles.beams.(beam).dashes);
            if isfield(handles.beams.(beam),'hingegraphic')
                delete(handles.beams.(beam).hingegraphic);
            end
            if isfield(handles.beams.(beam),'leftim')
                delete(handles.beams.(beam).leftim);
            end
            if isfield(handles.beams.(beam),'rightim')
                delete(handles.beams.(beam).rightim);
            end
            handles.togglebutton4.Value = 0;
            handles.togglebutton4.BackgroundColor = [0.95, 0.95, 0.95];
            handles.beams.(beam).changethisbeam = false;
            handles.temp = i;
        end
    end
end
        EIvalue=readfrac(get(handles.edit13,'string'));
        handles.beams.(char(handles.beamnames(handles.temp))).EI=EIvalue;
        
        if (get(handles.radiobutton2,'Value')) == 1
            set(handles.text35, 'Visible','On');
            set(handles.edit14, 'Visible','On');
            if strcmp(get(handles.edit14,'string'), 'Nan'), set(handles.edit14,'string', '0'); end
            hingevalue=readfrac_parameter(get(handles.edit14,'string'),handles.beams.(char(handles.beamnames(handles.temp))).l);
            handles.beams.(char(handles.beamnames(handles.temp))).hinge=hingevalue;
        else
            handles.beams.(char(handles.beamnames(handles.temp))).hinge=-1;
        end
     
        if strcmp(get(handles.edit26,'string'),'0')||isempty(get(handles.edit26,'string'))
            F = [{0} {0} {0}];
        else
            if isfield(handles.beams.(char(handles.beamnames(handles.temp))),'F')
                handles.beams.(char(handles.beamnames(handles.temp))) = rmfield(handles.beams.(char(handles.beamnames(handles.temp))),'F');
            end
            F = get(handles.uitable5,'data');
        end
        
        n = handles.beams.(char(handles.beamnames(handles.temp))).n;
        t = handles.beams.(char(handles.beamnames(handles.temp))).t;
        if not(iscell(F))
            F = mat2cell(F,ones(1,numel(F(:,1))),ones(1,numel(F(1,:))));
            for i = 1: numel(F)
                F{i} = num2str(F{i});
            end
        end
        for i=1:numel(F(:,3))
            if not(isnumeric(F{i,3}))
            if char(F{i,3}) == 'h'
                F{i,3} = acosvec([1; 0],n);
            elseif char(F{i,3}) == 'v'
                F{i,3} = acosvec([0; -1],n);
            elseif char(F{i,3}) == 'n'
                F{i,3} = 0;
            else, F{i,3} = str2double(F{i,3});
            end
            end
            %Reading fractions in uitable5
            if not(isnumeric(F{i,1}))
            F{i,1} = readfrac(F{i,1});
            end
            if not(isnumeric(F{i,2}))
            F{i,2} = readfrac_parameter(char(F{i,2}),handles.beams.(char(handles.beamnames(handles.temp))).l);
            end
        end
        zeroline = [];
        if numel(F(:,1))>1
        for i = 1 : numel(F(:,1))
            if cell2mat(F(i,1)) == 0
                zeroline = [zeroline,i];
            end
        end
        end
        F(zeroline,:) = [];
            
        handles.beams.(char(handles.beamnames(handles.temp))).F = F;          
        
        %Check for free ends with point loads or moments:
        pt1 = pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1));
        pt2 = pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2));
        n = handles.beams.(char(handles.beamnames(handles.temp))).n;
        % Check point 1
        % Find orthogonal rollers at point 1
        if strcmp(handles.points.(pt1).constrainttype,'roller') && numel(handles.points.(pt1).neighbours) == 1
        alpha = handles.points.(pt1).constraintangle;
        if alpha ~= 1000
            rollervec = [cosd(alpha) -sind(alpha);sind(alpha) cosd(alpha)]*[0;1];
        end
        end
        % true free end:
        if numel(handles.points.(pt1).neighbours) == 1 &&...
                ((strcmp(handles.points.(pt1).constrainttype,'none') && ...
                strcmp(handles.points.(pt1).knottype,'free'))||...
                (strcmp(handles.points.(pt1).constrainttype,'roller') && ...
                alpha ~= 1000 && iscollinear(rollervec,t)))
            if norm(handles.points.(pt1).F) ~= 0
                delete(handles.points.(pt1).loads.Fimg);
                delete(handles.points.(pt1).loads.Ftext);
                alpha_F = num2str(acosvec(handles.points.(pt1).F,n));
                if all(all(str2double(handles.beams.(char(handles.beamnames(handles.temp))).F)==[0 0 0]))
                    handles.beams.(char(handles.beamnames(handles.temp))).F = [{num2str(norm(handles.points.(pt1).F))},{'0'},{alpha_F}];
                else
                    handles.beams.(char(handles.beamnames(handles.temp))).F = ...
                    [handles.beams.(char(handles.beamnames(handles.temp))).F; {num2str(norm(handles.points.(pt1).F))},{'0'},{alpha_F}];
                end
                alpha = 1000;
                rollervec = [];
            end
            if handles.points.(pt1).M ~= 0
                delete(handles.points.(pt1).loads.Mimg);
                delete(handles.points.(pt1).loads.Mtext);
                delete(handles.points.(pt1).loads.Mpoly);
                handles.beams.(char(handles.beamnames(handles.temp))).M = [handles.points.(pt1).M,0];
            end
        end
        
        % Check point 2
        % Find orthogonal rollers at point 2
        if strcmp(handles.points.(pt2).constrainttype,'roller') && numel(handles.points.(pt2).neighbours) == 1
        alpha = handles.points.(pt2).constraintangle;
        if alpha ~= 1000
            rollervec = [cosd(alpha) -sind(alpha);sind(alpha) cosd(alpha)]*[0;1];
        end
        end
        % true free end:
        if numel(handles.points.(pt2).neighbours) == 1 &&...
                ((strcmp(handles.points.(pt2).constrainttype,'none') && ...
                strcmp(handles.points.(pt2).knottype,'free'))||...
                (strcmp(handles.points.(pt2).constrainttype,'roller') && ...
                alpha ~= 1000 && iscollinear(rollervec,t)))
            l = handles.beams.(char(handles.beamnames(handles.temp))).l;
            if norm(handles.points.(pt2).F) ~= 0
                delete(handles.points.(pt2).loads.Fimg);
                delete(handles.points.(pt2).loads.Ftext);
                alpha_F = num2str(acosvec(handles.points.(pt2).F,n));
                if all(all(str2double(handles.beams.(char(handles.beamnames(handles.temp))).F)==[0 0 0]))
                    handles.beams.(char(handles.beamnames(handles.temp))).F = [{num2str(norm(handles.points.(pt2).F))},{num2str(l)},{alpha_F}];
                else
                    handles.beams.(char(handles.beamnames(handles.temp))).F = ...
                    [handles.beams.(char(handles.beamnames(handles.temp))).F; {num2str(norm(handles.points.(pt2).F))},{num2str(l)},{alpha_F}];
                end
            end
            if handles.points.(pt2).M ~= 0
                delete(handles.points.(pt2).loads.Mimg);
                delete(handles.points.(pt2).loads.Mtext);
                delete(handles.points.(pt2).loads.Mpoly);
                handles.beams.(char(handles.beamnames(handles.temp))).M = [handles.points.(pt2).M,l];
            end
        end
        
        q1value=readfrac(get(handles.edit18,'string'));
        q1svalue = readfrac_parameter(get(handles.edit19,'string'),handles.beams.(char(handles.beamnames(handles.temp))).l);
        handles.beams.(char(handles.beamnames(handles.temp))).q1=[q1value, q1svalue];
        
        
        q2value=readfrac(get(handles.edit21,'string'));
        q2svalue = readfrac_parameter(get(handles.edit22,'string'),handles.beams.(char(handles.beamnames(handles.temp))).l);
        handles.beams.(char(handles.beamnames(handles.temp))).q2 = [q2value, q2svalue];
        
        if get(handles.radiobutton4, 'Value')==1 %if direction of load 'normal to beam' is choosen
            handles.qdirection = 1;
            a = 0;
            handles.beams.(char(handles.beamnames(handles.temp))).qangle = [a];
        elseif get(handles.radiobutton5, 'Value')==1 %if direction of load 'vertical' is choosen
            handles.qdirection = 2;
            d = [0; -1];
            a = acosvec(d,n);
            handles.beams.(char(handles.beamnames(handles.temp))).qangle = [a];
        elseif get(handles.radiobutton6, 'Value')==1 %if direction of load 'horizontal' is choosen
            handles.qdirection = 3;
            d = [1;0];
            a = acosvec(d,n);
            handles.beams.(char(handles.beamnames(handles.temp))).qangle = [a];
        else
            handles.beams.(char(handles.beamnames(handles.temp))).qangle = 0;
        end
        
        if not(isfield(handles.beams.(char(handles.beamnames(handles.temp))),'M')) || ...
                handles.changebeam && (handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).M == 0 && strcmp(handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).knottype,'free') ||...
                not(strcmp(handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).knottype,'free'))) &&...
                (handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2))).M == 0 && strcmp(handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2))).knottype,'free') ||...
                not(strcmp(handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2))).knottype,'free')))
        Mvalue=readfrac(get(handles.edit24,'string'));
        Msvalue = readfrac_parameter(get(handles.edit25,'string'),handles.beams.(char(handles.beamnames(handles.temp))).l);
        handles.beams.(char(handles.beamnames(handles.temp))).M=[Mvalue, Msvalue];
        end

        psivalue=readfrac(get(handles.edit27,'string'));
        handles.beams.(char(handles.beamnames(handles.temp))).psi=psivalue;

        
        %beam
        x_beam=[handles.points.(char(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1)))).r(1), handles.points.(char(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2)))).r(1)];
        y_beam=[handles.points.(char(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1)))).r(2), handles.points.(char(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2)))).r(2)];
        
        beamsscale=handles.beams.(char(handles.beamnames(1))).EI;
        thickness=handles.beams.(char(handles.beamnames(handles.temp))).EI;
        handles.beamwidth = thickness/beamsscale;
        
        %plot beam
        handles.beams.(char(handles.beamnames(handles.temp))).image = plot(x_beam,y_beam,'-k','LineWidth',thickness/beamsscale); %uistack(...,'bottom') defines that the drawing of the beams are under the pins
        uistack(handles.beams.(char(handles.beamnames(handles.temp))).image,'bottom');
        setappdata(handles.beams.(char(handles.beamnames(handles.temp))).image,'beam',char(handles.beamnames(handles.temp)));
%        set(handles.beams.(char(handles.beamnames(handles.temp))).image, 'buttonDownFcn',@callsolution);
        setappdata(handles.beams.(char(handles.beamnames(handles.temp))).image,'blue',false);
        set(getpoints,'WindowButtonDownFcn',@callsolution);
        
        
        %plot dashed line under the positive side of the beam
        nvector = handles.beams.(char(handles.beamnames(handles.temp))).n;
        handles.plotscale=max((handles.scalexupper-handles.scalexlower),(handles.scaleyupper-handles.scaleylower));
        handles.beams.(char(handles.beamnames(handles.temp))).dashes = plot(x_beam+nvector(1)*0.007*handles.plotscale,y_beam+nvector(2)*0.007*handles.plotscale,'--k');
        
        %plot distributed load
        if not(handles.beams.(char(handles.beamnames(handles.temp))).q1(1) == 0 &&...
                handles.beams.(char(handles.beamnames(handles.temp))).q1(2) == 0)
        handles = distributedload(handles);
        end
        
        %plot the point loads
        handles = pointload(handles);

        %plot the external moments
        handles = mext(handles);
        
        %plot the hinge
        if handles.beams.(char(handles.beamnames(handles.temp))).hinge ~= -1
            r = handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).r;
            t = handles.beams.(char(handles.beamnames(handles.temp))).t;
            s = handles.beams.(char(handles.beamnames(handles.temp))).hinge;
            handles.beams.(char(handles.beamnames(handles.temp))).hingegraphic = plot(r(1)+t(1)*s,r(2)+t(2)*s,'o-k','MarkerFaceColor','w');
            uistack(handles.beams.(char(handles.beamnames(handles.temp))).hingegraphic,'top');
        end
              
        %plot the image of the constraint
        %left point = point with lower number
        %right point = point with higher number
        leftconstraint = handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).constrainttype;
        rightconstraint = handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2))).constrainttype;
        leftpos = handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).r;
        rightpos = handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2))).r;
        n = handles.beams.(char(handles.beamnames(handles.temp))).n;
        t = handles.beams.(char(handles.beamnames(handles.temp))).t;
        leftangle = handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).constraintangle;
        rightangle = handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2))).constraintangle;

        %Angles of rotation
        if not(strcmp(leftconstraint,'none'))        
            figleft = imread(strcat(leftconstraint,'.png'));
        if strcmp(leftconstraint,'roller')
            if numel(handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).neighbours) == 1
                if leftangle == 1000
                    if -n(1)<=0
                    leftangle = acosd(dot([0;1],-n));
                    else leftangle = -acosd(dot([0;1],-n));
                    end
                end
            end
        elseif strcmp(leftconstraint, 'spring')
            if -n(1)<=0
                    leftangle = acosd(dot([0;1],-n));
            else leftangle = -acosd(dot([0;1],-n));
            end
        elseif strcmp(leftconstraint, 'fixed')
            if -n(1)<=0
                leftangle = acosd(dot([0;1],-n))+180;
                else leftangle = -acosd(dot([0;1],-n))+180;
            end
        else leftangle = 0;
        end
        imscale = (handles.scalexupper-handles.scalexlower)*0.08;
        if imscale == 0
            imscale = (handles.scaleyupper-handles.scaleylower)*0.08;
        end
        figleft = imrotate_white(figleft,-leftangle);
        handles.beams.(char(handles.beamnames(handles.temp))).leftim = ...
            imagesc([leftpos(1)+imscale, leftpos(1)-imscale], [leftpos(2)+imscale,leftpos(2)-imscale], figleft);
        uistack(handles.beams.(char(handles.beamnames(handles.temp))).leftim,'bottom');
        end
        
        if not(strcmp(rightconstraint,'none'))
            figright = imread(strcat(rightconstraint,'.png'));
        if strcmp(rightconstraint,'roller')
            if numel(handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2))).neighbours) == 1
                if rightangle == 1000
                    if -n(1)<=0
                    rightangle = acosd(dot([0;1],-n));
                    else rightangle = -acosd(dot([0;1],-n));
                    end
                end
            end
        elseif strcmp(rightconstraint, 'spring')
            if -n(1)<=0
                    rightangle = acosd(dot([0;1],-n));
            else rightangle = -acosd(dot([0;1],-n));
            end
        elseif strcmp(rightconstraint,'fixed')
            if -n(1)<=0
                rightangle = acosd(dot([0;1],-n));
                else rightangle = -acosd(dot([0;1],-n));
            end
        else rightangle = 0;
        end
        figright = imrotate_white(figright,-rightangle);
        imscale = (handles.scalexupper-handles.scalexlower)*0.08;
        if imscale == 0
            imscale = (handles.scaleyupper-handles.scaleylower)*0.08;
        end
        handles.beams.(char(handles.beamnames(handles.temp))).rightim = ...
            imagesc([rightpos(1)+imscale, rightpos(1)-imscale], [rightpos(2)+imscale,rightpos(2)-imscale], figright);
        uistack(handles.beams.(char(handles.beamnames(handles.temp))).rightim,'bottom');
        end
    % Text beam rotation if there is any
    if strcmp(handles.unit,'kN'), unit = 'rad';
    elseif strcmp(handles.unit,'q'), unit = '\frac{ql^3}{EI}';
    elseif strcmp(handles.unit,'Q'), unit = '\frac{Ql^2}{EI}';
    else unit = '\frac{Ml}{EI}'; end
    if not(handles.beams.(char(handles.beamnames(handles.temp))).psi) == 0
        text(handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).r(1)...
            + handles.beams.(char(handles.beamnames(handles.temp))).t(1)/2 ...
            + handles.beams.(char(handles.beamnames(handles.temp))).n(1)*imscale,...
            handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).r(2)...
            + handles.beams.(char(handles.beamnames(handles.temp))).t(2)/2 ...
            + handles.beams.(char(handles.beamnames(handles.temp))).n(2)*imscale,...
            strcat('$ \psi = ', checkfrac(handles.beams.(char(handles.beamnames(handles.temp))).psi),unit,'$'),'Interpreter','latex');
    end
    
    %Bring all constraint images to the bottom layer
    for i = 1 : numel(handles.beamnames)
        beam = char(handles.beamnames(i));
        if isfield(handles.beams.(beam),'leftim')
            uistack(handles.beams.(beam).leftim,'bottom')
        end
        if isfield(handles.beams.(beam),'rightim')
            uistack(handles.beams.(beam).rightim,'bottom')
        end
    end
    %Compute Forcemethod
    handles = solveforcemethod(char(handles.beamnames(handles.temp)),handles);
        
    % delete the n-vector of the current beam plotted previously.
    delete(handles.nimg);
    delete(handles.ntext);

    % delete the t-vector of the current beam plotted previously
    delete(handles.timg);
    delete(handles.ttext);
    
    %delete the direction of the angle
    delete(handles.acircplot);
    delete(handles.apolyplot);
    delete(handles.atext);
    if not(handles.changebeam)
        %go to the next beam
        handles.temp=handles.temp+1;
    end
       
if numel(handles.beamnames) >= (handles.temp) && not(handles.changebeam)
        %reset the GUI and set the new beam
        if strcmp(handles.unit,'kN'), unitl = 'm';
        else, unitl = 'L'; end
        set(handles.text15, 'String', num2str(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1)));
        set(handles.text25, 'String', num2str(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(2)));
        set(handles.text37,'String', strcat(num2str(handles.beams.(char(handles.beamnames(handles.temp))).l),unitl));
        set(handles.edit13,'String',num2str(handles.beams.(char(handles.beamnames(handles.temp-1))).EI));
        set(handles.edit14,'String','');
        set(handles.edit14,'Visible','off');
        set(handles.text35,'Visible','off');
        set(handles.radiobutton2,'Value',0);
        set(handles.radiobutton4,'Value',0);
        set(handles.radiobutton5,'Value',0);
        set(handles.radiobutton6,'Value',0);
        set(handles.edit26,'string','');
        tablesize = [0 0 0];
        set(handles.uitable5,'Data',tablesize);
        set(handles.text33,'Visible','On');
        set(handles.edit18,'String','0');
        set(handles.edit19,'String','0');
        set(handles.edit21,'String','0');
        set(handles.edit22,'String','0');
        set(handles.edit24,'String','0');
        set(handles.edit25,'String','0');
        set(handles.edit27,'String','0');    
        set(handles.edit22,'string','L');

%Plot normal Vector of the first beam
r1=handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).r(1);
r2=handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).r(2);
pn1 = handles.beams.(char(handles.beamnames(handles.temp))).n(1)*0.2*handles.imscale^0.8;
pn2 = handles.beams.(char(handles.beamnames(handles.temp))).n(2)*0.2*(handles.imscale)^0.8;
handles.nimg=quiver(r1-pn1/2,r2-pn2/2,1.5*pn1,1.5*pn2,'k');
handles.ntext=text(r1+pn1+0.05*handles.imscale^0.8,r2+pn2+0.05*(handles.imscale)^0.8,'n');

%Plot tangential vector of the first beam
pt1 = handles.beams.(char(handles.beamnames(handles.temp))).t(1)*0.2*handles.imscale^0.8;
pt2 = handles.beams.(char(handles.beamnames(handles.temp))).t(2)*0.2*handles.imscale^0.8;
handles.timg=quiver(r1,r2,pt1,pt2,'k');
handles.ttext=text(r1+pt1+0.05*handles.imscale^0.8,r2+pt2+0.05*(handles.imscale)^0.8,'t');


%plot positive direction of the angle (semi-circle)
    %Find the starting angle (alpha(x direction)=0)
    if pn1 >= 0 && pn2 >= 0             %first sector
        alpha = atand(pt2/pt1);
    elseif pn1 < 0 && pn2 >= 0
        alpha = 180 + atand(pt2/pt1);   %second sector
    elseif pn1 < 0 && pn2 < 0
        alpha = 180 + atand(pt2/pt1);   %third sector
    elseif pn1 >= 0 && pn2 <= 0
        alpha = 360 + atand(pt2/pt1);   %fourth sector
    end
    
    %prepare the circle
    th = linspace( alpha - 90, alpha + 90, 100);
    R = 0.25*norm([pn1;pn2]);
    x = -R*cosd(th) + r1;
    y = -R*sind(th) + r2;
    
    %prepare the arrow
    mpoly = [r1;r2] + [0.25*pn1; 0.25*pn2];
    spoly = mpoly + 0.4*R*[cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)]*[-1/sqrt(2);-1/sqrt(2)];
    epoly = mpoly + 0.4*R*[cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)]*[-1/sqrt(2);1/sqrt(2)];
    px=[spoly(1),mpoly(1),epoly(1)];
    py=[spoly(2),mpoly(2),epoly(2)];
    handles.acircplot = plot(x,y,'k');
    handles.apolyplot =  plot(px,py,'k');
    handles.atext = text(r1 - 0.5*pt1 ,r2 - 0.5*pt2, '\alpha');

else
    handles.pushbutton7.Visible = 'on';
    handles.save_geometry.Visible = 'on';
    handles.togglebutton2.Visible = 'on';
    handles.togglebutton3.Visible = 'on';
    handles.pushbutton6.Visible = 'off';
    handles.togglebutton4.Visible = 'on';
    if strcmp(handles.unit,'kN'), unitl = 'm';
        else, unitl = 'L'; end
        set(handles.text14, 'String', 'You are finished!');
        set(handles.text23, 'Visible', 'Off');
        set(handles.text36, 'Visible', 'Off');
        set(handles.text15, 'String', '');
        set(handles.text25, 'String', '');
        set(handles.text37,'String', '');
        set(handles.edit13,'String', '');
        set(handles.edit14,'String','');
        set(handles.edit14,'Visible','off');
        set(handles.text35,'Visible','off');
        set(handles.radiobutton2,'Value',0);
        set(handles.radiobutton4,'Value',0);
        set(handles.radiobutton5,'Value',0);
        set(handles.radiobutton6,'Value',0);
        set(handles.edit26,'string','');
        tablesize = [0 0 0];
        set(handles.uitable5,'Data',tablesize);
        set(handles.text33,'Visible','On');
        set(handles.edit18,'String','0');
        set(handles.edit19,'String','0');
        set(handles.edit21,'String','0');
        set(handles.edit22,'String','0');
        set(handles.edit24,'String','0');
        set(handles.edit25,'String','0');
        set(handles.edit27,'String','0');
        
        %Everything invisible
        handles.finalcover.Visible = 'On';
        handles.finaltext.Visible = 'On';
        handles.finaltext2.Visible = 'On';
        handles.finaltext3.Visible = 'On';
        handles.finaltext4.Visible = 'On';
        handles.info.Visible = 'off';
        
%       handles.finalcover.VerticalAlignment = 'center';
        handles.finaltext.FontName = 'Bahnschrift';
        handles.finaltext.FontSize = 12;
        handles.finaltext2.FontName = 'Bahnschrift';
        handles.finaltext2.FontSize = 12;
        handles.finaltext3.FontName = 'Bahnschrift';
        handles.finaltext3.FontSize = 10;
    myicon=imread('ok.jpg');
    msgbox('All beams are defined now. You can continue.','Well done!','custom',myicon);
end
if handles.changebeam
    handles.changebeam = false;
end
handles.pushbutton6.Enable = 'on';
result = handles;
end