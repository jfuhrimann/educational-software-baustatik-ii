function foundpsi = testpsi(handles)
foundpsi = false;
for i = 1 : numel(handles.beamnames)
    if handles.beams.(char(handles.beamnames(i))).haspsi
        disp(strcat(char(handles.beamnames(i)),'has a psi'));
        foundpsi = true;
    end
end
if not(foundpsi)
    disp('System unverschieblich');
end
end