function callsolution(object,eventdata)
handles=guidata(gcf);
C = get(gca,'CurrentPoint');
if isfield(handles,'beamnames')
beamnames = handles.beamnames;
for i = 1 : numel(beamnames)
    beam = char(beamnames(i));
    r = handles.points.(pointname(handles.beams.(beam).beampoints(1))).r;
    t = handles.beams.(beam).t;
    n = handles.beams.(beam).n;
    p = [C(1,1);C(1,2)];
    syms lambda kappa
    [lambda_s,kappa_s] = solve(p + lambda*n == r + t*kappa,[lambda,kappa]);
    blue = getappdata(handles.beams.(beam).image, 'blue');
    if not(isempty(lambda_s)) && not(isempty(kappa_s)) && abs(lambda_s) < handles.imscale/80 && abs(kappa_s) < handles.beams.(beam).l && kappa_s>=0
        if not(handles.changebeam)
        if not(blue)
        handles.beams.(beam).image.Color = 1/255*[68,186,80];
        handles.beams.(beam).dashes.Color = 1/255*[68,186,80];
        setappdata(handles.beams.(beam).image,'blue',true)
        end
        solution(beam,handles);
        else %Want to modify beam characteristics
        handles.beams.(beam).image.Color = 1/255*[66,161,244];
        handles.beams.(beam).dashes.Color = 1/255*[66,161,244];
        handles.finalcover.Visible = 'Off';
        handles.finaltext.Visible = 'Off';
        handles.finaltext2.Visible = 'Off';
        handles.finaltext3.Visible = 'Off';
        handles.finaltext4.Visible = 'Off';
        handles.info.Visible = 'on';
        handles.pushbutton7.Visible = 'Off';
        handles = changebeamfcn(handles,beam);
        end
    end
end
else %We have only points, no beams yet
    p = [C(1,1);C(1,2)];
    if p(1) < handles.axes1.XLim(2) && p(1) > handles.axes1.XLim(1) && p(2) < handles.axes1.YLim(2) && p(2) > handles.axes1.YLim(1)
    if handles.usegrid && not(handles.defineneighbours)
        for i = 1 : numel(handles.xcoords)
            for j = 1 : numel(handles.ycoords)
                rp = [handles.xcoords(i); handles.ycoords(j)];
                if norm(rp-p) < norm([handles.axes1.XLim(2)-handles.axes1.XLim(1);handles.axes1.YLim(2)-handles.axes1.YLim(1)])/80
                    if isfield(handles,'points')
                    for k = 1 : numel(fieldnames(handles.points))
                        existing = false;
                        if all(handles.points.(pointname(k)).r == rp)
                            existing = true;
                            break;
                        end
                    end
                    else, existing = false;
                    end
                    if not(existing)
                    handles = newpoint(rp,handles);
                    found = true;
                    break;
                    end
                else
                    found = false;
                end
            end
            if found == true
                break;
            end
        end
    elseif handles.usegrid && handles.defineneighbours
    for i = 1 : numel(handles.pointnames)
        point = char(handles.pointnames(i));
        rp = handles.points.(point).r;
        if norm(rp-p) < norm([handles.axes1.XLim(2)-handles.axes1.XLim(1);handles.axes1.YLim(2)-handles.axes1.YLim(1)])/80
            if not(handles.firstclick)
                handles.firstclick = true;
                handles.firstclicknr = handles.points.(point).pointnr;
                handles.points.(point).pointmarker.MarkerEdgeColor = [255 192 0]/255;
                handles.points.(point).textbox.Color = [255 192 0]/255;
            else
                if strcmp(point,pointname(handles.firstclicknr)) || isfield(handles.points.(pointname(handles.firstclicknr)),'neighbours') && ismember(handles.points.(point).pointnr,handles.points.(pointname(handles.firstclicknr)).neighbours)
                    handles.firstclick = false;
                    handles.points.(pointname(handles.firstclicknr)).pointmarker.MarkerEdgeColor = [0,0,0];
                    handles.points.(pointname(handles.firstclicknr)).textbox.Color = [0,0,0];
                    handles.firstclicknr = 0;
                else
                handles.secondclick = true;
                if isfield(handles.points.(pointname(handles.firstclicknr)),'neighbours')
                    handles.points.(pointname(handles.firstclicknr)).neighbours = [handles.points.(pointname(handles.firstclicknr)).neighbours, handles.points.(point).pointnr];
                else
                    handles.points.(pointname(handles.firstclicknr)).neighbours = handles.points.(point).pointnr;
                end
                if isfield(handles.points.(point),'neighbours')
                    handles.points.(point).neighbours = [handles.points.(point).neighbours, handles.points.(pointname(handles.firstclicknr)).pointnr];
                else
                    handles.points.(point).neighbours = handles.points.(pointname(handles.firstclicknr)).pointnr;
                end
                if isfield(handles,'neighbourlines')
                    nr = numel(fieldnames(handles.neighbourlines))+1;
                    handles.neighbourlines.(strcat('line',num2str(nr))) = line([handles.points.(point).r(1),handles.points.(pointname(handles.firstclicknr)).r(1)],...
                    [handles.points.(point).r(2), handles.points.(pointname(handles.firstclicknr)).r(2)],...
                    'Color',[.5,.5,.5]);
                else
                handles.neighbourlines.line1 = line([handles.points.(point).r(1),handles.points.(pointname(handles.firstclicknr)).r(1)],...
                    [handles.points.(point).r(2), handles.points.(pointname(handles.firstclicknr)).r(2)],...
                    'Color',[.5,.5,.5]);
                end
                handles.firstclick = false;
                handles.secondclick = false;
                handles.points.(pointname(handles.firstclicknr)).textbox.Color = [0,0,0];
                handles.points.(pointname(handles.firstclicknr)).pointmarker.MarkerEdgeColor = [0,0,0];
                handles.firstclicknr = 0;
                end
            end
        end
    end
    elseif handles.changepoint
    for i = 1 : numel(handles.pointnames)
        point = char(handles.pointnames(i));
        rp = handles.points.(point).r;
        if norm(rp-p) < norm([handles.axes1.XLim(2)-handles.axes1.XLim(1);handles.axes1.YLim(2)-handles.axes1.YLim(1)])/80
            handles=deletepoint(handles,point);
        end
    end
    end
    end
end
guidata(gcf,handles);
end