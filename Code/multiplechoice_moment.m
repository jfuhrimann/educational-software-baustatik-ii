function h = multiplechoice_moment(Fig, h, M)

set(0, 'currentfigure', Fig);

% [{'\bfWrong:\rm .'},...{'\bfReason:\rm  I.'}, ... {'.'}];

l = h.beam.l;
ca = h.S.newconstrainta;
cb = h.S.newconstraintb;
sm = h.beam.M(2);
sh = h.beam.hinge;

syms x
M(x) = M;

colorM = [0.5 0.8 1];

heightgrafic = 65/214*h.screendim(4) - 15240/107;
widthgrafic = 15/134*h.screendim(3) + 540/67;
left = h.graficpanel.Position(3)/2-widthgrafic/2;
height = h.graficpanel.Position(4);
                       
a = [1, 2, 3];
a = a(randperm(length(a)));
b = 0.09*h.screendim(4);
pos1 = height-heightgrafic*a(1)-20*a(1)-b;
pos2 = height-heightgrafic*a(2)-20*a(2)-b;
pos3 = height-heightgrafic*a(3)-20*a(3)-b;
        
Ml = sm/l;
Mr = (l-sm)/l;

h.multiplechoice.txt = uicontrol('Parent',h.graficpanel,...
       'Style','text',...
       'Position',[20 height-35 300 20],...
       'String','Choose the right moment line:',...
        'HorizontalAlignment','left',...
        'FontSize',12,...
        'FontName','Calibri',...
        'FontWeight','bold',...
        'Background',h.graficpanel.BackgroundColor);
   
h.multiplechoice1 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos1+heightgrafic, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);
h.multiplechoice2 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos2+heightgrafic, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);
h.multiplechoice3 = uicontrol('Style', 'radiobutton', ...
                           'Units',    'pixels', ...
                           'Position', [10, pos3+heightgrafic, 20, 20], ...
                           'Parent',h.graficpanel,...
                           'Background',h.graficpanel.BackgroundColor, ...
                           'Value',    0);

% GRAFIC 1
% generate Wrong answers
        h.multiplechoice.grafic1 = axes('Parent', h.graficpanel,'units','pixels');
        h.multiplechoice.grafic1.Position = [left, pos1+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic1,'xtick',[]);
        set(h.multiplechoice.grafic1,'ytick',[]);
        set(h.multiplechoice.grafic1,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic1,'ycolor',[0.9294    0.9529    0.9882]);
        hold on;        
        
        if sm == 0
            if h.S.N.end > 0
                N(x) = piecewise(x>=0 & x<=l, h.S.Mvalue);
                h.mc.help1 = [{'\bfWrong:\rm The second point cannot take a moment.'},...
                            {'\bfReason:\rm  Doing the moment equilibrium at the second point, the first point must have a reaction force. This will cause an internal moment, that rotates opposite to the external moment .'}];
            else
                N(x) = piecewise(x>=0 & x<=l, -h.S.Mvalue + h.S.Mvalue*x/l);
                h.mc.help1 = [{'\bfWrong:\rm The moment must be constant.'},...
                            {'\bfReason:\rm  If you try to cut different times between 0 and L, you will notice that  the free body diagram will always show the same inner moment.'}];
            end
        elseif sm == l
            if h.S.N.end > 0
                N(x) = piecewise(x>=0 & x<=l, h.S.Mvalue);
                h.mc.help1 = [{'\bfWrong:\rm The first point cannot take a moment.'},...
                            {'\bfReason:\rm  Doing the moment equilibrium at the first point, the second point must have a reaction force. This will cause an internal moment, that rotates opposite to the external moment .'}];
            else
                N(x) = piecewise(x>=0 & x<=l, h.S.Mvalue*x/l);
                h.mc.help1 = [{'\bfWrong:\rm The moment must be constant.'},...
                        {'\bfReason:\rm  If you try to cut different times between 0 and L, you will notice that  the free body diagram will always show the same inner moment.'}];
            end
        elseif (strcmp(ca, 'fixed') || strcmp(ca, 'torsional spring')) || (strcmp(cb, 'fixed') || strcmp(cb, 'torsional spring'))
            if sh > 0
                if strcmp(ca, 'torsional spring')
                    N(x) = piecewise(x>=0 & x<sm, h.S.Mvalue*sm/sh*x/sm, x>=sm & x<sh, -h.S.Mvalue*(1-sm/sh) + h.S.Mvalue*(1-sm/sh)*x/sh, x>=sh & x<=l, 0);
                    h.mc.help1 = [{'\bfWrong:\rm The spring can also take a moment.'},...
                        {'\bfReason:\rm The spring takes a part of the moment proportionally with its flexibility.'}];
                elseif strcmp(cb, 'fixed') || strcmp(cb, 'torsional spring')
                    if sm <= sh
                        Ml = h.S.Mvalue*sm/sh;
                        Mr = h.S.Mvalue*(1-sm/sh);
                        N(x) = piecewise(x>=0 & x<sm, Ml*x/sm, x>=sm & x<=l, -Mr + Mr*(x-sm)/sm + Mr*2/3);            
                        h.mc.help1 = [{'\bfWrong:\rm The moment line must go through 0 at the hinge.'},...
                        {'\bfReason:\rm If you cut the system free at the hinge, its inner force is only a shear force (and also a normal force, which is here equal to zero).'}, ...
                        {'The hinge cannot take a moment.'}];
                    else
                        if h.S.rotationpoint == 1
                            Ml = h.S.Mvalue*(sm-sh)/(l-sh);
                            Mr = h.S.Mvalue*(l-sm)/(l-sh);
                            N(x) = piecewise(x>=0 & x<sh, 0, x>=sh & x<sm, Ml/(sm-sh)*(x-sh), x>=sm & x<=l, -Mr + Mr/(l-sm)*(x-sm) + Mr/2);                
                            h.mc.help1 = [{'\bfWrong:\rm The moment must be constant on the right side.'},...
                            {'\bfReason:\rm  If you try to cut different times between the position where the external moment is and L, you will notice that  the free body diagram will always show the same inner moment.'}, ... 
                            {'This means that the inner moment must be constant from the position of the external moment until L.'}];
                        else
                            Ml = h.S.Mvalue*(sm-sh)/(l-sh);
                            Mr = h.S.Mvalue*(l-sm)/(l-sh);
                            N(x) = piecewise(x>=0 & x<sh, 0, x>=sh & x<sm, Ml/(sm-sh)*(x-sh), x>=sm & x<=l, -Mr + Mr/(l-sm)*(x-sm));
                            if h.S.N.end > 0
                                h.mc.help1 = [{'\bfWrong:\rm The internal moment on the left side cannot be zero.'},...
                                {'\bfReason:\rm  The moment on the right side causes a reaction force on the hinge. This will cause a point load on the left side. The left side can be considered as cantilever with a point load (reaction force of the hinge) at the free end.'}, ... 
                                {'The moment caused by a point load on a cantilever is linear.'}];
                            else
                                h.mc.help1 = [{'\bfWrong:\rm The reaction force of the moment on the second point cannot be zero.'},...
                                {'\bfReason:\rm  The second point is a fixed point and it can absorb a moment.'}];
                            end
                        end
                    end
                else
                    if sm <= sh
                        N(x) = solvemoment(l,[zeros(2, 3); M(0), sh, 0; zeros(1, 3)] ,0,0,0,0,0,0,'connection','free');
                        h.mc.help1 = [{'\bfWrong:\rm The moment must be constant on the left side.'},...
                        {'\bfReason:\rm  If you try to cut different times between 0 and the position where the external moment is, you will notice that  the free body diagram will always show the same inner moment.'}, ... 
                        {'This means that the inner moment must be constant from 0 until the position of the external moment.'}];
                    else
                        if strcmp(cb, 'fixed') || strcmp(cb, 'torsional spring')
                            Ml = h.S.Mvalue*(sm-sh)/(l-sh);
                            Mr = h.S.Mvalue*(1-(sm-sh)/(l-sh));
                            N(x) = piecewise(x>=0 & x<sh, -Ml/sh*x, x>=sh & x<=l, Mr - Mr/(l-sm)*(x-sh));         
                            h.mc.help1 = [{'\bfWrong:\rm The moment must be constant on the right side.'},...
                        {'\bfReason:\rm  If you try to cut different times between the position where the external moment is and L, you will notice that  the free body diagram will always show the same inner moment.'}, ... 
                        {'This means that the inner moment must be constant from the position of the external moment until L.'}];
                        else
                            Ml = h.S.Mvalue*(sm-sh)/(l-sh);
                            Mr = h.S.Mvalue*(1-(sm-sh)/(l-sh));
                            N(x) = piecewise(x>=0 & x<sm, -h.S.Mvalue*2/3 + Ml/(sm-sh)*x, x>=sm & x<=l, - Mr + Mr/(l-sm)*(x-sm));            
                            h.mc.help1 = [{'\bfWrong:\rm The moment line must go through 0 at the hinge.'},...
                            {'\bfReason:\rm If you cut the system free at the hinge, its inner force is only a shear force (and also a normal force, which is here equal to zero).'}, ...
                            {'The hinge cannot take a moment.'}];
                        end
                    end
                end
            else
                if sm == l/2
                    N(x) = piecewise(x>=0 & x<sm, Mr, x>=sm & x<=l, -Ml);
                    h.mc.help1 =  [{'\bfWrong:\rm The slope of the inner moment cannot be equal to zero and the reaction forces cannot have a moment.'},...
                            {'\bfReason:\rm The supports cannot transfer a moment. This implies that the slope must increase constantly until the position of the moment and then decrease again.'}];
                else
                    N(x) = piecewise(x>=0 & x<sm, Mr/sm*x, x>=sm & x<=l, -Ml +Ml/(l-sm)*(x-sm));
                    h.mc.help1 =  [{'\bfWrong:\rm The slope of the inner moment must stay constant.'},...
                            {'\bfReason:\rm The slope would be affected if there were other forces or moments acting on the beam.'}];
                end
            end
        else
            N(x) = piecewise(x>=0 & x<sm, Mr/sm*x, x>=sm & x<=l, -Ml +Ml/(l-sm)*(x-sm));
            h.mc.help1 = [{'\bfWrong:\rm The slope of the inner moment must stay constant.'},...
                        {'\bfReason:\rm The slope would be affected if there were other forces or moments acting on the beam.'}];
        end       
                
        fplot(h.multiplechoice.grafic1,-N(x),[0,l],'color',colorM,'Linewidth',2);         
        fplot(h.multiplechoice.grafic1, 0, [0,l],'-k'); %line at x=0 to distinguish
        
        if isnan(N(0)), border0 = -N(0+1/10^10); else, border0 = -N(0); end
        if isnan(N(l)), borderl = -N(l-1/10^10); else, borderl = -N(l); end
        if double(N(0)) ~= 0, plot(h.multiplechoice.grafic1, [0,0],[0,border0],'color',colorM,'Linewidth',2); end
        if double(N(l)) ~= 0, plot(h.multiplechoice.grafic1, [l,l],[borderl,0],'color',colorM,'Linewidth',2); end

% GRAFIC 2           
% generate Wrong answers     
        h.multiplechoice.grafic2 = axes('Parent', h.graficpanel, 'units','pixels');
        h.multiplechoice.grafic2.Position = [left, pos2+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic2,'xtick',[]);
        set(h.multiplechoice.grafic2,'ytick',[]);
        set(h.multiplechoice.grafic2,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic2,'ycolor',[0.9294    0.9529    0.9882]);
        hold on;
        
            if sm == 0
                if h.S.N.end > 0
                    N(x) = piecewise(x>=0 & x<=l, 0);
                    h.mc.help2 = [{'\bfWrong:\rm There must be an internal moment.'},...
                                {'\bfReason:\rm  Doing the moment equilibrium at the second point, the first point must have a reaction force. This will cause an internal moment, that rotates opposite to the external moment .'}];
                else
                    N(x) = piecewise(x>=0 & x<=l, h.S.Mvalue*x/l);
                    h.mc.help2 = [{'\bfWrong:\rm The moment must be constant.'},...
                                {'\bfReason:\rm  If you try to cut different times between 0 and L, you will notice that  the free body diagram will always show the same inner moment.'}];
                end
            elseif sm == l
                if h.S.N.end > 0
                    N(x) = piecewise(x>=0 & x<=l, 0);
                    h.mc.help2 = [{'\bfWrong:\rm There must be an internal moment.'},...
                                {'\bfReason:\rm  Doing the moment equilibrium at the first point, the second point must have a reaction force. This will cause an internal moment, that rotates opposite to the external moment .'}];
                else
                    N(x) = piecewise(x>=0 & x<=l, -h.S.Mvalue + h.S.Mvalue*x/l);
                    h.mc.help2 = [{'\bfWrong:\rm The moment must be constant.'},...
                                {'\bfReason:\rm  If you try to cut different times between 0 and L, you will notice that  the free body diagram will always show the same inner moment.'}];
                end
            elseif (strcmp(ca, 'fixed') || strcmp(ca, 'torsional spring')) && ~(strcmp(cb, 'fixed') || strcmp(cb, 'torsional spring'))
                if h.S.N.end == 0    
                    N(x) = piecewise(x>=0 & x<sh, Ml, x>=sh & x<=l, -Mr + Mr*x/l);
                    if sm < sh
                        h.mc.help2 = [{'\bfWrong:\rm The right side of the beam cannot have an inner moment.'},...
                        {'\bfReason:\rm  If the beam is separated at the hinge, there are two new systems: The left side turns to a fixed point with a free end and the right side is a single supported beam (on both sides are hinges).'}, ...
                        {'The left side has a moment, which will generate a constant inner moment. The right side has nothing acting on the beam, hence the supporting and inner forces are equal to 0.'}];
                    else
                        h.mc.help2 = [{'\bfWrong:\rm The inner moment must increase linearly from the first hinge and decrease with the same slope until it reaches zero at the second hinge.'},...
                        {'\bfReason:\rm  The hinges cannot have a moment.'}];
                    end
                else
                    if sh < 0 && sm == l/2
                        N(x) = piecewise(x>=0 & x<sm, Mr/4/sm*x, x>=sm & x<=l, -Ml/2 +Ml/2/(l-sm)*(x-sm));
                        h.mc.help2 = [{'\bfWrong:\rm The moment line must have a constant slope.'},...
                            {'\bfReason:\rm  The moment only causes a jump, but the slope stays constant.'}];
                    else
                        N(x) = piecewise(x>=0 & x<=sm, h.S.Mvalue*sm/l, x>=sm & x<=l, -h.S.Mvalue*(1-sm/l));
                        h.mc.help2 = [{'\bfWrong:\rm The inner moment must increase linearly from the first hinge and decrease with the same slope until it reaches zero at the second hinge.'},...
                            {'\bfReason:\rm  The hinges cannot have a moment.'}];
                    end
                end
            elseif (strcmp(cb, 'fixed') || strcmp(cb, 'torsional spring')) && ~(strcmp(ca, 'fixed') || strcmp(ca, 'torsional spring'))
                if h.S.N.end == 0    
                    if sh > 0 && sm < sh
                        Ml = h.S.Mvalue*sm/sh;
                        Mr = h.S.Mvalue*(1-sm/sh);
                        N(x) = piecewise(x>=0 & x<sm, Ml*x/sm, x>=sm & x<sh, -Mr + Mr/(sh-sm)*(x-sm), x>=sh & x<=l, 0);            
                        h.mc.help2 = [{'\bfWrong:\rm The moment cannot be 0 on the right side.'},...
                        {'\bfReason:\rm If you cut the system free at the hinge, its inner force is a shear force which will cause a moment also on the right side.'}];
                    else
                        if strcmp(cb, 'fixed') || strcmp(cb, 'torsional spring')
                                Ml = h.S.Mvalue*(sm-sh)/(l-sh);
                                Mr = h.S.Mvalue*(1-(sm-sh)/(l-sh));
                                if strcmp(cb, 'torsional spring')
                                    N(x) = piecewise(x>=0 & x<sh, 0, x>=sh & x<sm, Ml/(sm-sh)*(x-sh), x>=sm & x<=l, -Mr + Mr/(l-sm)*(x-sm));        
                                else
                                    N(x) = piecewise(x>=0 & x<sh, 0, x>=sh & x<sm, Ml/(sm-sh)*(x-sh), x>=sm & x<=l, -Mr + Mr/(l-sm)*(x-sm) + Mr/2);   
                                end
                                h.mc.help2 = [{'\bfWrong:\rm The moment must be constant on the right side.'},...
                                    {'\bfReason:\rm  If you try to cut different times between the position where the external moment is and L, you will notice that  the free body diagram will always show the same inner moment.'}, ... 
                                    {'This means that the inner moment must be constant from the position of the external moment until L.'}];
                        else 
                            Ml = h.S.Mvalue*(sm-sh)/(l-sh);
                            Mr = h.S.Mvalue*(l-sm)/(l-sh);
                            N(x) = piecewise(x>=0 & x<sh, Ml*x/sh, x>=sh & x<=l, -Mr);
                            h.mc.help2 = [{'\bfWrong:\rm The left side of the beam cannot have an inner moment.'},...
                            {'\bfReason:\rm  If the beam is separated at the hinge, there are two new systems: The right side turns to a fixed point with a free end and the left side is a single supported beam (on es are hinges).'}, ...
                            {'The right side has a moment, which will generate a constant inner moment. The left side has nothing acting on the beam, hence the supporting and inner forces are equal to 0.'}];
                        end
                    end
                else
                    N(x) = piecewise(x>=0 & x<=sm, h.S.Mvalue*sm/l, x>=sm & x<=l, -h.S.Mvalue*(1-sm/l));
                    h.mc.help2 =  [{'\bfWrong:\rm The inner moment must increase linearly from the first hinge and decrease with the same slope until it reaches zero at the second hinge.'},...
                        {'\bfReason:\rm  The hinges cannot have a moment.'}];
                end
            elseif (strcmp(ca, 'fixed') || strcmp(ca, 'torsional spring')) && (strcmp(cb, 'fixed') || strcmp(cb, 'torsional spring')) && sh > 0
                    if strcmp(ca, 'torsional spring')
                        N(x) = piecewise(x>=0 & x<=sm, h.S.Mvalue - h.S.Mvalue*x/sm, x>=sm & x<=l, 0);
                        h.mc.help2 =  [{'\bfWrong:\rm The inner moment must be constant.'},...
                        {'\bfReason:\rm  If the beam is separated at the hinge, there are two new systems: The left side turns to a fixed point with a free end and the right side is a single supported beam (on both sides are hinges).'}, ...
                        {'The left side has a moment, which will generate a constant inner moment.'}];
                    elseif sh > sm
                        N(x) = solvemoment(sh,zeros(3, 3),0,0,0,0,M(0),sm,'connection','hinge');
                        N(x) = piecewise(x>=0 & x<=sh, N(x), x>=sh & x<=l, 0);
                        h.mc.help2 =  [{'\bfWrong:\rm The moment line after the moment position must go to zero.'},...
                        {'\bfReason:\rm  The first part of the beam can be handled as a cantilever. By cutting the cantilever free after the position of the moment, it can be seen that the free end has either external nor internal forces.'}];
                    else
                        N(x) = piecewise(x>=0 & x<=sm, M(0) - M(0)*x/sh, x>=sm & x<=l, 0);
                        h.mc.help2 =  [{'\bfWrong:\rm The moment cannot be zero at the right side.'},...
                        {'\bfReason:\rm  The reaction force of the hinge has a value, which must be equal to that of the second point. This means that there is a shear force on the entire beam, in consequence the moment cannot be zero.'}];
                    end
            else
                if sh > 0
                    if strcmp(ca, 'torsional spring')
                        N(x) = piecewise(x>=0 & x<=sm, h.S.Mvalue - h.S.Mvalue*x/sm, x>=sm & x<=l, 0);
                        h.mc.help2 =  [{'\bfWrong:\rm The inner moment must be constant.'},...
                        {'\bfReason:\rm  If the beam is separated at the hinge, there are two new systems: The left side turns to a fixed point with a free end and the right side is a single supported beam (on both sides are hinges).'}, ...
                        {'The left side has a moment, which will generate a constant inner moment.'}];
                    elseif sh > sm
                        N(x) = solvemoment(l,zeros(3, 3),0,0,0,0,M(0),sh,'connection','free');
                        h.mc.help2 =  [{'\bfWrong:\rm The moment cannot be zero at the right side.'},...
                        {'\bfReason:\rm  The reaction force of the hinge has a value, which must be equal to that of the second point. This means that there is a shear force on the entire beam, in consequence the moment cannot be zero.'}];
                    else
                        N(x) = piecewise(x>=0 & x<=sm, M(0) - M(0)*x/sh, x>=sm & x<=l, 0);
                        h.mc.help2 =  [{'\bfWrong:\rm The moment cannot be zero at the right side.'},...
                        {'\bfReason:\rm  The reaction force of the hinge has a value, which must be equal to that of the second point. This means that there is a shear force on the entire beam, in consequence the moment cannot be zero.'}];
                    end
                else
                    N(x) = -M(x);
                    h.mc.help2 = [{'\bfWrong:\rm Pay attention in which direction the external moment rotates.'},...
                        {'\bfReason:\rmThe arrow must always come out of the moment diagram.'}];
                end
            end
        fplot(h.multiplechoice.grafic2, -N(x),[0,l],'color',colorM,'Linewidth',2); 
        fplot(h.multiplechoice.grafic2, 0, [0,l],'-k'); %line at x=0 to distinguish     
        
        if isnan(N(0)), border0 = -N(0+1/10^10); else, border0 = -N(0); end
        if isnan(N(l)), borderl = -N(l-1/10^10); else, borderl = -N(l); end
        if double(N(0)) ~= 0, plot(h.multiplechoice.grafic2, [0,0],[0,border0],'color',colorM,'Linewidth',2); end
        if double(N(l)) ~= 0, plot(h.multiplechoice.grafic2, [l,l],[borderl,0],'color',colorM,'Linewidth',2); end

% GRAFIC 3 (the right one)       
        h.multiplechoice.grafic3 = axes('Parent', h.graficpanel, 'units','pixels');
        h.multiplechoice.grafic3.Position = [left, pos3+heightgrafic/2, widthgrafic, heightgrafic];
        set(h.multiplechoice.grafic3,'xtick',[]);
        set(h.multiplechoice.grafic3,'ytick',[]);
        set(h.multiplechoice.grafic3,'xcolor',[0.9294    0.9529    0.9882]);
        set(h.multiplechoice.grafic3,'ycolor',[0.9294    0.9529    0.9882]);  
        hold on;
       
        fplot(h.multiplechoice.grafic3,-(M(x)),[0,l],'color',colorM,'Linewidth',2); 
        fplot(h.multiplechoice.grafic3, 0, [0,l],'-k'); %line at x=0 to distinguish 
        if M(0) ~= 0, plot(h.multiplechoice.grafic3, [0,0],[0,-M(0)],'color',colorM,'Linewidth',2); end
        if M(l) ~= 0, plot(h.multiplechoice.grafic3, [l,l],[-M(l),0],'color',colorM,'Linewidth',2); end
            
end