function number = checkfracstring(frac)
[n,d] = numden(sym(frac));
if double(d) >= 9999 || round(double(n),1) > round(double(n))
    number = num2str(round(double(frac),4));
else
    if strcmp(frac,'1') || strcmp(frac,'-1')
        number = num2str(double(n));
    elseif double(d) == 1
        number = num2str(double(n));
    else
        number = strcat(num2str(double(n)),'/',num2str(double(d)));
    end
end
end