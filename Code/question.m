function h = question(Fig, h, str, rightanswer, helpstr, multiplechoice, type)
% if text: istext = 1, if not: istext=0
% str: e.g. 'Choose the value for ....'
% rightanswer as string
% helpstr: help for the students to answer the question
% multiplechoice = true/false
% type: 'point load', 'distributed load', 'moment', 'delta',
% 'beam-end-moment', 'virtual mechanism'

set(0, 'currentfigure', Fig);

width = h.questionpanel.Position(3);
height = h.questionpanel.Position(4);

if h.interactive
uicontrol('Parent',h.questionpanel,...
       'Style','text',...
       'Position',[20 height-20-20 150 20],...
       'String','Type your answer:',...
        'HorizontalAlignment','left',...
        'FontSize',14,...
        'FontName','Calibri',...
        'FontWeight','bold',...
        'Background',h.questionpanel.BackgroundColor);

lines = ceil(strlength(str)/35); 

txt = uicontrol('Parent',h.questionpanel,...
       'Style','text',...
       'Position',[20 height-55-20*lines 250 20*lines],...
       'String',str,...
        'HorizontalAlignment','left',...
        'FontSize',12,...
        'FontName','Calibri',...
        'FontWeight','normal',...
        'Background',h.questionpanel.BackgroundColor);
    
editfield = uicontrol('Parent',h.questionpanel,...
       'Style','edit',...
       'Position',[30 height-135 220 30],...
       'FontSize',11.5,...
       'keypressfcn',@ok);    

tryagain = uicontrol('Parent',h.questionpanel,...
       'Style','text',...
       'String', [],...
       'Position',[20 height-210 280 20],...
       'HorizontalAlignment','left',...
       'FontSize',11,...
       'FontName','Calibri',...                   
       'String', [],...
       'Background',h.questionpanel.BackgroundColor);
   
tryagainmultiplechoice = uicontrol('Parent',h.graficpanel,...
       'Style','text',...
       'String', [],...
       'Position',[20 50 280 20],...
       'HorizontalAlignment','left',...
       'FontSize',11,...
       'FontName','Calibri',...                   
       'String', [],...
       'Background',h.graficpanel.BackgroundColor);   
   
helpbutton = uicontrol('Parent',h.questionpanel,...
        'Style','push',...
        'Position',[30 height-130-25/2-10-25 60 25],...
        'String','Help',...
        'HorizontalAlignment','left',...
        'FontSize',11,...
        'FontName','Calibri',...
        'Backgroundcolor',[0.6392    0.8667    1.0000],...
        'Callback',@helpfcn);

resultbutton = uicontrol('Parent',h.questionpanel,...
        'Style','push',...
        'Position',[110 height-130-25/2-10-25 60 25],...
        'String','Result',...
        'HorizontalAlignment','left',...
        'FontSize',11,...
        'FontName','Calibri',...
        'Backgroundcolor',[0.6392    0.8667    1.0000],...
        'Callback', @answerfcn);

submitbutton = uicontrol('Parent',h.questionpanel,...
        'Style','push',...
        'Position',[190 height-130-25/2-10-25 60 25],...
        'String','OK',...
        'HorizontalAlignment','left',...
        'FontSize',11,...
        'FontName','Calibri',...
        'Backgroundcolor',[0.6392    0.8667    1.0000],...
        'Callback', @submitfcn);
end

% previouspage
uicontrol('Parent',h.questionpanel,...
        'Style','push',...
        'Position',[20, 20, 100, 20],...
        'String','previous page',...
        'HorizontalAlignment','left',...
        'FontSize',11,...
        'FontName','Calibri',...
        'Backgroundcolor',[0.6392    0.8667    1.0000],...
        'Callback', @previouspagefcn);

% nextpage
uicontrol('Parent',h.questionpanel,...
        'Style','push',...
        'Position',[width-120, 20, 100, 20],...
        'String','next page',...
        'HorizontalAlignment','left',...
        'FontSize',11,...
        'FontName','Calibri',...
        'Backgroundcolor',[0.6392    0.8667    1.0000],...
        'Callback', @nextpagefcn);

valuedone = 0;    
graficdone = 0;
h.newpanelnr = h.panelnr;
if strcmp(type,'empty')
    if h.interactive
    set(txt,'Visible','off');
    set(editfield, 'Visible', 'Off');
    set(tryagain, 'Visible', 'Off');
    set(helpbutton, 'Visible', 'Off');
    set(resultbutton, 'Visible', 'Off');
    set(submitbutton, 'Visible', 'Off');
    end
    uiwait(Fig);
end

if ~h.interactive
    if strcmp(type, 'stiffness') || strcmp(type, 'unitforce')
        uiresume(Fig);        
    elseif strcmp(type, 'delta')
        fields = fieldnames(h.obj);
        h.lastbottompos = h.posforequ;
        h.name = char(fields(h.number-1));
        delete(h.obj.(strcat('equation',num2str(h.number))));
        h.number = h.number - 1;
        uiresume(Fig);
    elseif strcmp(type,'point load')
        nrpointload = h.nrpointload + h.graficpointload;
        h.graficpointload = h.graficpointload + 1;
        set(h.obj.(strcat('pointload',num2str(nrpointload))),'Visible', 'On');
        set(h.obj.(strcat('pointload',num2str(nrpointload))).Children, 'Visible', 'On');
    elseif strcmp(type, 'distributed load')
        set(h.obj.(strcat('distributedload',num2str(h.number))),'Visible', 'On');
        set(h.obj.(strcat('distributedload',num2str(h.number))).Children, 'Visible', 'On');
    elseif strcmp(type, 'moment')
        set(h.obj.(strcat('moment',num2str(h.number))),'Visible', 'On');
        set(h.obj.(strcat('moment',num2str(h.number))).Children, 'Visible', 'On');
    else
        h.obj.(h.name).String = strrep(char(h.obj.(h.name).String), '...', rightanswer);
        h.obj.(h.name).Visible = 'On';
    end
    pause(0.8);
else    
    if multiplechoice
        if strcmp(type,'point load')
            nrpointload = h.nrpointload + h.graficpointload;
            h.graficpointload = h.graficpointload + 1;
        elseif strcmp(type, 'virtual mechanism')
            valuedone = 1;
            set(txt, 'Visible', 'Off');
            set(editfield, 'Visible', 'Off');
            set(tryagain, 'Visible', 'Off');
            set(helpbutton, 'Visible', 'Off');
            set(resultbutton, 'Visible', 'Off');
            set(submitbutton, 'Visible', 'Off');
        elseif strcmp(type, 'beam-end-moment')
            valuedone = 1;
            set(editfield, 'Visible', 'Off');
            set(tryagain, 'Visible', 'Off');
            set(helpbutton, 'Visible', 'On');
            set(resultbutton, 'Visible', 'Off');
            set(submitbutton, 'Visible', 'Off');
            set(h.equationpanel,'Visible','On');
            set(h.equationpanel.Children,'Visible','On');
        end

        set(h.graficpanel, 'Visible','On');
        set(h.multiplechoice1, 'Callback', @choice1chosen);
        set(h.multiplechoice2, 'Callback', @choice2chosen);
        set(h.multiplechoice3, 'Callback', @choice3chosen);
    end
            
    uiwait(Fig);
end

    function choice1chosen(event, hObject)
        set(tryagainmultiplechoice,'String','Try again!');
        set(h.multiplechoice2, 'Value', 0);
        if ~isempty(h.mc.help1)
            myicon = imread('Attention.jpg');
            Opt.Interpreter = 'tex';
            Opt.WindowStyle = 'normal';
            h.msg1 = msgbox(h.mc.help1, 'Attention','custom',myicon,[],Opt);
        end
    end
    
    function choice2chosen(event, hObject)
        set(tryagainmultiplechoice,'String','Try again!');
        set(h.multiplechoice1, 'Value', 0);    
        if ~isempty(h.mc.help2)
            myicon = imread('Attention.jpg');
            Opt.Interpreter = 'tex';
            Opt.WindowStyle = 'normal';
            h.msg1 = msgbox(h.mc.help2, 'Attention','custom',myicon,[],Opt);
        end
    end

    function choice3chosen(event, hObject)
        if strcmp(type,'beam-end-moment')
            delete(h.equationpanel);
            set(h.multiplechoice.text.str,'string',' ');
        end
        delete(h.multiplechoice1);
        delete(h.multiplechoice2);
        delete(h.multiplechoice3);
        
        delete(h.multiplechoice.grafic1);
        delete(h.multiplechoice.grafic2);
        
        if valuedone == 1
            set(h.multiplechoice.txt, 'Visible', 'Off');
            set(h.graficpanel, 'Visible', 'Off');
            if strcmp(type,'point load')
                set(h.obj.(strcat('pointload',num2str(nrpointload))),'Visible', 'On');
                set(h.obj.(strcat('pointload',num2str(nrpointload))).Children, 'Visible', 'On');
            elseif strcmp(type, 'distributed load')
                set(h.obj.(strcat('distributedload',num2str(h.number))),'Visible', 'On');
                set(h.obj.(strcat('distributedload',num2str(h.number))).Children, 'Visible', 'On');
            elseif strcmp(type, 'moment')
                set(h.obj.(strcat('moment',num2str(h.number))),'Visible', 'On');
                set(h.obj.(strcat('moment',num2str(h.number))).Children, 'Visible', 'On');
            end
            set(tryagainmultiplechoice, 'String', 'Well done!');
            if ~strcmp(type, 'virtual mechanism')
             set(tryagain, 'String', 'Well done! Go on.');
            end
            pause(h.pausequestion);
            set(tryagainmultiplechoice, 'String', []);
            set(editfield, 'String', []);
            set(txt, 'String', []);
            set(editfield, 'Visible', 'Off');
            set(editfield, 'Visible', 'Off');
            set(helpbutton, 'Visible', 'Off');
            set(resultbutton, 'Visible', 'Off');
            set(submitbutton, 'Visible', 'Off');
            delete(h.multiplechoice.grafic3); 
            if h.newpanelnr < h.panelnr
                panel_offset = h.panelnr-h.newpanelnr;
                set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                h.newpanelnr = h.newpanelnr + panel_offset;
            end
            uiresume(Fig);
        else
            set(tryagainmultiplechoice, 'String', 'Well done! Now answer the question above.');
            graficdone = 1;
        end
    end

    function [] = ok(event, hObject)
               p = get(gcf, 'CurrentCharacter');
               pause(0.1);
               if double(p) == 13
                    answeruser = num2str(readfrac(get(editfield,'String')));
                    if round(str2double(answeruser),3) == round(str2num(rightanswer),3)
                        if ~multiplechoice
                            if strcmp(type,'delta')
                                fields = fieldnames(h.obj);
                                if h.obj.(char(fields(h.number-1))).Position(2) - 30 > 40
                                    h.lastbottompos = h.obj.(char(fields(h.number-1))).Position(2) - 30;
                                else
                                    h.lastbottompos = h.height - 10;
                                end
                                h.name = char(fields(h.number-1));
                                delete(h.obj.(strcat('equation',num2str(h.number))));
                                h.number = h.number - 1;
                                pause(h.pausequestion);
                                set(editfield, 'String', []);
                                set(txt, 'String', []);     
                                set(editfield, 'Visible', 'Off');
                                set(editfield, 'Visible', 'Off');
                                set(helpbutton, 'Visible', 'Off');
                                set(resultbutton, 'Visible', 'Off');
                                set(submitbutton, 'Visible', 'Off');
                                if h.newpanelnr < h.panelnr
                                    panel_offset = h.panelnr-h.newpanelnr;
                                    set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                                    set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                                    set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                                    set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                                    h.newpanelnr = h.newpanelnr + panel_offset;
                                end
                                uiresume(Fig);
                            elseif strcmp(type, 'stiffness') || strcmp(type, 'unitforce')
                                pause(h.pausequestion);
                                set(editfield, 'String', []);
                                set(txt, 'String', []);               
                                set(editfield, 'Visible', 'Off');
                                set(editfield, 'Visible', 'Off');
                                set(helpbutton, 'Visible', 'Off');
                                set(resultbutton, 'Visible', 'Off');
                                set(submitbutton, 'Visible', 'Off');
                                if h.newpanelnr < h.panelnr
                                    panel_offset = h.panelnr-h.newpanelnr;
                                    set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                                    set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                                    set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                                    set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                                    h.newpanelnr = h.newpanelnr + panel_offset;
                                end
                                uiresume(Fig);
                            else
                                h.obj.(h.name).String = strrep(char(h.obj.(h.name).String), '...', rightanswer);
                            end
                        end
                        
                        if graficdone == 1 && multiplechoice
                            set(h.multiplechoice.txt, 'Visible', 'Off');
                            set(h.graficpanel, 'Visible', 'Off');
                            if strcmp(type,'point load')
                                set(h.obj.(strcat('pointload',num2str(nrpointload))),'Visible', 'On');
                                set(h.obj.(strcat('pointload',num2str(nrpointload))).Children, 'Visible', 'On');
                            elseif strcmp(type, 'distributed load')
                                set(h.obj.(strcat('distributedload',num2str(h.number))),'Visible', 'On');
                                set(h.obj.(strcat('distributedload',num2str(h.number))).Children, 'Visible', 'On');
                            elseif strcmp(type, 'moment')
                                set(h.obj.(strcat('moment',num2str(h.number))),'Visible', 'On');
                                set(h.obj.(strcat('moment',num2str(h.number))).Children, 'Visible', 'On');
                            end
                            if ~strcmp(type, 'virtual mechanism')
                             set(tryagain, 'String', 'Well done! Go on.');
                            end
                            pause(h.pausequestion);
                            set(editfield, 'String', []);
                            set(txt, 'String', []);
                            set(editfield, 'Visible', 'Off');
                            set(helpbutton, 'Visible', 'Off');
                            set(resultbutton, 'Visible', 'Off');
                            set(submitbutton, 'Visible', 'Off');
                            set(tryagainmultiplechoice, 'String', []);
                            delete(h.multiplechoice.grafic3);
                            if h.newpanelnr < h.panelnr
                                panel_offset = h.panelnr-h.newpanelnr;
                                set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                                set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                                h.newpanelnr = h.newpanelnr + panel_offset;
                            end
                            uiresume(Fig);
                        elseif multiplechoice
                            set(tryagain,'String','Well done! Now find the right moment line.');
                            valuedone = 1;
                        else
                            pause(h.pausequestion);
                            set(editfield, 'String', []);
                            set(txt, 'String', []);
                            set(editfield, 'Visible', 'Off');
                            set(helpbutton, 'Visible', 'Off');
                            set(resultbutton, 'Visible', 'Off');
                            set(submitbutton, 'Visible', 'Off');
                            if h.newpanelnr < h.panelnr
                                panel_offset = h.panelnr-h.newpanelnr;
                                set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                                set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                                h.newpanelnr = h.newpanelnr + panel_offset;
                            end
                            uiresume(Fig);
                        end
                    else
                        set(tryagain,'String','Try again!');
                    end
               end
    end

    function [] = submitfcn(event, hObject)
                   answeruser = num2str(readfrac(get(editfield,'String')));
                    if round(str2double(answeruser),3) == round(str2num(rightanswer),3)
                        if ~multiplechoice
                            if strcmp(type,'delta')
                                fields = fieldnames(h.obj);
                                if h.obj.(char(fields(h.number-1))).Position(2) - 30 > 40
                                    h.lastbottompos = h.obj.(char(fields(h.number-1))).Position(2) - 30;
                                else
                                    h.lastbottompos = h.height - 10;
                                end
                                h.name = char(fields(h.number-1));
                                delete(h.obj.(strcat('equation',num2str(h.number))));
                                h.number = h.number - 1;
                                pause(h.pausequestion);
                                set(editfield, 'String', []);
                                set(txt, 'String', []);     
                                set(editfield, 'Visible', 'Off');
                                set(editfield, 'Visible', 'Off');
                                set(helpbutton, 'Visible', 'Off');
                                set(resultbutton, 'Visible', 'Off');
                                set(submitbutton, 'Visible', 'Off');
                                if h.newpanelnr < h.panelnr
                                    panel_offset = h.panelnr-h.newpanelnr;
                                    set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                                    set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                                    set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                                    set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                                    h.newpanelnr = h.newpanelnr + panel_offset;
                                end
                                uiresume(Fig);
                            elseif strcmp(type, 'stiffness') || strcmp(type, 'unitforce')
                                pause(h.pausequestion);
                                set(editfield, 'String', []);
                                set(txt, 'String', []);               
                                set(editfield, 'Visible', 'Off');
                                set(editfield, 'Visible', 'Off');
                                set(helpbutton, 'Visible', 'Off');
                                set(resultbutton, 'Visible', 'Off');
                                set(submitbutton, 'Visible', 'Off');
                                if h.newpanelnr < h.panelnr
                                    panel_offset = h.panelnr-h.newpanelnr;
                                    set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                                    set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                                    set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                                    set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                                    h.newpanelnr = h.newpanelnr + panel_offset;
                                end
                                uiresume(Fig);
                            else
                                h.obj.(h.name).String = strrep(char(h.obj.(h.name).String), '...', rightanswer);
                            end
                        end
                        
                        if graficdone == 1 && multiplechoice
                            set(h.multiplechoice.txt, 'Visible', 'Off');
                            set(h.graficpanel, 'Visible', 'Off');
                            if strcmp(type,'point load')
                                set(h.obj.(strcat('pointload',num2str(nrpointload))),'Visible', 'On');
                                set(h.obj.(strcat('pointload',num2str(nrpointload))).Children, 'Visible', 'On');
                            elseif strcmp(type, 'distributed load')
                                set(h.obj.(strcat('distributedload',num2str(h.number))),'Visible', 'On');
                                set(h.obj.(strcat('distributedload',num2str(h.number))).Children, 'Visible', 'On');
                            elseif strcmp(type, 'moment')
                                set(h.obj.(strcat('moment',num2str(h.number))),'Visible', 'On');
                                set(h.obj.(strcat('moment',num2str(h.number))).Children, 'Visible', 'On');
                            end
                            if ~strcmp(type, 'virtual mechanism')
                             set(tryagain, 'String', 'Well done! Go on.');
                            end
                            pause(h.pausequestion);
                            set(editfield, 'String', []);
                            set(txt, 'String', []);
                            set(editfield, 'Visible', 'Off');
                            set(helpbutton, 'Visible', 'Off');
                            set(resultbutton, 'Visible', 'Off');
                            set(submitbutton, 'Visible', 'Off');
                            set(tryagainmultiplechoice, 'String', []);
                            delete(h.multiplechoice.grafic3);
                            if h.newpanelnr < h.panelnr
                                panel_offset = h.panelnr-h.newpanelnr;
                                set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                                set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                                h.newpanelnr = h.newpanelnr + panel_offset;
                            end
                            uiresume(Fig);
                        elseif multiplechoice
                            set(tryagain,'String','Well done! Now find the right moment line.');
                            valuedone = 1;
                        else
                            pause(h.pausequestion);
                            set(editfield, 'String', []);
                            set(txt, 'String', []);
                            set(editfield, 'Visible', 'Off');
                            set(helpbutton, 'Visible', 'Off');
                            set(resultbutton, 'Visible', 'Off');
                            set(submitbutton, 'Visible', 'Off');
                            if h.newpanelnr < h.panelnr
                                panel_offset = h.panelnr-h.newpanelnr;
                                set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                                set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                                h.newpanelnr = h.newpanelnr + panel_offset;
                            end
                            uiresume(Fig);
                        end
                    else
                        set(tryagain,'String','Try again!');
                    end
    end

    function helpfcn(event, hObject)
        if ~strcmp(helpstr,' ')
            myicon=imread('think.jpg');
            if strcmp(type,'beam-end-moment') || strcmp(type, 'stiffness')
                Opt.Interpreter = 'tex';
                Opt.WindowStyle = 'modal';
                helpbox = msgbox(helpstr,'Help','custom',myicon,[],Opt);
                helpbox.Position(3) = 300;
            else
            msgbox(helpstr,'Help','custom',myicon); 
            end
        else
            myicon=imread('dontknow.jpg');
            msgbox('Sorry we cannot help you.','Help','custom',myicon); 
        end
    end

    function answerfcn(event, hObject)
        if ~multiplechoice
            if strcmp(type,'delta')
                fields = fieldnames(h.obj);
                if h.obj.(char(fields(h.number-1))).Position(2) - 30 > 40
                    h.lastbottompos = h.obj.(char(fields(h.number-1))).Position(2) - 30;
                else
                    h.lastbottompos = h.height - 10;
                end
                h.name = char(fields(h.number-1));
                delete(h.obj.(strcat('equation',num2str(h.number))));
                h.number = h.number - 1;
            elseif strcmp(type, 'stiffness') || strcmp(type, 'unitforce')
                pause(h.pausequestion);
                set(editfield, 'String', []);
                set(txt, 'String', []);       
                set(editfield, 'Visible', 'Off');
                set(helpbutton, 'Visible', 'Off');
                set(resultbutton, 'Visible', 'Off');
                set(submitbutton, 'Visible', 'Off');
                if h.newpanelnr < h.panelnr
                    panel_offset = h.panelnr-h.newpanelnr;
                    set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                    set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                    set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                    set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                    h.newpanelnr = h.newpanelnr + panel_offset;
                end
                uiresume(Fig);
            else
                pause(h.pausequestion);                
                h.obj.(h.name).String = strrep(char(h.obj.(h.name).String), '...', rightanswer);
            end
            set(editfield, 'String', []);
            set(txt, 'String', []);          
            set(editfield, 'Visible', 'Off');
            set(helpbutton, 'Visible', 'Off');
            set(resultbutton, 'Visible', 'Off');
            set(submitbutton, 'Visible', 'Off');
            if h.newpanelnr < h.panelnr
                panel_offset = h.panelnr-h.newpanelnr;
                set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                h.newpanelnr = h.newpanelnr + panel_offset;
            end
            uiresume(Fig);
        elseif graficdone == 1 && multiplechoice
            set(h.graficpanel, 'Visible', 'Off');
            if strcmp(type,'point load')
                set(h.obj.(strcat('pointload',num2str(nrpointload))),'Visible', 'On');
                set(h.obj.(strcat('pointload',num2str(nrpointload))).Children, 'Visible', 'On');
            elseif strcmp(type, 'distributed load')
                set(h.obj.(strcat('distributedload',num2str(h.number))),'Visible', 'On');
                set(h.obj.(strcat('distributedload',num2str(h.number))).Children, 'Visible', 'On');
            elseif strcmp(type, 'moment')
                set(h.obj.(strcat('moment',num2str(h.number))),'Visible', 'On');
                set(h.obj.(strcat('moment',num2str(h.number))).Children, 'Visible', 'On');
            end
            set(editfield, 'String', []);
            set(txt, 'String', []);
            set(editfield, 'Visible', 'Off');
            set(helpbutton, 'Visible', 'Off');
            set(resultbutton, 'Visible', 'Off');
            set(submitbutton, 'Visible', 'Off');
            set(tryagainmultiplechoice, 'String', []);
            delete(h.multiplechoice.grafic3);
            if ~strcmp(type, 'virtual mechanism')
             set(tryagain, 'String', 'Well done! Go on.');
            end
            pause(h.pausequestion);
            if h.newpanelnr < h.panelnr
                panel_offset = h.panelnr-h.newpanelnr;
                set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))),'Visible','On');
                set(h.(strcat('panel',num2str(h.newpanelnr+panel_offset))).Children,'Visible','On');
                h.newpanelnr = h.newpanelnr + panel_offset;
            end
            uiresume(Fig);
        else
            set(editfield, 'String', rightanswer);
            set(tryagain,'String','Now find the right moment line.');
            valuedone = 1;
        end
    end

    function previouspagefcn(event, hObject)
            if h.newpanelnr > 1
                set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                set(h.(strcat('panel',num2str(h.newpanelnr-1))).Children,'Visible','On');
                set(h.(strcat('panel',num2str(h.newpanelnr-1))),'Visible','On');
                h.newpanelnr = h.newpanelnr - 1;
            end
            guidata(Fig, h)
    end

    function nextpagefcn(event, hObject)

            if h.newpanelnr < h.panelnr
                set(h.(strcat('panel',num2str(h.newpanelnr))),'Visible','Off');
                set(h.(strcat('panel',num2str(h.newpanelnr))).Children,'Visible','Off');
                set(h.(strcat('panel',num2str(h.newpanelnr+1))),'Visible','On');
                set(h.(strcat('panel',num2str(h.newpanelnr+1))).Children,'Visible','On');
                h.newpanelnr = h.newpanelnr + 1;
            end
            guidata(Fig, h)   
    end
end
