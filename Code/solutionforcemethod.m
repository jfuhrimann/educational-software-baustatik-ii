word = actxserver('Word.Application');                          %start Word
word.Visible =1;                                                             %make Word Visible
document=word.Documents.Add;                                 %create new Document
selection=word.Selection;                                              %set Cursor
selection.Font.Name='Calibri';                                       %set Font
selection.Font.Size=12;                                                 %set Size

margin = 28.34646;                                                        %1cm is circa 28.34646 points
selection.Pagesetup.RightMargin=2*margin;                %set right Margin to 2cm
selection.Pagesetup.LeftMargin=2*margin;                  %set left Margin to 2cm
selection.Pagesetup.TopMargin=2*margin;                  %set top Margin to 2cm
selection.Pagesetup.BottomMargin=2*margin;             %set bottom Margin to 2cm

selection.Paragraphs.LineUnitAfter=0.5;                      %sets the amount of spacing
                                            %between paragraphs(in gridlin

heading1 = document.Styles.Item('Heading 1');
heading1.Font.Size = 20;
selection.TypeText('Force method for beam 1-2:');   
selection.Style='Heading 1';
selection.TypeParagraph;

% H1 = document.Styles.Item('Heading 1')
% H1.Font.Name = 'Garamond';
% H1.Font.Size = 20;
% H1.Font.Bold = 1;
% H1.Font.TextColor.RGB=60000; % some ugly color green

selection.TypeParagraph
selection.TypeText('That is all for today!')
selection.TypeParagraph;                    %line break
selection.TypeText('Static determination:');                                             
prompt = {'Enter matrix size:','Enter colormap name:'};
title = 'Input';
dims = [1 35];
definput = {'20','hsv'};
answer = inputdlg(prompt,title,dims,definput)               
                                            
                                            
% selection.TypeText('Hello World!');         %write Text
% selection.TypeParagraph;                    %line break
% x = 0:pi/100:2*pi;
% y = sin(x);
% figtoprint=figure(1);                       %opens new figure window
% plot(x,y);
% title('Title');
% xlabel('x-label');
% ylabel('y-label');
% grid on;
% print(figtoprint,'-dmeta');                 %print figure to clipboard
% invoke(word.Selection,'Paste');             %paste figure to Word
% set (0, 'DefaultAxesFontSize', 8, ...
%  'DefaultAxesFontName', 'Times');
% figure;
% set (gcf, 'Units', 'centimeter');
% pos = get (gcf, 'Position');
% pos(3) = 10;
% pos(4) = 8;
% set (gcf, 'Position', pos);
% set (gca, 'Units', 'centimeter');
% set (gca, 'Position', [1.5 1.2 8 6]);
% theta=0:2*pi/200:2*pi;
% plot (sin(theta),cos(theta));
% axis ([0 1.05 -0.2 1.05]);
% xlabel ('\theta_i');
% ylabel ('amplitude');
% set (gcf, 'PaperPositionMode', 'auto');
% print -depsc SamplePlot.eps;
% print(figtoprint,'-dmeta');                 %print figure to clipboard
% invoke(word.Selection,'Paste');             %paste figure to Word
% figtoprint=figure(2)
% 
% print(figtoprint,'-dmeta');                 %print figure to clipboard
% invoke(word.Selection,'Paste');             %paste figure to Word