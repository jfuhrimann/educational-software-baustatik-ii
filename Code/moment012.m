function [M1, M2, delta11, delta22, delta12] = moment12(ca, N, l, cf_a, cf_b,EI)
    syms x

    if N == 1 && (strcmp(ca,'fixed') || strcmp(ca,'torsional spring'))
        M1 = @(x) -1+x/l;
        M2 = @(x) 0;
    elseif N == 1
        M1 = @(x) 0;
        M2 = @(x) x/l;
    elseif N == 2
        M1 = @(x) -1+x/l;
        M2 = @(x) x/l;
    else
        M1 = @(x) 0;
        M2 = @(x) 0;
    end

    delta11 = int(M1(x)*M1(x)/EI,x,0,l) + cf_b;
    delta22 = int(M2(x)*M2(x)/EI,x,0,l) + cf_a;
    delta12 = int(M1(x)*M2(x)/EI,x,0,l);                        
    
end