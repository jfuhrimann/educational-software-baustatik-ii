function result = finalmoments(handles)
% This function plots the definitive momentlines into the figure of the
% system.
pointnames = handles.pointnames;
beamnames = handles.beamnames;
for i = 1 : numel(beamnames)
    beam = char(beamnames(i));
    l = handles.beams.(beam).l;
    n = handles.beams.(beam).n;
    t = handles.beams.(beam).t;
    ra = handles.points.(pointname(handles.beams.(beam).beampoints(1))).r;
    rb = handles.points.(pointname(handles.beams.(beam).beampoints(2))).r;
    % Find the maximal beamendmoment:
    Mmax = 0;
    for pt = 1 : numel(pointnames)
        current = char(pointnames(pt));
        neighbours = handles.points.(current).neighbours;
        for ne = 1 : numel(neighbours)
            Mmax = max(Mmax,abs(approximate_equation(handles.points.(current).(pointname(neighbours(ne))).M)));
        end
    end
    
    if Mmax ~= 0
    imscale = (handles.scalexupper-handles.scalexlower)/(5*Mmax);
    else 
        imscale = handles.imscale/5;
    end
    
    x = linspace(0,l,100);
    y = zeros(2,numel(x)+2);
    for j = 1 : numel(x)
    y(:,j) = ra + x(j)*t + handles.beams.(beam).Mx(x(j))*n*imscale;
    end
    y(:,numel(x) + 1) = rb;
    y(:,numel(x) + 2) = ra;
    handles.beams.(beam).Mplot = patch(y(1,:),y(2,:),[0.9098 0.9412 1.0000]);
    set(handles.beams.(beam).Mplot,'EdgeColor','b')
    uistack(handles.beams.(beam).Mplot,'bottom');
    %Labels of the values
    handles = labelmomentlines(ra,rb,n,t,imscale,beam,handles);

end
%Loadgraphics at bottom
for i = 1 : numel(beamnames)
    beam = char(beamnames(i));
    if isfield(handles.beams.(beam),'loads')
        if isfield(handles.beams.(beam).loads,'qquiver1')
            uistack(handles.beams.(beam).loads.qquiver1,'bottom');
            uistack(handles.beams.(beam).loads.qpatch1,'bottom');
        end
        if isfield(handles.beams.(beam).loads,'qquiver2')
            uistack(handles.beams.(beam).loads.qquiver2,'bottom');
            uistack(handles.beams.(beam).loads.qpatch2,'bottom');
        end
    end
end
%Constraintgraphics at bottom.
for i = 1 : numel(beamnames)
    beam = char(beamnames(i));
    if isfield(handles.beams.(beam),'leftim')
        uistack(handles.beams.(beam).leftim,'bottom');
    end
    if isfield(handles.beams.(beam),'rightim')
        uistack(handles.beams.(beam).rightim,'bottom');
    end
end
result = handles;
end