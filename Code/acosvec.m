function alpha = acosvec(u,v)
%Calculates the angle between two vectors with positive direction
%conter-clockwise: By which angle has v to be rotated in order to be
%collinear with u?
alphacheck = acosd(dot(u,v)/(norm(u)*norm(v)));
new_v = [cosd(alphacheck) -sind(alphacheck);sind(alphacheck) cosd(alphacheck)]*v;
if iscollinear(round(u,10),round(new_v,10))
    alpha = alphacheck;
else
    alpha = -alphacheck;
end
end