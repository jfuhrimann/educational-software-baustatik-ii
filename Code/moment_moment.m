function [M0, Vatot, Vbtot, diagrams] = moment_moment(l,sm,M,knottype_a,knottype_b,M0, diagrams)

syms x 

    if strcmp(knottype_b,'free')
         Va = 0;
         Mfixed = M;
     elseif strcmp(knottype_a,'free')
         Va = 0;
         Mfixed = 0;
     else
         Va = M/l;
         Mfixed = 0;
    end        
    
    Vatot = Va;
    Vbtot = -Va;
    
    if sm == l
        M01(x) = piecewise(x>=0 & x<=l, Va*x);
        M02(x) = piecewise(x>=0 & x<=l, 0);
        M00(x) = piecewise(x>=0 & x<=l, Mfixed) + M01(x) + M02(x);
    else
        M01(x) = piecewise(x>=0 & x<sm, Va*x, x>=sm & x<=l, 0);
        M02(x) = piecewise(x>=0 & x<sm, 0, x>=sm & x<=l, M01(0) + Va*sm - M + Va*(x-sm));
        M00(x) = piecewise(x>=0 & x<=l, Mfixed) + M01(x) + M02(x);
    end

    diagrams.m = diagrams.m + M00(x);
    
    M0(x) = M00(x) + M0(x);

end