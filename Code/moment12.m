function [M1, M2, delta11, delta22, delta12] = moment12(ca, N, l, sh, cf_as, cf_bs, cf_at, cf_bt, EI)
syms M1(x) M2(x)

    if N == 1 && (strcmp(ca,'fixed') || strcmp(ca,'torsional spring')) && sh < 0
        M1(x) = piecewise(x>=0 & x<=l, -1+x/l);
        M2(x) = piecewise(x>=0 & x<=l, 0);
    elseif N == 1 && sh < 0
        M1(x) = piecewise(x>=0 & x<=l, 0);
        M2(x) = piecewise(x>=0 & x<=l, x/l);
    elseif N == 2 && sh < 0
        M1(x) = piecewise(x>=0 & x<=l, -1+x/l);
        M2(x) = piecewise(x>=0 & x<=l, x/l);
    elseif N ==1 && sh > 0
        M1(x) = piecewise(x>=0 & x<=l, 0);
        M2(x) = piecewise(x>=0 & x<=l, x/(l-sh)+1-l/(l-sh));               
    else
        M1(x) = piecewise(x>=0 & x<=l, 0);
        M2(x) = piecewise(x>=0 & x<=l, 0);
    end
    
    if sh < 0
        delta11 = int(M1(x)*M1(x)/EI,x,0,l) + cf_at*M1(0)*M1(0) + cf_bt*M1(l)*M1(l)+ cf_bs/l^2;
        delta22 = int(M2(x)*M2(x)/EI,x,0,l) + cf_bt*M2(l)*M2(l) + cf_at*M2(0)*M2(0) + cf_as/l^2;
    else
        delta11 = int(M1(x)*M1(x)/EI,x,0,l);
        delta22 = int(M2(x)*M2(x)/EI,x,0,l) + cf_bt*M2(l)*M2(l) + cf_at*M2(0)*M2(0);
    end
    
    delta12 = int(M1(x)*M2(x)/EI,x,0,l);                        

end