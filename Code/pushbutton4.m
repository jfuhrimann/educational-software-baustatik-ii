function result = pushbutton4(handles)

handles.togglebutton7.Visible = 'off';

if strcmp(handles.unit,'kN'), unit = 'm'; else, unit = 'L'; end

set(handles.edit1, 'Visible','Off'); 
set(handles.info2,'Visible','off');
  set(handles.edit2, 'Visible','Off'); 
  set(handles.numberofneighbours, 'Visible','Off'); 
  set(handles.neighboursok, 'Visible','Off'); 
  set(handles.neighbours, 'Visible','Off'); 
  set(handles.axes2, 'Visible','Off');
  set(handles.axes2.Children,'Visible','Off');
  set(handles.edit8, 'Visible','Off'); 
  set(handles.edit12, 'Visible','Off'); 
  set(handles.edit29, 'Visible','Off'); 
  set(handles.edit31, 'Visible','Off'); 
  set(handles.edit32, 'Visible','Off'); 
  set(handles.addpoint, 'Visible','Off'); 
  set(handles.pushbutton4, 'Visible','Off'); 
  set(handles.edit12, 'Visible','Off');  
  
  set(handles.cover, 'Visible','On');  
  
  set(handles.text14, 'Visible','On');
  set(handles.info,'Visible','On');
  set(handles.text15, 'Visible','On');  
  set(handles.text16, 'Visible','On');  
  set(handles.text17, 'Visible','On');  
  set(handles.text18, 'Visible','On');  
  set(handles.text19, 'Visible','On');  
  set(handles.text21, 'Visible','On');  
  set(handles.text22, 'Visible','On');  
  set(handles.text23, 'Visible','On');  
  set(handles.text25, 'Visible','On');
  set(handles.text26, 'Visible','On');  
  set(handles.text27, 'Visible','On');  
  set(handles.text28, 'Visible','On');  
  set(handles.text29, 'Visible','On');  
  set(handles.text30, 'Visible','On');  
  set(handles.text31, 'Visible','On');  
  set(handles.text32, 'Visible','On');  
  set(handles.text33, 'Visible','On');
  set(handles.text36, 'Visible','On');
  set(handles.text37, 'Visible','On');
  set(handles.edit13, 'Visible','On');  
  set(handles.edit18, 'Visible','On');  
  set(handles.edit19, 'Visible','On');  
  set(handles.edit21, 'Visible','On');  
  set(handles.edit22, 'Visible','On');  
  set(handles.edit24, 'Visible','On');  
  set(handles.edit25, 'Visible','On');  
  set(handles.edit26, 'Visible','On');  
  set(handles.edit27, 'Visible','On');  
  set(handles.pushbutton5, 'Visible','On');  
  set(handles.pushbutton6, 'Visible','On');  
  set(handles.pushbutton7, 'Visible','On');
  set(handles.uitable5, 'Visible','On');
  set(handles.radiobutton2,'Visible','On');
  set(handles.radiobutton4,'Visible','On');
  set(handles.radiobutton5,'Visible','On');
  set(handles.radiobutton6,'Visible','On');
  set(handles.save_geometry,'Visible','On');
    handles.pushbutton7.Visible = 'off';
    handles.save_geometry.Visible = 'off';
    handles.togglebutton5.Visible = 'off';
for i=1:handles.totalpoints
    pointi=pointname(i);
    
    for j=1:numel(handles.points.(pointi).neighbours)
        if handles.points.(pointi).neighbours(j)>i
        beamstruct=beamname(handles.points.(pointi).pointnr,handles.points.(pointi).neighbours(j));
        
        beampoints='beampoints';
        beampointsvalue=[i,handles.points.(pointi).neighbours(j)]; %vector of the beam with [initial point, end point]
        
        %length of the beam
        l='l';
        vnp = handles.points.(char(pointname(handles.points.(pointi).neighbours(j)))).r - handles.points.(pointi).r; % vnp = Vector from Neighbour to first Point
        lvalue=norm(vnp);
        
        %vector tangential to the beam
        t='t';
        tvalue=vnp/lvalue; %unit vector
        
        %vector normal to the beam
        n='n';
        nvalue=[tvalue(2);-tvalue(1)];
                
        handles.beams.(beamstruct)=struct(beampoints,beampointsvalue,l,lvalue,t,tvalue,n,nvalue);
        handles.beams.(beamstruct).changethisbeam = false;
        end
    end
end

handles.beamnames=fieldnames(handles.beams); %vector with all the beams

handles.temp=1; %tempor�re Variable speichern, um zu wissen, an welcher Stelle wir uns im point1.neighbours befinden (wird im Verlauf vom Programm erh�ht)
a=num2str(handles.points.point1.neighbours(1)); % take first neighbour of point 1

% initialization (first beam should appear in text field)
set(handles.text15, 'string', '1');
set(handles.text25, 'string', a);
set(handles.text37,'string',{[num2str(handles.beams.(char(handles.beamnames(handles.temp))).l) unit]});
set(handles.edit22,'string','L');

%Plot normal Vector of the first beam
r1=handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).r(1);
r2=handles.points.(pointname(handles.beams.(char(handles.beamnames(handles.temp))).beampoints(1))).r(2);
pn1 = handles.beams.(char(handles.beamnames(handles.temp))).n(1)*0.2*handles.imscale^0.8;
pn2 = handles.beams.(char(handles.beamnames(handles.temp))).n(2)*0.2*(handles.imscale)^0.8;
handles.nimg=quiver(r1-pn1/2,r2-pn2/2,1.5*pn1,1.5*pn2,'k');
handles.ntext=text(r1+pn1+0.05*handles.imscale^0.8,r2+pn2+0.05*(handles.imscale)^0.8,'n');

%Plot tangential vector of the first beam
pt1 = handles.beams.(char(handles.beamnames(handles.temp))).t(1)*0.2*handles.imscale^0.8;
pt2 = handles.beams.(char(handles.beamnames(handles.temp))).t(2)*0.2*handles.imscale^0.8;
handles.timg=quiver(r1,r2,pt1,pt2,'k');
handles.ttext=text(r1+pt1+0.05*handles.imscale^0.8,r2+pt2+0.05*(handles.imscale)^0.8,'t');


%plot positive direction of the angle (semi-circle)
    %Find the starting angle (alpha(x direction)=0)
    if pn1 >= 0 && pn2 >= 0             %first sector
        alpha = atand(pt2/pt1);
    elseif pn1 < 0 && pn2 >= 0
        alpha = 180 + atand(pt2/pt1);   %second sector
    elseif pn1 < 0 && pn2 < 0
        alpha = 180 + atand(pt2/pt1);   %third sector
    elseif pn1 >= 0 && pn2 <= 0
        alpha = 360 + atand(pt2/pt1);   %fourth sector
    end
    
    %prepare the circle
    th = linspace( alpha - 90, alpha + 90, 100);
    R = 0.25*norm([pn1;pn2]);
    x = -R*cosd(th) + r1;
    y = -R*sind(th) + r2;
    
    %prepare the arrow
    mpoly = [r1;r2] + [0.25*pn1; 0.25*pn2];
    spoly = mpoly + 0.4*R*[cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)]*[-1/sqrt(2);-1/sqrt(2)];
    epoly = mpoly + 0.4*R*[cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)]*[-1/sqrt(2);1/sqrt(2)];
    px=[spoly(1),mpoly(1),epoly(1)];
    py=[spoly(2),mpoly(2),epoly(2)];
    handles.acircplot = plot(x,y,'k');
    handles.apolyplot =  plot(px,py,'k');
    handles.atext = text(r1 - 0.5*pt1 ,r2 - 0.5*pt2, '\alpha');
result = handles;
end