function approxeqn = approximate_equation(eqn)
%This function replaces all of the symbolic variables of an equation by 1
%(for example units). The result approxeqn is of type sym.
allvar = symvar(eqn);
for i = 1 : numel(allvar)
    eqn = subs(eqn,allvar(i),1);
end
approxeqn = eqn;
end