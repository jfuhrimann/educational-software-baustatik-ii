function result = drawgrid(origin,xlimits,ylimits,meshwidth,handles)
%Draws the grid as specified by xlimits, ylimitsy, meshwidth to the axes
%with handle: handles.axes1
axes(handles.axes1);
hold on
handles.axes1.XLim = [xlimits(1),xlimits(2)];     
handles.axes1.YLim = [ylimits(1),ylimits(2)];   
axis equal;
set(getpoints,'WindowButtonDownFcn',@callsolution);
%grid lines // y from origin onwards to positive direction
xcoords = [];
ycoords = [];
numberxu = ceil((handles.axes1.XLim(2)-origin(1))/meshwidth) + 1;
for i = 1 : numberxu
    handles.xline(i) = line([origin(1)+(i-1)*meshwidth, origin(1)+(i-1)*meshwidth],[handles.axes1.YLim(1),handles.axes1.YLim(2)],'Color',[.9,.9,.9]);
    xcoords = [xcoords,origin(1)+(i-1)*meshwidth];
end
numberxd = ceil((origin(1)-handles.axes1.XLim(1))/meshwidth);
for i = 1 : numberxd
    handles.xline(numberxu + 1 + i) = line([origin(1)-i*meshwidth, origin(1)-i*meshwidth],[handles.axes1.YLim(1),handles.axes1.YLim(2)],'Color',[.9,.9,.9]);
    xcoords = [origin(1)-i*meshwidth,xcoords];
end    
numberyu = ceil((handles.axes1.YLim(2)-origin(2))/meshwidth) + 1;
for i = 1 : numberyu
    handles.yline(i) = line([handles.axes1.XLim(1),handles.axes1.XLim(2)],[origin(2)+(i-1)*meshwidth, origin(2)+(i-1)*meshwidth],'Color',[.9,.9,.9]); 
    ycoords = [origin(2)+(i-1)*meshwidth;ycoords];
end
numberyd = ceil((origin(1)-handles.axes1.XLim(1))/meshwidth);
for i = 1 : numberyd
    handles.yline(numberyu + 1 + i) = line([handles.axes1.XLim(1),handles.axes1.XLim(2)],[origin(2)-i*meshwidth, origin(2)-i*meshwidth],'Color',[.9,.9,.9]);
    ycoords = [ycoords;origin(2)-i*meshwidth];
end
handles.xcoords = xcoords;
handles.ycoords = ycoords;
result = handles;
end