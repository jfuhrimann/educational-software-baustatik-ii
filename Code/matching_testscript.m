clear
clc
% Initialise some kind of system
% 
%---------------------
%5  |2        3|     6
%   |          |
%   |1        4|


% handles.points.point1.r = [1;0];
% handles.points.point1.pointnr = 1;
% handles.points.point1.knottype = 'connection';
% handles.points.point1.constrainttype = 'fixed';
% handles.points.point2.r = [1;1];
% handles.points.point2.pointnr = 2;
% handles.points.point2.knottype = 'connection';
% handles.points.point2.constrainttype = 'none';
% handles.points.point3.r = [2;1];
% handles.points.point3.pointnr = 3;
% handles.points.point3.knottype = 'connection';
% handles.points.point3.constrainttype = 'none';
% handles.points.point4.r = [2;0];
% handles.points.point4.pointnr = 4;
% handles.points.point4.knottype = 'connection';
% handles.points.point4.constrainttype = 'fixed';
% handles.points.point5.r = [0;1];
% handles.points.point5.pointnr = 5;
% handles.points.point5.knottype = 'hinge';
% handles.points.point5.constrainttype = 'roller';
% handles.points.point6.r = [3;1];
% handles.points.point6.pointnr = 6;
% handles.points.point6.knottype = 'hinge';
% handles.points.point6.constrainttype = 'roller';
% 
% handles.points.point1.pvec = [0 0; 0 0];
% handles.points.point1.tvec = [1 0; 0 1];
% handles.points.point1.fix = true;
% handles.points.point1.neighbours = [2];
% handles.points.point2.pvec = [];
% handles.points.point2.tvec = [];
% handles.points.point2.fix = false;
% handles.points.point2.neighbours = [1 3 5];
% handles.points.point3.pvec = [];
% handles.points.point3.tvec = [];
% handles.points.point3.fix = false;
% handles.points.point3.neighbours = [2 4 6];
% handles.points.point4.pvec = [3 3; 0 0];
% handles.points.point4.tvec = [1 0; 0 1];
% handles.points.point4.fix = true;
% handles.points.point4.neighbours = [3];
% handles.points.point5.pvec = [0;1];
% handles.points.point5.tvec = [0;1];
% handles.points.point5.fix = false;
% handles.points.point5.neighbours = [2];
% handles.points.point6.pvec = [3; 1];
% handles.points.point6.tvec = [0;1];
% handles.points.point6.fix = false;
% handles.points.point6.neighbours = [3];
% 
% %Load section
% handles.points.point1.M = 0;
% handles.points.point1.F = [0;0];
% handles.points.point2.M = 0;
% handles.points.point2.F = [0;0];
% handles.points.point3.M = 0;
% handles.points.point3.F = [0;0];
% handles.points.point4.M = 0;
% handles.points.point4.F = [0;0];
% handles.points.point5.M = 0;
% handles.points.point5.F = [1;0];
% handles.points.point6.M = 0;
% handles.points.point6.F = [0;0];
% 
% syms Mtot(x)
% Mtot(x) = 0;
% handles.beams.beam1x2.S = struct('s12',4,'s21',4,'t12',2,'Mtot',Mtot);
% handles.beams.beam2x3.S = struct('s12',4,'s21',4,'t12',2,'Mtot',Mtot);
% handles.beams.beam3x4.S = struct('s12',4,'s21',4,'t12',2,'Mtot',Mtot);
% handles.beams.beam2x5.S = struct('s12',3,'s21',0,'t12',0,'Mtot',Mtot);
% handles.beams.beam3x6.S = struct('s12',3,'s21',0,'t12',0,'Mtot',Mtot);
% 
% handles.beams.beam1x2.F = [{'0'},{'0'},{'0'}];
% handles.beams.beam2x3.F = [{'0'},{'0'},{'0'}];
% handles.beams.beam3x4.F = [{'0'},{'0'},{'0'}];
% handles.beams.beam2x5.F = [{'0'},{'0'},{'0'}];
% handles.beams.beam3x6.F = [{'0'},{'0'},{'0'}];
% 
% handles.beams.beam1x2.M = [0 0];
% handles.beams.beam2x3.M = [0 0];
% handles.beams.beam3x4.M = [0 0];
% handles.beams.beam2x5.M = [0 0];
% handles.beams.beam3x6.M = [0 0];
% 
% handles.beams.beam1x2.q1 = [0 0];
% handles.beams.beam1x2.q2 = [0 0];
% handles.beams.beam1x2.qangle = 0;
% handles.beams.beam2x3.q1 = [0 0];
% handles.beams.beam2x3.q2 = [0 0];
% handles.beams.beam2x3.qangle = 0;
% handles.beams.beam3x4.q1 = [0 0];
% handles.beams.beam3x4.q2 = [0 0];
% handles.beams.beam3x4.qangle = 0;
% handles.beams.beam2x5.q1 = [0 0];
% handles.beams.beam2x5.q2 = [0 0];
% handles.beams.beam2x5.qangle = 0;
% handles.beams.beam3x6.q1 = [0 0];
% handles.beams.beam3x6.q2 = [0 0];
% handles.beams.beam3x6.qangle = 0;
% 
% handles.beams.beam1x2.haspsi = true;
% handles.beams.beam1x2.n = [1;0];
% handles.beams.beam1x2.t = [0;1];
% handles.beams.beam1x2.l = 1;
% handles.beams.beam1x2.beampoints = [1 2];
% handles.beams.beam2x3.haspsi = false;
% handles.beams.beam2x3.n = [0;-1];
% handles.beams.beam2x3.t = [1;0];
% handles.beams.beam2x3.l = 1;
% handles.beams.beam2x3.beampoints = [2 3];
% handles.beams.beam3x4.haspsi = true;
% handles.beams.beam3x4.n = [-1;0];
% handles.beams.beam3x4.t = [0;-1];
% handles.beams.beam3x4.l = 1;
% handles.beams.beam3x4.beampoints = [3 4];
% handles.beams.beam2x5.haspsi = false;
% handles.beams.beam2x5.n = [0; 1];
% handles.beams.beam2x5.t = [-1; 0];
% handles.beams.beam2x5.l = 1;
% handles.beams.beam2x5.beampoints = [2 5];
% handles.beams.beam3x6.haspsi = false;
% handles.beams.beam3x6.n = [0;-1];
% handles.beams.beam3x6.t = [1;0];
% handles.beams.beam3x6.l = 1;
% handles.beams.beam3x6.beampoints = [3 6];
% handles.pointnames = fieldnames(handles.points);
% handles.beamnames = fieldnames(handles.beams);

%Some kind of system 2
% -------------
% |6          |5
% |           |
% |           |
% |4          |3
% -------------
% |           |
% |           |
% |           |
% |           |
% -------------
% 1           2
handles.points.point1.r = [0;0];
handles.points.point1.pointnr = 1;
handles.points.point1.knottype = 'connection';
handles.points.point1.constrainttype = 'pin';
handles.points.point2.r = [1;0];
handles.points.point2.pointnr = 2;
handles.points.point2.knottype = 'connection';
handles.points.point2.constrainttype = 'pin';
handles.points.point3.r = [1;1];
handles.points.point3.pointnr = 3;
handles.points.point3.knottype = 'connection';
handles.points.point3.constrainttype = 'none';
handles.points.point4.r = [0;1];
handles.points.point4.pointnr = 4;
handles.points.point4.knottype = 'connection';
handles.points.point4.constrainttype = 'none';
handles.points.point5.r = [1;2];
handles.points.point5.pointnr = 5;
handles.points.point5.knottype = 'connection';
handles.points.point5.constrainttype = 'none';
handles.points.point6.r = [0;2];
handles.points.point6.pointnr = 6;
handles.points.point6.knottype = 'connection';
handles.points.point6.constrainttype = 'none';

handles.points.point1.pvec = [0 0; 0 0];
handles.points.point1.tvec = [1 0; 0 1];
handles.points.point1.fix = true;
handles.points.point1.neighbours = [2 4];
handles.points.point2.pvec = [1;0];
handles.points.point2.tvec = [0;1];
handles.points.point2.fix = true;
handles.points.point2.neighbours = [1 3];
handles.points.point3.pvec = [];
handles.points.point3.tvec = [];
handles.points.point3.fix = false;
handles.points.point3.neighbours = [2 4 5];
handles.points.point4.pvec = [];
handles.points.point4.tvec = [];
handles.points.point4.fix = false;
handles.points.point4.neighbours = [1 3 6];
handles.points.point5.pvec = [];
handles.points.point5.tvec = [];
handles.points.point5.fix = false;
handles.points.point5.neighbours = [3 6];
handles.points.point6.pvec = [];
handles.points.point6.tvec = [];
handles.points.point6.fix = false;
handles.points.point6.neighbours = [4 5];

%Load section
handles.points.point1.M = 0;
handles.points.point1.F = [0;0];
handles.points.point2.M = 0;
handles.points.point2.F = [0;0];
handles.points.point3.M = 0;
handles.points.point3.F = [0;0];
handles.points.point4.M = 0;
handles.points.point4.F = [1;0];
handles.points.point5.M = 0;
handles.points.point5.F = [0;0];
handles.points.point6.M = 1;
handles.points.point6.F = [1;0];

syms Mtot(x)
Mtot(x) = 0;
handles.beams.beam1x2.S = struct('s12',4,'s21',4,'t12',2,'Mtot',Mtot);
handles.beams.beam2x3.S = struct('s12',4,'s21',4,'t12',2,'Mtot',Mtot);
handles.beams.beam3x4.S = struct('s12',4,'s21',4,'t12',2,'Mtot',Mtot);
handles.beams.beam1x4.S = struct('s12',4,'s21',4,'t12',2,'Mtot',Mtot);
handles.beams.beam3x5.S = struct('s12',4,'s21',4,'t12',2,'Mtot',Mtot);
handles.beams.beam4x6.S = struct('s12',4,'s21',4,'t12',2,'Mtot',Mtot);
handles.beams.beam5x6.S = struct('s12',4,'s21',4,'t12',2,'Mtot',Mtot);

handles.beams.beam1x2.F = [{'0'},{'0'},{'0'}];
handles.beams.beam2x3.F = [{'0'},{'0'},{'0'}];
handles.beams.beam3x4.F = [{'0'},{'0'},{'0'}];
handles.beams.beam1x4.F = [{'0'},{'0'},{'0'}];
handles.beams.beam3x5.F = [{'0'},{'0'},{'0'}];
handles.beams.beam4x6.F = [{'0'},{'0'},{'0'}];
handles.beams.beam5x6.F = [{'0'},{'0'},{'0'}];

handles.beams.beam1x2.M = [0 0];
handles.beams.beam2x3.M = [0 0];
handles.beams.beam3x4.M = [0 0];
handles.beams.beam1x4.M = [0 0];
handles.beams.beam3x5.M = [0 0];
handles.beams.beam4x6.M = [0 0];
handles.beams.beam5x6.M = [0 0];

handles.beams.beam1x2.q1 = [0 0];
handles.beams.beam1x2.q2 = [0 0];
handles.beams.beam1x2.qangle = 0;
handles.beams.beam2x3.q1 = [0 0];
handles.beams.beam2x3.q2 = [0 0];
handles.beams.beam2x3.qangle = 0;
handles.beams.beam3x4.q1 = [0 0];
handles.beams.beam3x4.q2 = [0 0];
handles.beams.beam3x4.qangle = 0;
handles.beams.beam1x4.q1 = [0 0];
handles.beams.beam1x4.q2 = [0 0];
handles.beams.beam1x4.qangle = 0;
handles.beams.beam3x5.q1 = [0 0];
handles.beams.beam3x5.q2 = [0 0];
handles.beams.beam3x5.qangle = 0;
handles.beams.beam4x6.q1 = [0 0];
handles.beams.beam4x6.q2 = [0 0];
handles.beams.beam4x6.qangle = 0;
handles.beams.beam5x6.q1 = [0 0];
handles.beams.beam5x6.q2 = [0 0];
handles.beams.beam5x6.qangle = 0;


handles.beams.beam1x2.haspsi = false;
handles.beams.beam1x2.n = [0;1];
handles.beams.beam1x2.t = [1;0];
handles.beams.beam1x2.l = 1;
handles.beams.beam1x2.beampoints = [1 2];
handles.beams.beam2x3.haspsi = true;
handles.beams.beam2x3.n = [1;0];
handles.beams.beam2x3.t = [0;1];
handles.beams.beam2x3.l = 1;
handles.beams.beam2x3.beampoints = [2 3];
handles.beams.beam3x4.haspsi = false;
handles.beams.beam3x4.n = [0;-1];
handles.beams.beam3x4.t = [-1;0];
handles.beams.beam3x4.l = 1;
handles.beams.beam3x4.beampoints = [3 4];
handles.beams.beam1x4.haspsi = true;
handles.beams.beam1x4.n = [1;0];
handles.beams.beam1x4.t = [0;1];
handles.beams.beam1x4.l = 1;
handles.beams.beam1x4.beampoints = [1 4];
handles.beams.beam3x5.haspsi = true;
handles.beams.beam3x5.n = [1;0];
handles.beams.beam3x5.t = [0;1];
handles.beams.beam3x5.l = 1;
handles.beams.beam3x5.beampoints = [3 5];
handles.beams.beam4x6.haspsi = true;
handles.beams.beam4x6.n = [1;0];
handles.beams.beam4x6.t = [0;1];
handles.beams.beam4x6.l = 1;
handles.beams.beam4x6.beampoints = [4 6];
handles.beams.beam5x6.haspsi = false;
handles.beams.beam5x6.n = [0;-1];
handles.beams.beam5x6.t = [-1;0];
handles.beams.beam5x6.l = 1;
handles.beams.beam5x6.beampoints = [5 6];


handles.pointnames = fieldnames(handles.points);
handles.beamnames = fieldnames(handles.beams);

% Here comes the code to be copied into the main program


handles.beamdone = [{}];
% Find a fixpoint to start a path from
for j = 1 : numel(handles.pointnames)
    if handles.points.(char(handles.pointnames(j))).fix
        startpoint = handles.points.(char(handles.pointnames(j))).pointnr;
        break;
    end
end

handles = initpath(startpoint,1,handles);
handles = initmatch(1,handles);

nvar = countvar(handles.psieqn);
handles.validpsieqn = valideqn(handles.psieqn);
neq = numel(handles.validpsieqn);
dgr = nvar-neq;
varstosolve = symvar(handles.validpsieqn);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The part below is to be integrated into an interface dialog
[sol,varstosolve] = psidialog(dgr,varstosolve,handles);

if not(isstruct(sol))
    sol = struct(char(string(varstosolve)),sol);
end

handles.varstosolve = varstosolve;
handles.varindep = symvar(handles.validpsieqn);
for i = 1 : numel(varstosolve)
handles.varindep(handles.varindep == varstosolve(i)) = [];
end






% disp(strcat('Please choose: ',num2str(dgr),' angle(s) of rotation in function of which the others shall be computed')); %Ische nite
% You can choose from:......... SOME CODE

% str1 = '2'; %first beampoint
% str2 = '3'; %second beampoint
% parameter1 = handles.beams.(beamname(str2double(str1),str2double(str2))).psisym;
% str1 = '1';
% str2 = '4';
% parameter2 = handles.beams.(beamname(str2double(str1),str2double(str2))).psisym;
% varstosolve(varstosolve == parameter1) = [];
% varstosolve(varstosolve == parameter2) = [];

% The part above is to be integrated into an interface dialog
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sol = solve(handles.validpsieqn,varstosolve);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find a valid solution for default systems
% [sol,varstosolve] = validsol(handles.validpsieqn,varstosolve,dgr);

handles = renamepsi(sol,varstosolve,handles);
handles.dgr = dgr;
%varstosolve are the variables, the equations have been solved for by
%default;

% Find the knots having psis
handles = findphi(handles);

%Formulate all of the point equilibrium-equations
handles = pointequilibrium(handles);

%Formulate the displacement-equilibria:
handles.displeqn = [];
for i = 1 : numel(handles.varindep)
    handles = displacementequilibrium(handles.varindep(i),handles);
end

disp(handles.displeqn);
disp(handles.pointeqn); %Just for testing purposes
solangles = solve([handles.displeqn;handles.pointeqn]);
names = fieldnames(solangles);
%This is just for testing purposes: 
for i = 1 : numel(names)
    disp(names(i));
    disp(solangles.(char(names(i))));
end
% Substitute all the symbolic variables by their actual values
for p = 1 : numel(handles.pointnames)
   current = handles.points.(char(handles.pointnames(p))).pointnr;
   neighbours = handles.points.(char(handles.pointnames(p))).neighbours;
   for n = 1 : numel(neighbours)
       for i = 1 : numel(names)
           syms (char(names(i)))
           handles.points.(pointname(current)).(pointname(neighbours(n))).M = ...
               subs(handles.points.(pointname(current)).(pointname(neighbours(n))).M,...
               eval(char(names(i))),solangles.(char(names(i))));
       end
   end
end

%Superpone the Moment lines M0 and the beam-end-moments calculated by the
%displacement method.
handles = MomentSup(handles);

%Plot the final moment lines:
handles = finalmoments(handles)