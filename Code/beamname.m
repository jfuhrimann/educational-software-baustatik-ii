function beamstruct=beamname(i,j)
if i<j
    istr=num2str(i);
    jstr=num2str(j);
else
    istr=num2str(j);
    jstr=num2str(i);
end
beamstruct=strcat('beam',istr,'x',jstr);
end