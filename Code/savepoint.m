function result = savepoint(handles)
if not(handles.usegrid)
handles.togglebutton7.Visible ='on';
end
pointnrstr = get(handles.pointnr,'string');
    pointnrvalue = str2double(pointnrstr);
    if pointnrvalue == 1
        set(getpoints,'WindowButtonDownFcn',@callsolution);
    end
    handles.totalpoints=pointnrvalue;
    pointstruct = pointname(pointnrvalue);
    pointnr = 'pointnr';
    rvalue= [readfrac(get(handles.edit1,'String'));readfrac(get(handles.edit2,'String'))];
    r='r';
    neighboursvalue = get(handles.neighbours,'data');
    neighbours = 'neighbours';
    constrainttype = 'constrainttype';
    constrainttypevalue = handles.temp.constrainttype;
    if strcmp(constrainttypevalue,'roller')
        if not(isempty(get(handles.edit8,'string')))
        constraintanglevalue = readfrac(get(handles.edit8,'string'));
        else constraintanglevalue = 1000;
        end
    else constraintanglevalue = 0;
    end
    if or(strcmp(constrainttypevalue,'torsional spring'),strcmp(constrainttypevalue,'spring'))
        stiffnessvalue = readfrac(get(handles.edit8,'string'));
    else stiffnessvalue = 0;
    end
    constraintangle = 'constraintangle';
    stiffness = 'stiffness';
    knottype = 'knottype';
    knottypevalue = handles.temp.knottype;
    Fx = readfrac(get(handles.edit29,'string'));
    Fy = readfrac(get(handles.edit31,'string'));
    Fvalue = [Fx;Fy];
    F='F';
    Mvalue = readfrac(get(handles.edit32,'string'));
    M='M';
    phivalue = readfrac(get(handles.edit12,'string'));
    phi='phi';
    handles.points.(pointstruct).pointnr = pointnrvalue;
    handles.points.(pointstruct).r = rvalue;
    handles.points.(pointstruct).neighbours = neighboursvalue;
    handles.points.(pointstruct).constrainttype= constrainttypevalue;
    handles.points.(pointstruct).constraintangle = constraintanglevalue;
    handles.points.(pointstruct).stiffness = stiffnessvalue;
    handles.points.(pointstruct).knottype = knottypevalue;
    handles.points.(pointstruct).F = Fvalue;
    handles.points.(pointstruct).M = Mvalue;
    handles.points.(pointstruct).phi = phivalue;
    if not(handles.changepoint) && not(handles.modify)
    handles.pointnames=fieldnames(handles.points);
    end
    
    %scaling the axes
    if not(handles.usegrid)
    if isfield(handles, 'scalexupper')
        if handles.scalexupper < handles.points.(pointstruct).r(1) %x-axis must be refitted towards +infty
            handles.scalexupper=handles.points.(pointstruct).r(1);
        end
        if handles.scaleyupper < handles.points.(pointstruct).r(2) %y-axis must be refitted towards + infty
            handles.scaleyupper=handles.points.(pointstruct).r(2);
        end
        if handles.scalexlower > handles.points.(pointstruct).r(1) %x-axis must be refitted towards -infty
            handles.scalexlower=handles.points.(pointstruct).r(1);
        end
        if handles.scaleylower > handles.points.(pointstruct).r(2) %y-axis must be refitted towards -infty
            handles.scaleylower=handles.points.(pointstruct).r(2);
        end
    else
        handles.scalexupper=handles.points.(pointstruct).r(1);
        handles.scaleyupper=handles.points.(pointstruct).r(2);
        handles.scalexlower=handles.points.(pointstruct).r(1);
        handles.scaleylower=handles.points.(pointstruct).r(2);
    end

    %scalefactor
    handles.imscale = abs(handles.scalexupper - handles.scalexlower) + 2;
    if handles.imscale == 0
        handles.imscale = abs(handles.scaleyupper-handles.scaleylower) + 2;
    end
    end
    %find maximal F value
    if isfield(handles,'Fmax')
        if norm(Fvalue)>handles.Fmax
            handles.Fmax = norm(Fvalue);
        end
    else, handles.Fmax = norm(Fvalue);
    end
        
    
%fit the axis and plot the point
    axes(handles.axes1);
    hold on
    %delete the moments drawn so far to plot them anew and fit the scale
    %delete the point loads drawn so far to plot them anew later
    if handles.changepoint && not(handles.modify) && not(handles.usegrid), limitval = handles.oldtotal;
    elseif not(handles.usegrid) && handles.changepoint && handles.modify, limitval = pointnrvalue;
    elseif not(handles.usegrid) && not(handles.changepoint) && handles.modify, limitval = pointnrvalue;
    else limitval = numel(handles.pointnames);
    end
    
    for i=1 : limitval
        if isfield(handles.points.(char(handles.pointnames(i))),'textbox')
            delete(handles.points.(char(handles.pointnames(i))).textbox);
            handles.points.(char(handles.pointnames(i))) = rmfield(handles.points.(char(handles.pointnames(i))),'textbox');
        end
        if isfield(handles.points.(char(handles.pointnames(i))),'pointmarker')
            delete(handles.points.(char(handles.pointnames(i))).pointmarker);
            handles.points.(char(handles.pointnames(i))) = rmfield(handles.points.(char(handles.pointnames(i))),'pointmarker');
        end
        if isfield(handles.points.(char(handles.pointnames(i))),'loads')
        if isfield(handles.points.(char(handles.pointnames(i))).loads,'Mimg')
            delete(handles.points.(char(handles.pointnames(i))).loads.Mimg);
            delete(handles.points.(char(handles.pointnames(i))).loads.Mpoly);
            delete(handles.points.(char(handles.pointnames(i))).loads.Mtext);
        end
        if isfield(handles.points.(char(handles.pointnames(i))).loads,'Fimg')
            delete(handles.points.(char(handles.pointnames(i))).loads.Fimg);
            delete(handles.points.(char(handles.pointnames(i))).loads.Ftext);
        end
        if isfield(handles.points.(char(handles.pointnames(i))).loads,'phitext')
            delete(handles.points.(char(handles.pointnames(i))).loads.phitext);
        end
        end
        if isfield(handles.points.(char(handles.pointnames(i))),'stiffnesstext')
            delete(handles.points.(char(handles.pointnames(i))).stiffnesstext);
        end

    end
    
    %delete all of the helplines
    if isfield(handles,'neighbourlines')
        linenames = fieldnames(handles.neighbourlines);
        for i = 1 : numel(linenames)
            thisline = char(linenames(i));
            delete(handles.neighbourlines.(thisline));
        end
    end
    if not(handles.usegrid)
    axis([handles.scalexlower-0.125*handles.imscale,handles.scalexupper+0.125*handles.imscale,handles.scaleylower-0.125*handles.imscale,handles.scaleyupper+.125*handles.imscale]);
    axis equal
    end

% Draw new helplines
    linenr = 1;
    for i = 1 : limitval
        if isfield(handles.points,(pointname(i)))
            neighbours = handles.points.(pointname(i)).neighbours;
            for j = 1 : numel(neighbours)
                if neighbours(j) > i
                    if isfield(handles.points,(pointname(neighbours(j))))
                        handles.neighbourlines.(strcat('line',num2str(linenr))) = ...
                            line([handles.points.(pointname(i)).r(1),handles.points.(pointname(neighbours(j))).r(1)],...
                            [handles.points.(pointname(i)).r(2), handles.points.(pointname(neighbours(j))).r(2)],...
                            'Color',[.5,.5,.5]);
                            linenr = linenr + 1;
                    end
                end
            end
        end
    end
    
    for i = 1 : limitval
        point = pointname(i);
        if isfield(handles.points.(point),'knottype') && strcmp(handles.points.(point).knottype,'hinge')
            pointsymbol = 'o';
        else, pointsymbol = 'x'; end
    handles.points.(point).pointmarker = ...
        plot(handles.points.(point).r(1),handles.points.(point).r(2),strcat(pointsymbol,'-k'),'MarkerFaceColor','w');
    uistack(handles.points.(point).pointmarker,'top');

    if isfield(handles.points.(point),'phi')&&handles.points.(point).phi ~= 0
        if strcmp(handles.unit,'kN'), unit = 'rad';
        elseif strcmp(handles.unit,'q'), unit = '\frac{ql^3}{EI}';
        elseif strcmp(handles.unit,'Q'), unit = '\frac{Ql^2}{EI}';
        else unit = '\frac{Ml}{EI}'; end
    handles.points.(point).loads.phitext = text(handles.points.(point).r(1) + 0.03*(handles.imscale)^0.8, handles.points.(point).r(2)+0.03*(handles.imscale)^0.8, strcat('$\phi=', checkfrac(handles.points.(point).phi), unit, '$'),'Interpreter','latex');
    end
    
    if isfield(handles.points.(point),'constrainttype') && or(strcmp(handles.points.(point).constrainttype,'spring'),strcmp(handles.points.(point).constrainttype,'torsional spring'))
        if strcmp(handles.points.(point).constrainttype,'torsional spring')
        if strcmp(handles.unit,'kN'), unit = '\frac{1}{kNm}';
        else unit = '\frac{l}{EI}'; end
        else
        if strcmp(handles.unit,'kN'), unit = '\frac{m}{kN}';
        else unit = '\frac{l^3}{EI}'; end
        end
        handles.points.(point).stiffnesstext = text(handles.points.(point).r(1)+0.03*(handles.imscale)^0.8,handles.points.(point).r(2)+0.03*(handles.imscale)^0.8,{['$c_f =' checkfrac(handles.points.(point).stiffness) unit '$']},'Interpreter','latex');
    end
    end
%plot the Moments & textboxes anew
for i = 1 : limitval
    handles.points.(char(handles.pointnames(i))).textbox = text(handles.points.(char(handles.pointnames(i))).r(1)+0.03*(handles.imscale)^0.8,handles.points.(char(handles.pointnames(i))).r(2)-0.06*(handles.imscale)^0.8,strcat('$\raisebox{.5pt}{\textcircled{\raisebox{-.9pt}{',num2str(handles.points.(char(handles.pointnames(i))).pointnr),'}}}$'),'Interpreter','latex');

    if isfield(handles.points.(char(handles.pointnames(i))),'M') && handles.points.(char(handles.pointnames(i))).M ~= 0
        if strcmp(handles.unit,'kN'), unit = 'kNm';
        elseif strcmp(handles.unit,'q'), unit = 'ql^2';
        elseif strcmp(handles.unit,'Q'), unit = 'Ql';
        else unit = 'M'; end
    th = linspace( pi/2, -pi, 100);
    R = 0.1*(handles.imscale)^0.8;
    x = R*cos(th) + handles.points.(char(handles.pointnames(i))).r(1);
    y = R*sin(th) + handles.points.(char(handles.pointnames(i))).r(2);
    handles.points.(char(handles.pointnames(i))).loads.Mimg=plot(x,y,'r','LineWidth',1.5);
    handles.points.(char(handles.pointnames(i))).loads.Mtext = text(handles.points.(char(handles.pointnames(i))).r(1),handles.points.(char(handles.pointnames(i))).r(2)+.4*(handles.imscale)^.7, {['$ M=' num2str(abs(handles.points.(char(handles.pointnames(i))).M)) unit '$']},'Interpreter','latex');
    handles.points.(char(handles.pointnames(i))).loads.Mtext.Color = 'red';
    if sign(handles.points.(char(handles.pointnames(i))).M) == 1 %M>0
        mpoly = handles.points.(char(handles.pointnames(i))).r+[0;R];
        spoly = mpoly + 0.5*R*[1/sqrt(2);1/sqrt(2)];
        epoly = mpoly + 0.5*R*[1/sqrt(2);-1/sqrt(2)];
        x=[spoly(1),mpoly(1),epoly(1)];
        y=[spoly(2),mpoly(2),epoly(2)];
    handles.points.(char(handles.pointnames(i))).loads.Mpoly = plot(x,y,'r','LineWidth',1.5);
    elseif sign(handles.points.(char(handles.pointnames(i))).M) ==-1
        mpoly = handles.points.(char(handles.pointnames(i))).r+[-R;0];
        spoly = mpoly + 0.5*R*[1/sqrt(2);-1/sqrt(2)];
        epoly = mpoly + 0.5*R*[-1/sqrt(2);-1/sqrt(2)];
        x=[spoly(1),mpoly(1),epoly(1)];
        y=[spoly(2),mpoly(2),epoly(2)];
     handles.points.(char(handles.pointnames(i))).loads.Mpoly = plot(x,y,'r','LineWidth',1.5);
    end
    end
end
%plot the point loads anew
for i = 1 : numel(handles.pointnames)
    if isfield(handles.points.(char(handles.pointnames(i))),'F')
    if or(handles.points.(char(handles.pointnames(i))).F(1) ~= 0, handles.points.(char(handles.pointnames(i))).F(2) ~= 0)
        if strcmp(handles.unit,'kN'), unit = 'kN';
        elseif strcmp(handles.unit,'q'), unit = 'ql';
        elseif strcmp(handles.unit,'Q'), unit = 'Q';
        else unit = '\frac{M}{l}'; end
    Fx = handles.points.(char(handles.pointnames(i))).F(1)/handles.Fmax;
    Fy = handles.points.(char(handles.pointnames(i))).F(2)/handles.Fmax;
    normF=norm(handles.points.(char(handles.pointnames(i))).F);
    rx=handles.points.(char(handles.pointnames(i))).r(1);
    ry=handles.points.(char(handles.pointnames(i))).r(2);
    handles.points.(char(handles.pointnames(i))).loads.Fimg = quiver(rx-Fx*0.2*handles.imscale^0.8, ry-Fy*0.2*(handles.imscale)^0.8, Fx*0.2*handles.imscale^0.8, Fy*0.2*(handles.imscale)^0.8,'r','LineWidth',1.5);
    handles.points.(char(handles.pointnames(i))).loads.Ftext = text(rx-Fx*0.3*handles.imscale^0.8, ry-Fy*0.3*(handles.imscale)^0.8, {['$ F=' num2str(normF) unit '$']},'Interpreter','latex');
    handles.points.(char(handles.pointnames(i))).loads.Ftext.Color = 'red';
    end
    end
end
%reset the GUI


if handles.modify && isfield(handles.points,pointname(pointnrvalue + 1)) && not(isfield(handles,'oldtotal'))
    i=pointnrvalue+1;
    %set all of the editfields according to their necessary content.
    point = pointname(i);
    handles.points.(point).MarkerEdgeColor = [255 192 0]/255;
    handles.points.(point).textbox.Color = [255 192 0]/255;    
    set(handles.pointnr,'string',num2str(i));
    set(handles.edit1,'string',num2str(handles.points.(point).r(1)));
    set(handles.edit2,'string',num2str(handles.points.(point).r(2)));
    set(handles.numberofneighbours,'string',num2str(numel(handles.points.(point).neighbours)));
    tablesize = handles.points.(point).neighbours;
    set(handles.neighbours,'Data',tablesize);
    %IDial for constraints
    if not(handles.usegrid)
    if strcmp(handles.points.(point).constrainttype,'fixed')
        handles = cdial_set('fixed','connection',handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    elseif strcmp(handles.points.(point).constrainttype,'pin')
        handles = cdial_set('pin',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    elseif strcmp(handles.points.(point).constrainttype,'roller')
        handles = cdial_set('roller',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','On');
        set(handles.text34,'Visible','On');
        set(handles.text34,'String','angle');
        if handles.points.(point).constraintangle == 1000
        	set(handles.edit8,'String','');
        else
            set(handles.edit8,'String',num2str(handles.points.(point).constraintangle));
        end
    elseif strcmp(handles.points.(point).constrainttype,'torsional spring')
        handles = cdial_set('torsional spring',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','On');
        if strcmp(handles.unit,'kN'), unit = '[1/kNm]';
        else, unit = '[l/EI]'; end
        set(handles.text34,'string',strcat('cf ',unit));
        set(handles.edit8,'String',num2str(handles.points.(point).stiffness));
    elseif strcmp(handles.points.(point).constrainttype,'spring')
        handles = cdial_set('spring','hinge',handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','On');
        if strcmp(handles.unit,'kN'), unit = '[m/kN]';
        else, unit = '[l^3/EI]'; end
        set(handles.text34,'string',strcat('cf ',unit));
        set(handles.edit8,'String',num2str(handles.points.(point).stiffness));
    elseif strcmp(handles.points.(point).constrainttype,'none')
        handles = cdial_set('none',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    end
    else
        handles = cdial_set('','',[],handles);
        set(handles.text34,'String',''); set(handles.edit8,'Visible','Off');
    end
    set(handles.edit29,'string','0');
    set(handles.edit31,'string','0');
    set(handles.edit32,'string','0');
    set(handles.edit12,'string','0');
%%%%%%%%%WAS PASSIERT BEI MODIFY UND ISFIELD OLDTOTAL UND ES GIBT NOCH ZU
%%%%%%%%%MODIFIZIERENDE PUNKTE
elseif handles.modify && isfield(handles,'oldtotal') && isfield(handles.points,pointname(handles.oldtotal+1))
    i = handles.oldtotal + 1;
    %set all of the editfields according to their necessary content.
    point = pointname(i);
    handles.points.(point).MarkerEdgeColor = [255 192 0]/255;
    handles.points.(point).textbox.Color = [255 192 0]/255; 
    set(handles.pointnr,'string',num2str(i));
    set(handles.edit1,'string',num2str(handles.points.(point).r(1)));
    set(handles.edit2,'string',num2str(handles.points.(point).r(2)));
    set(handles.numberofneighbours,'string',num2str(numel(handles.points.(point).neighbours)));
    tablesize = handles.points.(point).neighbours;
    set(handles.neighbours,'Data',tablesize);
    if not(handles.usegrid)
    if strcmp(handles.points.(point).constrainttype,'fixed')
        handles = cdial_set('fixed','connection',handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    elseif strcmp(handles.points.(point).constrainttype,'pin')
        handles = cdial_set('pin',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    elseif strcmp(handles.points.(point).constrainttype,'roller')
        handles = cdial_set('roller',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','On');
        set(handles.text34,'String','angle');
        if handles.points.(point).constraintangle == 1000
        	set(handles.edit8,'String','');
        else
            set(handles.edit8,'String',num2str(handles.points.(point).constraintangle));
        end
    elseif strcmp(handles.points.(point).constrainttype,'torsional spring')
        handles = cdial_set('torsional spring',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','On');
        if strcmp(handles.unit,'kN'), unit = '[1/kNm]';
        else, unit = '[l/EI]'; end
        set(handles.text34,'string',strcat('cf ',unit));
        set(handles.edit8,'String',num2str(handles.points.(point).stiffness));
    elseif strcmp(handles.points.(point).constrainttype,'spring')
        handles = cdial_set('spring','hinge',handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','On');
        if strcmp(handles.unit,'kN'), unit = '[m/kN]';
        else, unit = '[l^3/EI]'; end
        set(handles.text34,'string',strcat('cf ',unit));
        set(handles.edit8,'String',num2str(handles.points.(point).stiffness));
    elseif strcmp(handles.points.(point).constrainttype,'none')
        handles = cdial_set('none',handles.points.(point).knottype,handles.points.(point).neighbours,handles);
        set(handles.edit8,'Visible','Off');
        set(handles.text34,'String','');
    end
    else
        handles = cdial_set('','',[],handles);
        set(handles.text34,'String',''); 
        set(handles.edit8,'Visible','Off'); 
    end
    set(handles.edit29,'string','0');
    set(handles.edit31,'string','0');
    set(handles.edit32,'string','0');
    set(handles.edit12,'string','0');
%%%%%%%%WAS PASSIERT MIT MODIFY UND ISFIELD OLDTOTAL UND KEINE ZU
%%%%%%%%MODIFIZIERENDE PUNKTE MEHR: GEHE INS "ELSE"

%%%%%%%%WAS PASSIERT OHNE MODIFY MIT OLDTOTAL
elseif not(handles.modify) && isfield(handles,'oldtotal')
    i = handles.oldtotal + 1;
    set(handles.pointnr,'string',num2str(i));
    set(handles.edit1,'string','');
    set(handles.edit2,'string','');
    set(handles.numberofneighbours,'string','');
    tablesize = [];
    set(handles.neighbours,'Data',tablesize);
    handles = cdial_set('','',[],handles);
    set(handles.edit8,'string','');
    set(handles.edit8,'visible','Off');
    set(handles.edit29,'string','0');
    set(handles.edit31,'string','0');
    set(handles.edit32,'string','0');
    set(handles.edit12,'string','0');
    set(handles.text34,'visible','Off');
else
    if isfield(handles,'oldtotal')
        i = handles.oldtotal + 1;
    else
        i = pointnrvalue + 1;
    end
    set(handles.pointnr,'string',num2str(i));
    set(handles.edit1,'string','');
    set(handles.edit2,'string','');
    set(handles.numberofneighbours,'string','');
    tablesize = [];
    set(handles.neighbours,'Data',tablesize);
    handles = cdial_set('','',[],handles);
    handles.temp = rmfield(handles.temp,'constrainttype');
    set(handles.edit8,'string','');
    set(handles.edit8,'visible','Off');
    set(handles.edit29,'string','0');
    set(handles.edit31,'string','0');
    set(handles.edit32,'string','0');
    set(handles.edit12,'string','0');
    set(handles.text34,'visible','Off');
end
if isfield(handles,'oldtotal')
    handles.totalpoints = handles.oldtotal;
    handles = rmfield(handles,'oldtotal');
end
if handles.modify && handles.totalpoints == handles.totalpointstotal
    handles.pushbutton4.Visible = 'on';
    handles.addpoint.Visible = 'off';
    %%%%%%%%%%%%%%%%%
    handles.text2.String = 'You are done.';
    set(handles.pointnr,'Visible','Off');
    set(handles.edit1, 'Visible','Off'); 
  set(handles.edit2, 'Visible','Off'); 
  set(handles.text4,'Visible','Off');
  set(handles.text5,'Visible','Off');
  set(handles.numberofneighbours, 'Visible','Off'); 
  set(handles.neighboursok, 'Visible','Off'); 
  set(handles.neighbours, 'Visible','Off'); 
  set(handles.axes2, 'Visible','Off');
  set(handles.axes2.Children,'Visible','off');
  set(handles.edit8, 'Visible','Off'); 
  set(handles.edit12, 'Visible','Off');  
  set(handles.edit29, 'Visible','Off'); 
  set(handles.edit31, 'Visible','Off'); 
  set(handles.edit32, 'Visible','Off'); 
  set(handles.addpoint, 'Visible','Off'); 
  set(handles.edit12, 'Visible','Off');
  
    handles.text6.Visible = 'off';
    handles.text7.Visible = 'off';
    handles.text8.Visible = 'off';
    handles.text10.Visible = 'off';
    handles.text11.Visible = 'off';
    handles.text12.Visible = 'off';
    set(handles.info2,'Visible','off');
    %%%%%%%%%%%%%%%%%%%
    if handles.usegrid
        xmin = realmax;
        ymin = realmax;
        xmax = -realmax;
        ymax = -realmax;
        for i = 1 : numel(handles.pointnames)
            point = char(handles.pointnames(i));
            if handles.points.(point).r(1) < xmin
                xmin = handles.points.(point).r(1);
            end
            if handles.points.(point).r(1) > xmax
                xmax = handles.points.(point).r(1);
            end
            if handles.points.(point).r(2) < ymin
                ymin = handles.points.(point).r(2);
            end
            if handles.points.(point).r(2) > ymax
                ymax = handles.points.(point).r(2);
            end
        end
            handles.scalexlower = xmin;
            handles.scalexupper = xmax;
            handles.scaleylower = ymin;
            handles.scaleyupper = ymax;
            axes(handles.axes1);
            axis([handles.scalexlower-0.125*handles.imscale,handles.scalexupper+.125*handles.imscale,handles.scaleylower-0.125*handles.imscale,handles.scaleyupper+0.125*handles.imscale]);
            axis equal
            handles.imscale = abs(handles.scalexupper - handles.scalexlower) + 2;
            if handles.imscale == 0
                handles.imscale = abs(handles.scaleyupper-handles.scaleylower) + 2;
            end
            handles.togglebutton7.Visible = 'on';
            handles.usegrid = false;
        end
end
if handles.changepoint
    set(handles.togglebutton7,'Value',0);
    set(handles.togglebutton7,'BackgroundColor',[0.95,0.95,0.95]);
    handles.changepoint = false;
end
result = handles;
end