function bool = isonline(pvec,tvec,point)
syms lambda
lambda = solve(pvec + tvec*lambda == point, lambda);
bool = not(isempty(lambda));
end