function varargout = solution(beam,handles)
format short
f2h = findall(0, 'name', 'force method');

if isempty(f2h)
    
% PC's active screen size
screen_size = get(0,'ScreenSize');
pc_width  = screen_size(3);
pc_height = screen_size(4);
% pc_width = 1000;
% pc_height = 650;

if pc_width > 1600 || pc_height > 900
    pc_width = 1550;
    pc_height = 870;
end

%Matlab also does not consider the width of the border:
window_border  = 5;
%new position:
height = pc_height - 100;
width = height/sqrt(2) + 60;
widthqst = 300;
bigwidth = width + widthqst;
bottom = pc_height/2 - height/2 + 10;
left = pc_width/2 - width/2  - window_border/2 - widthqst/2;

Fig = figure('Tag', 'figure1', ...
      			'Units', 'pixels', ...
      			'Position', [left bottom bigwidth height], ...
      			'Name', 'force method', ...
      			'MenuBar', 'none', ...
      			'NumberTitle', 'off', ...
      			'Color', 'white', ...
                'Resize','off');
            
h = guihandles(Fig);
h.beam = handles.beams.(beam);
h.points = handles.points;
h.points = handles.points;
h.S = handles.beams.(beam).S;
h.unit = handles.unit;
h.interactive = handles.interactive;
h.theory = handles.theory;
ca = h.S.newconstrainta;
cb = h.S.newconstraintb;

if strcmp(h.unit,'kN/m')
    h.unitM = 'kNm';
    h.unitV = 'kN';
    h.unitstiffness = 'kNm';
    h.length = 'm';
elseif strcmp(h.unit,'q')
    h.unitM = 'ql^2';
    h.unitV = 'ql';
    h.unitstiffness = '\frac{EI}{l}';
    h.length = 'l';
elseif strcmp(h.unit,'Q')
    h.unitM = 'Ql';
    h.unitV = 'Q';
    h.unitstiffness = '\frac{EI}{l}';
    h.length = 'l';
else
    h.unitM = 'M';
    h.unitV = '\frac{M}{l}';
    h.unitstiffness = '\frac{EI}{l}';
    h.length = 'l';
end
    
h.height = height;
h.width = width;
h.bottom = bottom;
h.left = left;
h.screendim = [0, 0, pc_width, pc_height];

%start INITIALIZATION:
h.questionpanel = uipanel('Parent',Fig,'units','pixels','Position',[h.width, 0, widthqst+2, h.height+2],'BackgroundColor', [ 0.9216    0.9686    1.0000]); %[0.9, 0.9, 0.9]); %
h. graficpanel = uipanel('Parent',h.questionpanel,'Visible','Off','units','pixels','Position',[0, 0, widthqst+2, h.height-220],'BackgroundColor', [0.9882    0.9922    1.0000]); %[ 0.9216    0.9686    1.0000]); %
h.number = 1;
h.unitforcenr = 0;
h.panelnr = 1;
h.panel = strcat('panel',num2str(h.panelnr));
h.(h.panel) = uipanel('Parent',Fig,'units','pixels','Position',[0 0 h.width h.height+2],'BackgroundColor', 'white'); %[0.9, 0.9, 0.9]); %
h.name = strcat('text',num2str(h.number));
h.obj.(h.name) = uicontrol(h.(h.panel),'Style','Text','HorizontalAlignment','left','Position',[20, h.height-10-30, 600, 30],'BackgroundColor','white','FontSize',16, 'FontName','Calibri','FontWeight','bold',...
   'String',strcat('Solutions Force Method - Beam',{' '},num2str(handles.beams.(beam).beampoints(1)),' - ',{' '},num2str(handles.beams.(beam).beampoints(2))));

h.lastbottompos = h.obj.(h.name).Position(2);
h.delta10sum = 0;
h.delta20sum = 0;
h.delta10str = [];
h.delta20str = [];
h.load = 0;
h.pause = 2;
h.pausesuperposition = 1.5;
h.pausequestion = 0;
h.loadslist = [];
h.intconst1pl = [];
h.intconst2pl = [];
h.intconst1dl = [];
h.intconst2dl = [];
h.intconst1m = [];
h.intconst2m = [];
h.intsol1pl = [];
h.intconst1pl_l = [];
h.intconst1pl_r = [];
h.intconst2pl_l = [];
h.intconst2pl_r = [];
h.intconst1dl_l = [];
h.intconst1dl_r = [];
h.intconst2dl_l = [];
h.intconst2dl_r = [];
h.intconst1m_l = [];
h.intconst1m_r = [];
h.intconst2m_l = [];
h.intconst2m_r = [];
h.intsol2pl  = [];
h.intsol1dl = [];
h.intsol2dl  = [];
h.intsol1m = [];
h.intsol2m  = [];
h.Mmaxpl = [];
h.Mmaxpl_l = [];
h.Mmaxpl_r = [];
h.Mmaxdl_l = [];
h.Mmaxdl_r = [];
h.Mmaxm_l = [];
h.Mmaxm_r = [];
h.Mmax1 = [];
h.nrpointload = 0;
h.superposition = false;
h.allfuncequal0 = false;
% strhelpspring = 'Do not forget that the spring does internal work!';
strhelpspring = [];
h.panelofsuperposition = false;
set(0, 'currentfigure', Fig);
% end INITIALIZATION:

%PROGRAM:
h = beam_image_FM(Fig, h,false, false, true, true, true);
if h.beam.hinge > 0, hinge=1; else, hinge=0; end
h = theory(Fig, h,[{'Degree of static indeterminacy:'} {'title'} {1};...
                      {'There are many formulas to determine the degree of static indeterminacy, but they all are based on a simple principle: The amount of equations is subtracted from the number of unknowns.'} {'text'} {1};...
                      {'\bfFirst: \rmDetermine the number of unknowns: In these examples, the beams are axially rigid, so only the moment causes deformation and is therefore unknown (the shear force is deduced from the moment line). Check for each constraint if it can carry a shear force or a moment and sum them up.'} {'text'} {1}; ...
                      {'\bfSecond: \rmDetermine the amount of equations: There is an equation for the moment equilibrium and a second for the shear force equilibrium for each beam.'} {'text'} {1};...
                      {'\bfAttention: \rmA hinge along the beam creates a degree of freedom and allows to formulate another moment equilibrium.'} {'text'} {1}
                      {'$N = \sum{unknowns} - 2 - hinge$'} {'equation'} {1}]);
h = write(Fig, h,'text', 'What is the degree of static indeterminacy? n = ...', true, true);
helpstr = strcat('Count the constraints ( =',{' '},num2str(h.S.N.constraints),{' '},'), subtract the number of equations (M, V) and finally subtract the hinge, if there is one (it generates an additional degree of freedom: The moment there must be 0)');
h = question(Fig, h, {' '}, checkfracstring(h.S.N.constraints-2-hinge), helpstr, false, ' ');

if h.S.N.end == 1
    h = write(Fig, h,'text', strcat('You have to introduce one redundant force.', {' '}, h.S.releaseconstraint),false, false);
    h = beam_image_FM(Fig, h,true, true, false, false, false); 
elseif h.S.N.end == 2
    h = write(Fig, h,'text', strcat('You have to introduce two redundant forces.', {' '}, h.S.releaseconstraint), false, false);
    h = beam_image_FM(Fig, h,true, true, false, false, false);    
end
if h.S.N.end > 0
h = theory(Fig, h,[{'What is a redundant force?'} {'title'} {1};...
                      {'A statically indeterminated system (n > 0) can be made a statically determinate primary structure (n = 0) by removing n constraints.'} {'text'} {1};...
                      {'The constraints removed are then replaced by redundant forces (X_i). Together with the main loads (Q, q, M) they cause a deformation. This deformation must fulfil the kinematic compatibility conditions of the statically indeterminate system.'} {'text'} {1}]);

h = theory(Fig, h,[{'Compatibility conditions:'} {'title'} {1};...
                      {'The deformation (\delta_{i}) caused by the main loads (\delta_{i,0}) and by the redundant forces (\delta_{i,i}) must fulfil the compatibility conditions: If there is no displacement or rotation of the point i then the deformation is \delta_i = 0.'} {'text'} {1};...
                      {'$\delta_{i} = \delta_{i,0} + \sum X_j \cdot \delta_{i,j} = 0$'} {'equation'} {1};...
                      {'\delta_{i,0} = deformation (displacement/rotation) of point i caused by the main loads'} {'text'} {1};...
                      {'\delta_{i,i} = deformation (displacement/rotation) of point i caused by the redundant forces'} {'text'} {1}]);
                  
h = theory(Fig, h,[{'How to calculate the deformation \delta_{i,0} and \delta_{i,i}:'} {'title'} {1};...
                      {'The deformation is calculated using the energy theorem:'} {'text'} {1};...
                      {'$1 \cdot \delta_i + \sum \bar{R_i} \cdot r_i = \int \bar{M} \frac{M}{EI} dx + \int \bar{N} \frac{N}{EA} dx + \int \bar{V} \frac{V}{GA*} dx + \int \bar{T} \frac{T}{GK} dx + ...$'} {'equation'} {1};...
                      {'$... + \int \bar{M} \frac{\alpha_T \cdot (T_u - T_o)}{h} dx + \int \bar{N} \alpha_T \Delta T dx + \sum \bar{F} F c_f$'} {'equation'} {1}; ...
                      {'The equations means basically;'} {'text'} {1};...
                      {'$W_{ext} + W_{int} = 0     \Longrightarrow     W_{ext} = W_{int}$'} {'equation'} {1};...
                      {'\Rightarrow \itwork of the loads and reaction forces (ext. work) = strain energy (int. work)'} {'text'} {1};...
                      {' '} {'text'} {1};...
                      {'\bfSimplified energy theorem:'} {'text'} {1};...
                      {char(strcat('For the purpose of this educational software a simpler form of the equation is used.',{' '},...
                      'As the beams are assumed to behave corresponding to the Euler-Bernoulli-theory, shear deformations are neglected.',{' '},...
                      'Axial deformations of the beams are neglected as well, because the beams are assumed to be axially rigid.',{' '},...
                      'Torsion and temperature effects are assumed to have no effect (but can be taken in account by including an initial beam rotation).'))} {'text'} {1};...
                      {'$\delta_i = \int M_0 \frac{M_i}{EI} dx + \sum F_{i,0} \cdot F_{i,i} \cdot c_f$'} {'equation'} {1};...
                      {'\bfInternal work of spring elements:'} {'text'} {1};...
                      {'The following pictures explain the term F_{i,0} F_{i,i} c_f of the spring:'} {'text'} {1};...
                      {'spring internal work.jpg'} {'image'} {10}]);
end
                      
for i=1:h.S.N.end
        if i == 1
            h = write(Fig, h,'title', strcat('Redundant force', {' '}, h.S.nrX1, ':'), false, true);
            h = dograph(Fig, h,1, h.S.M1, false, 'unitforce');
            if h.S.sh ~= 2/3*h.S.l && h.S.sh ~= h.S.l/3 && h.S.sh ~= h.S.l/2 && h.S.sh > 0
                h = equation(Fig, h,strcat('\delta_{',h.S.delta1,',',h.S.delta1,'} =',...
                    '\underbrace{',checkfrac(h.intconst11_l),'}_{\mathrm{int. coeff.}}', '\cdot',...
                    '\underbrace{\left(',checkfrac(h.M11max_l),'\right)^2}_{\mathrm{',h.S.M_1max,'left}} \frac{',checkfrac(h.S.sh/h.S.l),'L_{tot}}{EI}+',...
                    '\underbrace{',checkfrac(h.intconst11_r),'}_{\mathrm{int. coeff.}}', '\cdot',...
                    '\underbrace{\left(',checkfrac(h.M11max_r),'\right)^2}_{\mathrm{',h.S.M_1max,'right}} \frac{(1-',checkfrac(h.S.sh/h.S.l),')L_{tot}}{EI}', h.S.cfval11, '=',checkfrac(h.S.intsol11)));    
            else
                h = equation(Fig, h,strcat('\delta_{',h.S.delta1,',',h.S.delta1,'} = \underbrace{',checkfrac(h.intconst11),'}_{\mathrm{int. coeff.}}', '\cdot',...
                    '\underbrace{\left(',checkfrac(h.S.M1max(1)),'\right)^2}_{\mathrm{',h.S.M_1max,'}} \frac{L_{tot}}{EI}', h.S.cfval11, '=',checkfrac(h.S.intsol11)));  
            end
        else
            h = write(Fig, h,'title', strcat('Redundant force', {' '}, h.S.nrX2, ':'), false, true);
            h = dograph(Fig, h,1, h.S.M2, false, 'unitforce');
            h = equation(Fig, h,strcat('\delta_{',h.S.delta2,',',h.S.delta2,'} = \underbrace{',checkfrac(h.intconst22),'}_{\mathrm{int. coeff.}}', '\cdot',...
                '\underbrace{\left(',checkfrac(h.S.M1max(2)),'\right)^2}_{\mathrm{',h.S.M_2max,'}} \frac{L_{tot}}{EI}', h.S.cfval22 ,'=',checkfrac(h.S.intsol22)));    
            h = equation(Fig, h,strcat('\delta_{',h.S.delta1,',',h.S.delta2,'} = \underbrace{',checkfrac(h.S.intconst12),'}_{\mathrm{int. coeff.}}', '\cdot',...
                '\underbrace{(',checkfrac(h.S.M1max(1)),')}_{\mathrm{',h.S.M_1max,'}} \cdot \underbrace{(',checkfrac(h.S.M1max(2)),')}_{\mathrm{',h.S.M_2max,'}} \frac{L_{tot}}{EI}','=',checkfrac(h.S.intsol12)));   
        end
end

% point load on the primary structure
if cell2mat(h.beam.F(1,1)) ~= 0  && h.S.N.end >= 0
    
    if h.S.N.end > 0
        h = write(Fig, h,'title', 'Moment caused by the point loads on the primary structure:', false, true);
    else
        h = write(Fig, h,'title', 'Moment line caused by the point loads:', false, true);
    end
    
    h = beam_image_FM(Fig, h,true, false, true, false, false);
    
    h = dograph(Fig, h,0, h.beam.S.diagrams.pl, false, 'pointload');
    h.graficpointload = 0;
    
    for i=1:numel(h.beam.F(:,1))
        h.load = h.load + 1;
        if h.interactive
            for j=1:i-1            
                h.beam.loads.Fimg(j).Color = [1 1 1];
                h.beam.loads.Ftext(j).Color = [1 1 1];
            end
            for k=i+1:numel(h.beam.F(:,1))
                h.beam.loads.Fimg(k).Color = [1 1 1];
                h.beam.loads.Ftext(k).Color = [1 1 1];
            end
            if i>1
                h.beam.loads.Fimg(i).Color = 'red';
                h.beam.loads.Ftext(i).Color = 'red';
            end
        end
        h = multiplechoice_loads(Fig, h,h.beam.S.diagrams.pl(i),i);
        str = strcat('Type the value of the moment at x = ',{' '}, checkfracstring(cell2mat(h.beam.F(i,2))),'.');
        h = question(Fig, h, str, checkfracstring(double(h.S.Mloads0(i))), {' '}, true, 'point load');
        if h.S.N.end > 0
            h = equation(Fig, h,strcat('\delta_{',h.S.delta1,',0,point load ',num2str(i),'} = ...'));
            if h.Mloadequal0(i) == 1
                h = question(Fig, h, 'Type the value for the delta:', checkfracstring(0), 'Observe the moment line.', false, 'delta');
                h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,point load ',num2str(i),'} =', unitangle(0, h.unit))));    
            elseif h.S.sh > 0 && ((h.S.sh < h.S.spl(i) && h.S.rotationpoint(1) == 2 && h.S.N.end == 1) || (h.S.sh > h.S.spl(i) && h.S.rotationpoint(1) == 2 && h.S.N.end == 1))
                if double(h.intconst1pl_r(i)) == 0
                    h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1pl(i))), strcat('The integration constant for the left side is:',{' '},checkfracstring(double(h.intconst1pl_l(i))), '.',strhelpspring), false, 'delta');
                    h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,point load ',num2str(i),'} = ', ...
                        '\underbrace{',checkfrac(h.intconst1pl_l(i)),'}_{\mathrm{int. coeff. left}}', '\cdot',...
                        '\underbrace{',checkfrac(h.Mmaxpl_l(i)),'}_{\mathrm{M_{0,max} left}}', '\cdot',...
                        '\underbrace{', checkfrac(h.S.M1max_l),'}_{\mathrm{',h.S.M_1max,'left}} \cdot \frac{',checkfrac(h.S.spl(i)/h.S.l),'L_{tot}}{EI}', h.S.cfval10(i+2), '=', unitangle(h.intsol1pl(i), h.unit))));    
                else
                    h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1pl(i))), strcat('The integration constant for the left side is:',{' '},checkfracstring(double(h.intconst1pl_l(i))), '. For the right side:',{' '},checkfracstring(double(h.intconst1pl_r(i))), '.',strhelpspring), false, 'delta');
                    h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,point load ',num2str(i),'} = ', ...
                    '\underbrace{',checkfrac(h.intconst1pl_l(i)),'}_{\mathrm{int. coeff. left}}', '\cdot',...
                    '\underbrace{',checkfrac(h.Mmaxpl_l(i)),'}_{\mathrm{M_{0,max}  left}}', '\cdot',...
                    '\underbrace{', checkfrac(h.S.M1max_l),'}_{\mathrm{',h.S.M_1max,'left}} \cdot \frac{L_{tot}}{EI} + ', ...
                    '\underbrace{',checkfrac(h.intconst1pl_r(i)),'}_{\mathrm{int. coeff. right}}', '\cdot',...
                    '\underbrace{',checkfrac(h.Mmaxpl_r(i)),'}_{\mathrm{M_{0,max}  right}}', '\cdot',...
                    '\underbrace{', checkfrac(h.S.M1max_r),'}_{\mathrm{',h.S.M_1max,'right}} \cdot \frac{L_{tot}}{EI}', h.S.cfval10(i+2), '=', unitangle(h.intsol1pl(i), h.unit))));    
                end
            else
                h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1pl(i))), strcat('The integration constant is:',{' '},checkfracstring(double(h.intconst1pl(i))), '.',strhelpspring), false, 'delta');
                h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,point load ',num2str(i),'} = ', ...
                    '\underbrace{',checkfrac(h.intconst1pl(i)),'}_{\mathrm{int. coeff.}}', '\cdot',...
                    '\underbrace{',checkfrac(h.Mmaxpl(i)),'}_{\mathrm{M_{0,max}}}', '\cdot',...
                    '\underbrace{', checkfrac(h.S.M1max(1)),'}_{\mathrm{',h.S.M_1max,'}} \cdot \frac{L_{tot}}{EI}', h.S.cfval10(i+2), '=', unitangle(h.intsol1pl(i), h.unit))));   
            end
            h.delta10sum = h.delta10sum + h.intsol1pl(i);
            if isempty(h.delta10str)
                h.delta10str = strcat(h.delta10str, checkfrac(h.intsol1pl(i)));
            else
                h.delta10str = strcat(h.delta10str, {' '}, '+', {' '}, checkfrac(h.intsol1pl(i)));
            end
        end
        if h.S.N.end > 1
            if h.Mloadequal0(i) == 1
                h = equation(Fig, h,strcat('\delta_{',h.S.delta2,',0,point load ',num2str(i),'} = ...'));
                h = question(Fig, h, 'Type the value for the delta:', checkfracstring(0), 'Observe the moment line.', false, 'delta');
                h = equation(Fig, h,char(strcat('\delta_{',h.S.delta2,',0,point load ',num2str(i),'} =', unitangle(0, h.unit))));    
            else
                h = equation(Fig, h,strcat('\delta_{',h.S.delta2,',0,point load ',num2str(i),'} = ...'));
                h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol2pl(i))), strcat('The integration constant is:',{' '},checkfracstring(double(h.intconst2pl(i))), '.',strhelpspring), false, 'delta');
                h = equation(Fig, h,char(strcat('\delta_{',h.S.delta2,',0,point load ',num2str(i),'} = ', ...
                    '\underbrace{',checkfrac(h.intconst2pl(i)),'}_{\mathrm{int. coeff.}}', '\cdot',...
                    '\underbrace{',checkfrac(h.Mmaxpl(i)),'}_{\mathrm{M_{0,max}}}', '\cdot',...
                    '\underbrace{', checkfrac(h.S.M1max(2)),'}_{\mathrm{',h.S.M_1max,'}} \cdot \frac{L_{tot}}{EI} =', unitangle(h.intsol2pl(i), h.unit))));    
                h.delta20sum = h.delta20sum + h.intsol2pl(i);
                if isempty(h.delta20str)
                    h.delta20str = strcat(h.delta20str, checkfrac(h.intsol2pl(i)));
                else
                    h.delta20str = strcat(h.delta20str, {' '}, '+', {' '}, checkfrac(h.intsol2pl(i)));
                end
            end
        end
    end
    
    if h.interactive
        for i=1:numel(h.beam.F(:,1))
            h.beam.loads.Fimg(i).Color = 'red';
            h.beam.loads.Ftext(i).Color = 'red';
        end
    end
end

% h = question(Fig, h, 'Kontrollpause:', checkfracstring(0), ' ', false, 'stiffness');
% h = question(Fig, h, 'Kontrollpause:', checkfracstring(0), ' ', false, 'stiffness');

% distributed load on the primary structure
if h.beam.q1(1) ~= 0 || h.beam.q2(1) ~= 0  && h.S.N.end >= 0
    h.load = h.load + 1;
    if h.S.N.end > 0
        h = write(Fig, h,'title', 'Moment caused by the distributed load on the primary structure:', false, true);
    else
        h = write(Fig, h,'title', 'Moment line caused by the distributed load:', false, true);
    end
    
    h = beam_image_FM(Fig, h,true, false, false, true, false);
    
    h = dograph(Fig, h,0, h.beam.S.diagrams.dl, false, 'distributedload');
    h = multiplechoice_distributedload(Fig, h,h.beam.S.diagrams.dl);        
    if h.Mmaxdl >= 0
        highlow = 'maximum';
    else
        highlow = 'minimum';
    end
    
    str = strcat('Type the value of the moment at the',{' '},highlow,'.');
    helpstr = strcat('The',{' '},highlow,{' '},'is at x =',{' '},checkfracstring(h.Mmaxdlx),'.');
    h = question(Fig, h, str, checkfracstring(h.Mmaxdl), helpstr, true, 'distributed load');
    
    if h.S.N.end > 0
        qangle = h.beam.qangle;
        qareal = h.beam.q1(1)*cosd(qangle);
        qbreal = h.beam.q2(1)*cosd(qangle);
        if qareal == 0 && qbreal == 0
            h = write(Fig, h,'text', 'The orthogonal projection of the distributed load on the beam is equal to 0. Therefore the delta is equal to 0:', false, true);
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,distr. load} =', unitangle(0, h.unit))));
            if h.S.N.end == 2
                h = equation(Fig, h,char(strcat('\delta_{',h.S.delta2,',0,distr. load} =', unitangle(0, h.unit))));
            end
        else
            h = equation(Fig, h,strcat('\delta_{',h.S.delta1,',0,distr. load} = ...'));
            if h.S.sh > 0 && ((h.S.sh <= h.S.a && h.S.sh <= h.S.b && h.S.rotationpoint(1) == 2 && h.S.N.end == 1) || (h.S.sh >= h.S.a && h.S.sh >= h.S.b && h.S.rotationpoint(1) == 1 && h.S.N.end == 1)) && ((h.beam.q1(1)>=0 && h.beam.q2(1)>=0) || (h.beam.q1(1)<=0 && h.beam.q2(1)<=0))
                h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1dl)), strcat('The integration constant for the left side is:',{' '},checkfracstring(double(h.intconst1dl_l)), '. For the right side:',{' '},checkfracstring(double(h.intconst1dl_r)), '.',strhelpspring), false, 'delta');
                h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,distr. load} = ', ...
                        '\underbrace{',checkfrac(h.intconst1dl_l),'}_{\mathrm{int. coeff. left}}', '\cdot',...
                        '\underbrace{',checkfrac(h.Mmaxdl_l),'}_{\mathrm{M_{0,max}  left}}', '\cdot',...
                        '\underbrace{', checkfrac(h.S.M1max_l(1)),'}_{\mathrm{',h.S.M_1max,'left}} \cdot \frac{',checkfrac(h.S.sh/h.S.l),'L_{tot}}{EI} + ', ...
                        '\underbrace{',checkfrac(h.intconst1dl_r),'}_{\mathrm{int. coeff. right}}', '\cdot',...
                        '\underbrace{',checkfrac(h.Mmaxdl_r),'}_{\mathrm{M_{0,max}  right}}', '\cdot',...
                        '\underbrace{', checkfrac(h.S.M1max_r(1)),'}_{\mathrm{',h.S.M_1max,'right}} \cdot \frac{(1-',checkfrac(h.S.sh/h.S.l),')L_{tot}}{EI}', h.S.cfval10(1), '=', unitangle(h.intsol1dl, h.unit))));    
            else
                h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1dl)), strcat('The integration constant is:',{' '},checkfracstring(double(h.intconst1dl)), '.',strhelpspring), false, 'delta');
                h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,distr. load} = ', ...
                    '\underbrace{',checkfrac(h.intconst1dl),'}_{\mathrm{int. coeff.}}', '\cdot',...
                    '\underbrace{',checkfrac(h.Mmaxdl),'}_{\mathrm{M_{0,max}}}', '\cdot',...
                    '\underbrace{', checkfrac(h.S.M1max(1)),'}_{\mathrm{',h.S.M_1max,'}} \cdot \frac{L_{tot}}{EI}', h.S.cfval10(1), '=', unitangle(h.intsol1dl, h.unit))));    
            end
            h.delta10sum = h.delta10sum + h.intsol1dl;
            if isempty(h.delta10str)
                h.delta10str = strcat(h.delta10str, checkfrac(h.intsol1dl));
            else
                h.delta10str = strcat(h.delta10str, {' '}, '+', {' '}, checkfrac(h.intsol1dl));
            end
            if h.S.N.end > 1
                h = equation(Fig, h,strcat('\delta_{',h.S.delta2,',0,distr. load} = ...'));
                h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol2dl)), strcat('The integration constant is:',{' '},checkfracstring(double(h.intconst2dl)), '.',strhelpspring), false, 'delta');
                h = equation(Fig, h,char(strcat('\delta_{',h.S.delta2,',0,distr. load} = ', ...
                    '\underbrace{',checkfrac(h.intconst2dl),'}_{\mathrm{int. coeff.}}', '\cdot',...
                    '\underbrace{',checkfrac(h.Mmaxdl),'}_{\mathrm{M_{0,max}}}', '\cdot',...
                    '\underbrace{', checkfrac(h.S.M1max(2)),'}_{\mathrm{',h.S.M_1max,'}} \cdot \frac{L_{tot}}{EI} =', unitangle(h.intsol2dl, h.unit))));    
                h.delta20sum = h.delta20sum + h.intsol2dl;
                if isempty(h.delta20str)
                    h.delta20str = strcat(h.delta20str, checkfrac(h.intsol2dl));
                else
                    h.delta20str = strcat(h.delta20str, {' '}, '+', {' '}, checkfrac(h.intsol2dl));
                end
            end
        end
    end
end

% h = question(Fig, h, 'Kontrollpause:', checkfracstring(0), ' ', false, 'stiffness');
% h = question(Fig, h, 'Kontrollpause:', checkfracstring(0), ' ', false, 'stiffness');

% moment on the primary structure
if h.beam.M(1) ~= 0 && h.S.N.end >= 0
    h.load = h.load + 1;
    if h.S.N.end > 0
        h = write(Fig, h,'title', 'Moment line caused by the moment on the primary structure:', false, true);
    else
        h = write(Fig, h,'title', 'Moment line caused by the moment:', false, true);
    end
    
    h = beam_image_FM(Fig, h,true, false, false, false, true);

    h = dograph(Fig, h,0, h.beam.S.diagrams.m, false, 'moment');
    h = multiplechoice_moment(Fig, h,h.S.diagrams.m);
    if strcmp(ca, 'fixed') && h.S.N.end == 0
        str = strcat('Type the value of the moment at the fixed point (point 1).');
        sol = checkfracstring(double(h.S.Mvalue0at0));
    elseif strcmp(cb, 'fixed') && h.S.N.end == 0
        str = strcat('Type the value of the moment at the fixed point (point 2).');
        sol = checkfracstring(double(h.S.Mvalue0atl));
    elseif strcmp(ca, 'torsional spring') && h.S.N.end == 0
        str = strcat('Type the value of the moment at the torsional spring (point 1).');
        sol = checkfracstring(double(h.S.Mvalue0at0));
    elseif strcmp(cb, 'torsional spring') && h.S.N.end == 0
        str = strcat('Type the value of the moment at the torsional spring (point 2).');
        sol = checkfracstring(double(h.S.Mvalue0atl));
    else
        str = strcat('Type the value of the moment at the first point (point 1).');
        sol = num2str(0);
    end
    h = question(Fig, h, str, sol, {' '}, true, 'moment');
    
    if h.S.N.end > 0
        h = equation(Fig, h,strcat('\delta_{',h.S.delta1,',0,moment} = ...'));
        if h.S.sm == 0 && h.S.N.end > 0
            h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1m)), strcat('The integration constant is:',{' '}, checkfracstring(double(h.intconst1m)), '.',strhelpspring), false, 'delta');
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,moment} = ', ...
                '\underbrace{',checkfrac(h.intconst1m),'}_{\mathrm{int. coeff.}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm),'}_{\mathrm{M_{0,max}}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M1max_l),'}_{\mathrm{',h.S.M_1max,'}} \cdot \frac{L_{tot}}{EI}', h.S.cfval10(2), '=', unitangle(h.intsol1m, h.unit))));
        elseif h.S.sm == h.S.l && h.S.N.end > 0
            h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1m)), strcat('The integration constant is:',{' '}, checkfracstring(double(h.intconst1m)), '.',strhelpspring), false, 'delta');
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,moment} = ', ...
                '\underbrace{',checkfrac(h.intconst1m),'}_{\mathrm{int. coeff.}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm),'}_{\mathrm{M_{0,max}}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M1max_r),'}_{\mathrm{',h.S.M_1max,'}} \cdot \frac{L_{tot}}{EI}', h.S.cfval10(2), '=', unitangle(h.intsol1m, h.unit))));
        elseif h.S.rotationpoint(1) == 2 && h.S.sh == -1
            h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1m)), strcat('There are two integration constants, if you separate the moment line in two sections:',{' '}, checkfracstring(double(h.intconst1m_lj)),{' '}, 'and', {' '}, checkfracstring(double(h.intconst1m_rj)), '.',strhelpspring), false, 'delta');
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,moment} = ', ...
                '\underbrace{',checkfrac(h.intconst1m_lj),'}_{\mathrm{int. coeff.left}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm_lj),'}_{\mathrm{M_{0,max}left}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M1max_sm),'}_{\mathrm{',h.S.M_1max,'left}} \cdot \frac{',checkfrac(h.S.sm/h.S.l),'L_{tot}}{EI} +', ...
                '\underbrace{',checkfrac(h.intconst1m_rj),'}_{\mathrm{int. coeff.right}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm_rj),'}_{\mathrm{M_{0,max}right}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M1max(1)),'}_{\mathrm{',h.S.M_1max,'right}} \cdot \frac{L_{tot} -',checkfrac(h.S.sm/h.S.l),'L_{tot}}{EI}', h.S.cfval10(2), '=', unitangle(h.intsol1m, h.unit))));
        elseif  h.S.sh == -1
            h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1m)), strcat('There are two integration constants, if you separate the moment line in two sections:',{' '}, checkfracstring(double(h.intconst1m_lj)),{' '}, 'and', {' '}, checkfracstring(double(h.intconst1m_rj)), '.',strhelpspring), false, 'delta');
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,moment} = ', ...
                '\underbrace{',checkfrac(h.intconst1m_lj),'}_{\mathrm{int. coeff.left}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm_lj),'}_{\mathrm{M_{0,max}left}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M1max(1)),'}_{\mathrm{',h.S.M_1max,'left}} \cdot \frac{',checkfrac(h.S.sm/h.S.l),'L_{tot}}{EI} +', ...
                '\underbrace{',checkfrac(h.intconst1m_rj),'}_{\mathrm{int. coeff.right}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm_rj),'}_{\mathrm{M_{0,max}right}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M1max_sm),'}_{\mathrm{',h.S.M_1max,'right}} \cdot \frac{L_{tot} -',checkfrac(h.S.sm/h.S.l),'L_{tot}}{EI}', h.S.cfval10(2), '=', unitangle(h.intsol1m, h.unit))));
        elseif h.S.sm > h.S.sh
            h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1m)), strcat('There are three integration constants, if you separate the moment line in three sections:',{' '}, checkfracstring(double(h.intconst1m_l)),{','},{' '}, checkfracstring(double(h.intconst1m_lj)),{' '}, 'and', {' '}, checkfracstring(double(h.intconst1m_rj)), '.',strhelpspring), false, 'delta');
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,moment} = ', ...
                '\underbrace{',checkfrac(h.intconst1m_l),'}_{\mathrm{int. coeff.left}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm_l),'}_{\mathrm{M_{0,max}left}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M1max_l),'}_{\mathrm{',h.S.M_1max,'left}} \cdot \frac{',checkfrac(h.S.sh/h.S.l),'L_{tot}}{EI} +', ...
                '\underbrace{',checkfrac(h.intconst1m_lj),'}_{\mathrm{int. coeff.left}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm_lj),'}_{\mathrm{M_{0,max}left}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M1max_sm),'}_{\mathrm{',h.S.M_1max,'left}} \cdot \frac{(',checkfrac(h.S.sm/h.S.l),'-',checkfrac(h.S.sh/h.S.l),')L_{tot}}{EI} +', ...
                '\underbrace{',checkfrac(h.intconst1m_rj),'}_{\mathrm{int. coeff.right}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm_rj),'}_{\mathrm{M_{0,max}right}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M1max_r),'}_{\mathrm{',h.S.M_1max,'right}} \cdot \frac{L_{tot} -',checkfrac(h.S.sm/h.S.l),'L_{tot}}{EI}', h.S.cfval10(2), '=', unitangle(h.intsol1m, h.unit))));
        else
            h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol1m)), strcat('The integration constant is:',{' '}, checkfracstring(double(h.intconst1m_square)), '.',strhelpspring), false, 'delta');
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0,moment} = ', ...
                '\underbrace{',checkfrac(h.intconst1m_square),'}_{\mathrm{int. coeff.}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm_square),'}_{\mathrm{M_{0,max}}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M1max_l),'}_{\mathrm{',h.S.M_1max,'}} \cdot \frac{',checkfrac(h.S.sm/h.S.l),'L_{tot}}{EI}', h.S.cfval10(2), '=', unitangle(h.intsol1m, h.unit))));
        end
        
    end
    
    if h.S.N.end > 0
        h.delta10sum = h.delta10sum + h.intsol1m;
        if isempty(h.delta10str)
            h.delta10str = strcat(h.delta10str, checkfrac(h.intsol1m));
        else
            h.delta10str = strcat(h.delta10str, {' '}, '+', {' '}, checkfrac(h.intsol1m));
        end
        
        if h.S.sm == 0 && h.S.N.end > 1
            h = equation(Fig, h,strcat('\delta_{',h.S.delta2,',0,moment} = ...'));
            h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol2m)), strcat('The integration constant is:',{' '}, checkfracstring(double(h.intconst2m)), '.',strhelpspring), false, 'delta');
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta2,',0,moment} = ', ...
                '\underbrace{',checkfrac(h.intconst2m),'}_{\mathrm{int. coeff.}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm),'}_{\mathrm{M_{0,max}}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M2max_l),'}_{\mathrm{',h.S.M_2max,'}} \cdot \frac{L_{tot}}{EI} =', unitangle(h.intsol2m, h.unit))));
        elseif h.S.sm == h.S.l && h.S.N.end > 1
            h = equation(Fig, h,strcat('\delta_{',h.S.delta2,',0,moment} = ...'));
            h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol2m)), strcat('The integration constant is:',{' '}, checkfracstring(double(h.intconst2m)), '.',strhelpspring), false, 'delta');
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta2,',0,moment} = ', ...
                '\underbrace{',checkfrac(h.intconst2m),'}_{\mathrm{int. coeff.}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm),'}_{\mathrm{M_{0,max}}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M2max_r),'}_{\mathrm{',h.S.M_2max,'}} \cdot \frac{L_{tot}}{EI} =', unitangle(h.intsol2m, h.unit))));
        elseif h.S.N.end > 1
            h = equation(Fig, h,strcat('\delta_{',h.S.delta2,',0,moment} = ...'));
            h = question(Fig, h, 'Type the value for the delta:', checkfracstring(double(h.intsol2m)), strcat('There are two integration constants, if you separate the moment line in two sections:',{' '}, checkfracstring(double(h.intconst2m_lj)),{' '}, 'and', {' '}, checkfracstring(double(h.intconst2m_rj)), '.',strhelpspring), false, 'delta');
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta2,',0,moment} = ', ...
                '\underbrace{',checkfrac(h.intconst2m_lj),'}_{\mathrm{int. coeff.left}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm_lj),'}_{\mathrm{M_{0,max}left}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M2max_sm),'}_{\mathrm{',h.S.M_2max,'left}} \cdot \frac{',checkfrac(h.S.sm/h.S.l),'L_{tot}}{EI} +', ...
                '\underbrace{',checkfrac(h.intconst2m_rj),'}_{\mathrm{int. coeff.right}}', '\cdot',...
                '\underbrace{',checkfrac(h.Mmaxm_rj),'}_{\mathrm{M_{0,max}right}}', '\cdot',...
                '\underbrace{', checkfrac(h.S.M2max_r),'}_{\mathrm{',h.S.M_2max,'right}} \cdot \frac{L_{tot} -',checkfrac(h.S.sm/h.S.l),'L_{tot}}{EI} =', unitangle(h.intsol2m, h.unit))));
        end
        
        h.delta20sum = h.delta20sum + h.intsol2m;
        
        if isempty(h.delta20str)
            h.delta20str = strcat(h.delta20str, checkfrac(h.intsol2m));
        else
            h.delta20str = strcat(h.delta20str, {' '}, '+', {' '}, checkfrac(h.intsol2m));
        end
    end
end

h = superposition(Fig, h,0);
% h = question(Fig, h, 'Kontrollpause:', checkfracstring(0), ' ', false, 'stiffness');

if h.S.N.end > 0
    if h.load ~= 0
        if h.load == 1
            h = write(Fig, h,'text', 'The value of the delta is:', false, true);
        else
            h = write(Fig, h,'text', 'Sum up all the deltas:', false, true);
        end
        if ~h.allfuncequal0 && h.load > 1
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0} = \sum \delta_{',h.S.delta1,',0,i} = ', h.delta10str, {' '}, '=', {' '}, unitangle(h.delta10sum, h.unit))));
        elseif ~h.allfuncequal0 && h.load == 1
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0} =', unitangle(h.delta10sum, h.unit))));
        else
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0} =', unitangle(0, h.unit))));
        end
    else
        h = write(Fig, h,'text', 'There are no loads, therefore the delta is equal to 0:', false, true);
        h = equation(Fig, h,char(strcat('\delta_{',h.S.delta1,',0} =', unitangle(0, h.unit))));
    end
    if h.S.N.end == 2
        if ~h.allfuncequal0 && h.load > 1
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta2,',0} = \sum \delta_{',h.S.delta2,',0,i} = ', h.delta20str, {' '}, '=', {' '}, checkfrac(h.delta20sum), unitangle(0, h.unit))));
        elseif ~h.allfuncequal0 && h.load == 1
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta2,',0} =', unitangle(h.delta20sum, h.unit))));
        else
            h = equation(Fig, h,char(strcat('\delta_{',h.S.delta2,',0} =', unitangle(0, h.unit))));
        end
    end
    if h.S.N.end == 1
        h = write(Fig, h,'text', strcat('The total rotation of', {' '}, h.S.pointreleased, {' '}, 'must be equal to 0 according to the bondary conditions of its constraint.'), false, true);
        h = equation(Fig, h,strcat('\delta_{point',h.S.delta1,'} = \delta_{',h.S.delta1,',0} +',h.S.nrX_1,' \cdot \delta_{',h.S.delta1,',',h.S.delta1,'} = 0'));
        h = question(Fig, h, strcat('Which is the value of the redundant force',{' '}, h.S.nrX1,'?'), checkfracstring(h.S.X1), {' '}, false, 'unitforce');        
        h = equation(Fig, h,char(strcat('\Longrightarrow', {' '},h.S.nrX_1,' = - \frac{\delta_{',h.S.delta1,',0}}{\delta_{',h.S.delta1,{','},h.S.delta1,'}} =',checkfrac(h.S.X1))));
    else
        h = write(Fig, h,'text', strcat('The total rotation of', {' '}, h.S.pointreleased, {' '}, 'must be equal to 0 according to the bondary conditions of their constraints.'), false, true);
        h = equation(Fig, h,strcat('\delta_{',h.S.delta1,'} = \delta_{',h.S.delta1,',0} + ', h.S.nrX_1, ' \cdot \delta_{',h.S.delta1,',',h.S.delta1,'} + ', h.S.nrX_2, ' \cdot \delta_{',h.S.delta1,',',h.S.delta2,'} = 0'));
        h = equation(Fig, h,strcat('\delta_{',h.S.delta2,'} = \delta_{',h.S.delta2,',0} + ', h.S.nrX_1, ' \cdot \delta_{',h.S.delta2,',',h.S.delta1,'} + ', h.S.nrX_2, ' \cdot \delta_{',h.S.delta2,',',h.S.delta2,'} = 0'));
        h = question(Fig, h, strcat('Which is the value of the redundant force',{' '}, h.S.nrX1,'?'), checkfracstring(h.S.X1), {' '}, false, 'unitforce');        
        h = equation(Fig, h,char(strcat('\Longrightarrow ', {' '}, h.S.nrX_1, ' =',checkfrac(h.S.X1))));
        h = question(Fig, h, strcat('Which is the value of the redundant force',{' '}, h.S.nrX2,'?'), checkfracstring(h.S.X2), {' '}, false, 'unitforce');        
        h = equation(Fig, h,char(strcat('\Longrightarrow ', {' '}, h.S.nrX_2, ' =',checkfrac(h.S.X2))));
    end
    h = integrategraph(Fig, h);

    h = superposition(Fig, h,2);    
    h = write(Fig, h,'title', 'Fixed-end-moments:', false, true);
    if h.load == 0
        h = write(Fig, h,'text', 'There are no loads on the beam, so the moments are equal to 0.', false, true);
    end
    h = equation(Fig, h,strcat('M_{',h.S.pa,',',h.S.pb,'}^0 =',checkfrac(h.S.Mtot_0), h.unitM));
    h = equation(Fig, h,strcat('M_{',h.S.pb,',',h.S.pa,'}^0 =',checkfrac(h.S.Mtot_l), h.unitM));
    
    h = write(Fig, h,'title', 'Derivation of the stiffnesses:', false, true);

    h = theory(Fig, h,[{'Displacement Method'} {'title'} {1};...
                      {'The force method works very well for systems with one or two beams, but it tends to become quite complex for dealing with statically highly indeterminate structures.'} {'text'} {1};...
                      {'Like the force method, the displacement method is a way to determine the reaction forces and elastic internal forces.'} {'text'} {1};...
                      {char(strcat('While for the force method, kinematic bonds are removed (augmenting the degree of freedom) and units forces are introduced at those specific locations,',{' '},...
                      'in the displacement method deformations are applied (knot-rotation-angle \phi and beam-rotation-angle \psi) and are calculated with the help of the equilibrium conditions and the principle of minimal elastic energy.'))} {'text'} {1};...
                      {' '} {'text'} {1};...
                      {'\bfHow does the displacement method works?'} {'text'} {1};...
                      {'\bf1) \rmDetermine whether the system is displaceable or not.'} {'text'} {1};...
                      {'\bf2) \rmSeparate the system in beams and determine the knot-rotation-angle \phi for each knot. If the system is displaceable determine also the independent beam-rotation-angles \psi.'} {'text'} {1};...
                      {'\bf3) \rmDetermine the fixed-end-moments M_{i,j}^0 and M_{j,i}^0 and the stiffnesses s_{i,j}, s_{j,i}, t_{i,j} and t_{j,i}.'} {'text'} {1};...
                      {'\bf4) \rmDetermine the beam-end-moments with help of the following equation:'} {'text'} {1};...
                      {'$M_{i,j} = M_{i,j}^0 + s_{i,j}\phi_i + t_{i,j}\phi_j - (s_{i,j} + t_{i,j})\psi_{i,j}$'},{'equation'},{1};...
                      {'\bf5) \rmDo the point-equilibrium to determine \phi:'} {'text'} {1};...
                      {'$M_{i} - \sum_{j} M_{i,j} = 0$'},{'equation'},{1};...
                      {'\bf6) \rmSet the displacement equilibrium with help of the principle of virtual work and determine \psi.'} {'text'} {1};...
                      {'\bf7) \rmSuperimpose the moments to the final moment line and derivate from that the internal forces.'} {'text'} {1};...
                      {' '} {'text'} {1};...
                      {'All this steps are carried out and explained in the solutions for the displacement method. Here, we just concentrate on determining the fixed-end-moments and the stiffnesses (step 3).'} {'text'} {1}]);
    
    h = theory(Fig, h,[{'\bfHow to determine the fixed-end-moments:'} {'text'} {1};...
                      {char(strcat('The fixed-end-moments M_{i,j}^0 and M_{j,i}^0 are determined using the force method as seen before for each beam. Do not confuse these with the beam-end-moments.'))} {'text'} {1};...
                      {' '} {'text'} {1}]);

    h = theory(Fig, h,[{'\bfWhat are the stiffnesses s and t?'} {'text'} {1};...
                      {char(strcat('In the picture below the principle of determining the stiffnesses (A) is illustrated.',{' '},...
                      'In thoughts every knot is assumed to be a fixed constraint (B). In consequence they will behave stiff regarding rotation.',{' '},...
                      'This assumption violates the moment equilibrium at that specific knot, because a knot can rotate and does not have to be stiff!',{' '},...
                      'To compensate that, a rotation is applied on every specific knot (C).'))} {'text'} {1};...
                      {' '} {'text'} {1};...
                      {'theory stiffnesses.png'} {'image'} {25}]);                  
                  
    h = theory(Fig, h,[{'\bfHow to determine the stiffnesses s and t:'} {'text'} {1};...
                      {'The mentioned rotation of the knot is \phi = 1.'} {'text'} {1};...
                      {'The stiffness s is the moment required to cause this unitary rotation and t is the consequent moment of this rotation at the other end of the beam.'} {'text'} {1};...
                      {'    M_{i,j} = s_{i,j} : moment necessary in i to cause an unitary rotation \phi_i = 1 on the point i'} {'text'} {1};...
                      {'    M_{j,i} = t_{j,i} : moment  in j caused by the unitary rotation \phi_i = 1 on the point i'} {'text'} {1};...
                      {'    M_{j,i} = s_{j,i} : moment necessary in i to cause an unitary rotation \phi_i = 1 on the point i'} {'text'} {1};...
                      {'    M_{i,j} = t_{i,j} : moment  in i caused by the unitary rotation \phi_j = 1 on the point j'} {'text'} {1};...
                      {'theory stiffnesses sij.png'} {'image'} {7};...
                      {'Analogue figure for s_{j,i} and t_{i,j}.'} {'text'} {1}; ...
                      {' '} {'text'} {1}; ...
                      {'The stiffnesses are finally calculated with the following equations:'} {'text'} {1}; ...
                      {' '} {'text'} {1}; ...
                      {'             For unitary rotation of point i:'} {'text'} {1}; ...
                      {'$\phi_{i} = s_{i,j} \cdot \phi_{i,i} + t_{j,i} \cdot \phi_{i,j} = 1$'} {'equation'} {1};...
                      {'$\phi_{j} = s_{i,j} \cdot \phi_{j,i} + t_{j,i} \cdot \phi_{j,j} = 0$'} {'equation'} {1};...
                      {'             For unitary rotation of point j:'} {'text'} {1}; ...
                      {'$\phi_{j} = s_{j,i} \cdot \phi_{j,j} + t_{i,j} \cdot \phi_{j,i} = 1$'} {'equation'} {1};...
                      {'$\phi_{i} = s_{j,i} \cdot \phi_{i,j} + t_{i,j} \cdot \phi_{i,i} = 0$'} {'equation'} {1}]);

    h = equation(Fig, h,strcat('\delta_{',h.S.pa,'} = s_{',h.S.pa,',',h.S.pb,'} \cdot \delta_{',h.S.pa,',',h.S.pa,'} + t_{',h.S.pb,',',h.S.pa,'} \cdot \delta_{',h.S.pa,',',h.S.pb,'} = 1'));
    h = equation(Fig, h,strcat('\delta_{',h.S.pb,'} = s_{',h.S.pa,',',h.S.pb,'} \cdot \delta_{',h.S.pb,',',h.S.pa,'} + t_{',h.S.pb,',',h.S.pa,'} \cdot \delta_{',h.S.pb,',',h.S.pb,'} = 0'));
    h = question(Fig, h, 'Which is the value of the stiffness s (EI/L)?', checkfracstring(h.S.s12), strcat('s_{',h.S.pa,',',h.S.pb,'} is the moment acting in ',{' '}, h.S.pa, {' '}, ' causing an unitary rotation of the point ',{' '}, h.S.pa.'), false, 'stiffness');
    h = equation(Fig, h,strcat('\Longrightarrow s_{',h.S.pa,',',h.S.pb,'} = \mathrm{',checkfrac(h.S.s12), h.unitstiffness,'}'));
    h = question(Fig, h, 'Which is the value of the stiffness t (EI/L)?', checkfracstring(h.S.t12), strcat('t_{',h.S.pb,',',h.S.pa,'} is the moment acting in ',{' '}, h.S.pb, {' '}, ' causing an unitary rotation of the point ',{' '}, h.S.pa.'), false, 'stiffness');
    h = equation(Fig, h,strcat('\Longrightarrow t_{',h.S.pb,',',h.S.pa,'} = \mathrm{',checkfrac(h.S.t12), h.unitstiffness,'}'));
    h = equation(Fig, h,strcat('\delta_{',h.S.pb,'} = s_{',h.S.pb,',',h.S.pa,'} \cdot \delta_{',h.S.pb,',',h.S.pb,'} + t_{',h.S.pa,',',h.S.pb,'} \cdot \delta_{',h.S.pb,',',h.S.pa,'} = 1'));
    h = equation(Fig, h,strcat('\delta_{',h.S.pa,'} = s_{',h.S.pb,',',h.S.pa,'} \cdot \delta_{',h.S.pa,',',h.S.pb,'} + t_{',h.S.pa,',',h.S.pb,'} \cdot \delta_{',h.S.pa,',',h.S.pa,'} = 0'));
    h = question(Fig, h, 'Which is the value of the stiffness s (EI/L)?', checkfracstring(h.S.s21), strcat('s_{',h.S.pb,',',h.S.pa,'} is the moment acting in ',{' '}, h.S.pb, {' '}, ' causing an unitary rotation of the point ',{' '}, h.S.pa,'.'), false, 'stiffness');
    h = equation(Fig, h,strcat('\Longrightarrow s_{',h.S.pb,',',h.S.pa,'} = \mathrm{',checkfrac(h.S.s21), h.unitstiffness,'}'));
    h = question(Fig, h, 'Which is the value of the stiffness t (EI/L)?', checkfracstring(h.S.t12), strcat('t_{',h.S.pa,',',h.S.pb,'} is the moment acting in ',{' '}, h.S.pa, {' '}, ' causing an unitary rotation of the point ',{' '}, h.S.pa,'.'), false, 'stiffness');
    h = equation(Fig, h,strcat('\Longrightarrow t_{',h.S.pa,',',h.S.pb,'} = t_{',h.S.pb,',',h.S.pa,'} = \mathrm{',checkfrac(h.S.t12), h.unitstiffness,'}'));
    h = write(Fig, h,'text', 'Well done! The fixed-end-moments and stiffnesses are defined. You can now choose another beam or continue with the kinematic analysis.', false, false);

else
    h = write(Fig, h,'text', 'The system is statically determined, therefore the stiffnesses are equal to 0:', false, true);
    h = equation(Fig, h,strcat('\Longrightarrow s_{',h.S.pa,',',h.S.pb,'} = s_{',h.S.pb,',',h.S.pa,'} = t_{',h.S.pa,',',h.S.pb,'} = t_{',h.S.pb,',',h.S.pa,'} = \mathrm{0', h.unitstiffness,'}'));
    h = write(Fig, h,'text', 'You can now choose another beam or continue with the kinematic analysis.', false, false);
end
% else
%     uiresume(h);
h = question(Fig, h, 'ische', 'ische', 'ische', false, 'empty');
end


%print
dopdf = uicontrol('Parent',h.questionpanel,'Style','text','String','Print','HorizontalAlignment','center','BackgroundColor',[0.38, 0.48, 0.55],'FontSize',12,'ForegroundColor','white', 'FontName','Calibri','Position',[h.width-50, height-20, 50, 20],'ButtonDownFcn',@printpage);
dopdf.Enable = 'Inactive';

function printpage(hObject,eventdata,figure)
    % get the proper format
    set(Fig, 'PaperUnits', 'points');
    set(Fig, 'PaperSize', [width, height]); 
    set(Fig, 'PaperPosition', [0, 0, width, height]); %[ left, bottom, width, height]
    set(Fig, 'PaperPositionMode', 'auto');
    
    document = 'force method.fig';
    copyobj(h.panel1,gcf); 
    print('doc1','-dpng','-r0')

%  print(gcf,'-dpdf',document);
    copyobj(h.panel2,gcf); 
        print('doc2','-dpng','-r0');

%     cmdfile = ['force method' '.txt'];
% % fh = fopen(cmdfile, 'w');
% % print(fh, '-q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile="%s" -f', 'page0.pdf');
% % print(fh, ' "%s"', 'page1.pdf');    
% %     print(fig1, '-dpdf', 'Filename.pdf')
% fprintf(fig0, '-q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOutputFile="%s" -f', 'forcemethod.pdf');%     print(gcf, '-dpdf', ['force method','.pdf']);
% fprintf(fig_handle, '-dpsc', '-append', 'D:\output_dir\output_file.ps')
end

% Use sprintf to create text with a new line character, such as sprintf('first line \n second line'). This property converts text with new line characters to cell arrays.
guidata(Fig,h);
uiwait(Fig);
end