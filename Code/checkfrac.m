function number = checkfrac(frac)
[n,d] = numden(sym(frac));
if d >= 9999
    number = num2str(round(double(frac),4));
else
    if frac == 1 || frac == -1
        number = strcat('{',num2str(double(n)),'}');
    elseif d == 1
        number = strcat('{',num2str(double(n)),'}');
    else
        if rem(n,1) ~= 0
            f = n/d;
            number = strcat('{',num2str(double(f)),'}');
        else
            number = strcat('\frac{',num2str(double(n)),'}{',num2str(double(d)),'}');
        end
    end
end
end
