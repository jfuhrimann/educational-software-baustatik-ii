function [M0, delta10, delta20] = moment_force(l,F,knottype_a,knottype_b,cf_a,cf_b,EI,delta10,delta20,M0,M1,M2)

syms x

    for i=1:numel(F(:,1))
        F(i,1) = F(i,1)*cosd(F(i,3));
        s = F(i,2);

        if strcmp(knottype_b,'free')
             Va = F(i,1);
             Mfixed = -F(i,1)*F(i,2);
         elseif strcmp(knottype_a,'free')
             Va = 0;
             Mfixed = 0;
         else
             Va = F(i,1)*(l-s)/l;
             Mfixed = 0;
        end
                       
        M01 = @(x) Va*x*heaviside(s-x) + Mfixed*heaviside(l-x);
        M02 = @(x) (Va*x - F(i,1)*(x-s))*heaviside(x-s);
        M0 = @(x) M0(x) + M01(x) + M02(x);

        delta10 = int(M0(x)*M1(x)/EI,x,0,l) + cf_b*l*(F(i,1)-Va) + delta10;
        delta20 = int(M0(x)*M2(x)/EI,x,0,l) + cf_a*l*F(i,1) + delta20;
    end
end