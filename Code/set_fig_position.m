function fig_position(h, top, left, height, width)

%             H is the handle to the figure.
%             TOP is the "y" screen coordinate for the top of the figure
%             LEFT is the "x" screen coordinate for the left side of the figure
%             HEIGHT is how tall you want the figure
%             WIDTH is how wide you want the figure
%
% Author: sparafucile17

% PC's active screen size
screen_size = get(0,'ScreenSize');
pc_width  = screen_size(3)
pc_height = screen_size(4)

% find center of screen
centerx = pc_width/2;
centery = pc_height/2;

%Matlab also does not consider the width of the border:
window_border  = 5;

%new position:
m_left   = centerx + left/2 + window_border;
m_bottom = pc_height - height - top - 1;
m_height = height;
m_width  = width - 1;

%Set the correct position of the figure
set(h, 'Position', [m_left, m_bottom, m_width, m_height]);

%If you want it to print to file correctly, this must also be set
% and you must use the "-r72" scaling to get the proper format
set(h, 'PaperUnits', 'points');
set(h, 'PaperSize', [width, height]); 
set(h, 'PaperPosition', [0, 0, width, height]); %[ left, bottom, width, height]