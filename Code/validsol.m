function [sol,varstosolve] = validsol(equation,varstosolve,dgr)
emptysol = [];
vartest = varstosolve(1);   %This one is always part of the solution
if numel(equation) > 1
while true
for i = 2 : numel(equation)
    for j = 1 : numel(symvar(equation(i)))
        eqnvars = symvar(equation(i));
        stop = false;
        if not(ismember(eqnvars(j),vartest))
            if not(isempty(emptysol))
            for k = 1 : numel(emptysol(:,1))    
                if not(ismember(eqnvars(j),emptysol(k,:)))
                    vartest = [vartest eqnvars(j)];
                    stop = true;
                    break;
                end
            end
            else
                vartest = [vartest eqnvars(j)];
                stop = true;
                break;
            end
        end
        if stop
            break;
        end
    end
end
%Only emtpy solutions
soltest = solve(equation,[vartest]);
solnames = fieldnames(soltest);
for n = 1 : numel(solnames)
    stop2 = false;
    if isempty(soltest.(char(solnames(n))))
        stop2 = true;
        break;
    end
end
if stop2
emptysol = [emptysol; vartest];
vartest = varstosolve(1);
else
varstosolve = vartest;
sol = soltest;
break;
end
end
else
    varstosolve = varstosolve(1);
    sol = solve(equation,varstosolve);
end
end