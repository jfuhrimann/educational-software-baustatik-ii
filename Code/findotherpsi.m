function result = findotherpsi(k,matching_pi,matching_ti,handles)
% Go through all of the neighbours of the precendent point k with number neighbours(i) >k
for i = 1 : numel(handles.points.(pointname(k)).neighbours)
    if not(isfield(handles.points.(pointname(handles.points.(pointname(k)).neighbours(i))),'done'))||not(handles.points.(pointname(handles.points.(pointname(k)).neighbours(i))).done)
%     if handles.points.(pointname(k)).neighbours(i)>k
    current = handles.points.(pointname(k)).neighbours(i);
    handles.points.(pointname(current)).done = true;
    r = handles.points.(pointname(current)).r;
    if isfield(handles.points.(pointname(current)),'pvec')
    pvec = handles.points.(pointname(current)).pvec;
    tvec = handles.points.(pointname(current)).tvec;
    else
        pvec = [];
        tvec = [];
    end
    matching_p = pvec;
    matching_t = tvec;
    neighbours = handles.points.(pointname(current)).neighbours;
    if not(isempty(pvec))
    nlines = numel(pvec(1,:));
    else nlines = 0;
    end
    if nlines >= 2
        handles.points.(pointname(current)).newfix = false;
        handles.points.(pointname(current)).fix = true;
    end
% Look at all of the neighbours of the current point. 
% Is the current point on one of their locklines? 
    for m = 1 : numel(neighbours)
    if isfield(handles.points.(pointname(neighbours(m))),'pvec')
    pvecn = handles.points.(pointname(neighbours(m))).pvec;
    tvecn = handles.points.(pointname(neighbours(m))).tvec;
    for j = 1 : numel(pvecn(1,:))
        if isonline(pvecn(:,j),tvecn(:,j),r)
            if not(isempty(matching_t))
            for x = 1 : numel(matching_t(1,:))
                foundcollinear = false;
                if iscollinear(matching_t(:,x),tvecn(:,j))
                    foundcollinear = true;
                    break;
                end
            end
            else foundcollinear = false;
            end
                if not(foundcollinear)
                nlines = nlines + 1;
                matching_p = [matching_p pvecn(:,j)];
                matching_t = [matching_t tvecn(:,j)];
                end
        end
        end
    end
    end
%is the current point on "hidden locklines"?
% nlines = nlines + otherline(current,handles);
    %look back from the new point with nr "current" to precedent point with nr "k"
    if nlines >=2
        handles.points.(pointname(current)).fix = true;
        if handles.points.(pointname(k)).fix % precedent point fix as well
            handles.beams.(beamname(k,current)).haspsi = false;
        else 
            if not(isempty(matching_ti))
                for j = 1 : numel(matching_ti(1,:))
                    foundpsi = true;
                    if not(iscollinear(matching_ti(:,j),handles.beams.(beamname(k,current)).t)) && not(all(matching_pi(:,j) == r))
                        foundpsi = false; %have found a lockline originating not on point current not collinear with this beam
                        break;
                    end
                end
            else foundpsi = true;
            end
                handles.beams.(beamname(k,current)).haspsi = foundpsi;
        end
        if not(isfield(handles.points.(pointname(current)),'newfix'))
            handles.points.(pointname(current)).newfix = true; %New fixpoint found (since it has become a fixpoint through its neighbours)
            %Create new lockline in the direction of all of the proceeding
            %beams if not already existing.
        for o = 1 : numel(neighbours)
            if isfield(handles.points.(pointname(neighbours(o))),'tvec')
            for p = 1 : numel(handles.points.(pointname(neighbours(o))).tvec(1,:))
                stop = false;
            if iscollinear(handles.beams.(beamname(current,neighbours(o))).t, handles.points.(pointname(neighbours(o))).tvec(:,p))
                stop = true;
                break;
            end
            end
            else stop = false;
            end
            if isfield(handles.points.(pointname(current)),'tvec')
            for p = 1 : numel(handles.points.(pointname(current)).tvec(1,:))
                stop2 = false;
                if iscollinear(handles.beams.(beamname(current,neighbours(o))).t, handles.points.(pointname(current)).tvec(:,p))
                stop2 = true;
                break;
                end
            end
            else
                stop2 = false;
            end

            if not(stop) && not(stop2)
                if isfield(handles.points.(pointname(current)),'pvec')
                handles.points.(pointname(current)).pvec = [handles.points.(pointname(current)).pvec r];
                handles.points.(pointname(current)).tvec = [handles.points.(pointname(current)).tvec handles.beams.(beamname(current,neighbours(o))).t];
                else
                handles.points.(pointname(current)).pvec = r;
                handles.points.(pointname(current)).tvec = handles.beams.(beamname(current,neighbours(o))).t;
                end
            end
        end
        end
    elseif nlines == 1
        handles.points.(pointname(current)).newfix = false;
        handles.points.(pointname(current)).fix = false;
        if not(isempty(matching_p)) %This lockline is not a "hidden lockline"
            if isempty(matching_ti)
                handles.beams.(beamname(k,current)).haspsi = true;
            else
            for q = 1:numel(matching_ti(1,:))
                foundpsi = true;
            if iscollinear(matching_ti(:,q),matching_t) && not(all(matching_p == matching_pi(:,q)))
                %No psi if one lockline of each this point and the precedent
                %one are collinear
                foundpsi = false;
                break;
            end
            end
                handles.beams.(beamname(k,current)).haspsi = foundpsi;
            end
        else %this lockline is a "hidden lockline"
            handles.beams.(beamname(k,current)).haspsi = true;
        end
    elseif nlines == 0
        handles.points.(pointname(current)).fix = false;
        handles.points.(pointname(current)).newfix = false;
        handles.beams.(beamname(k,current)).haspsi = true;
    end
    if nlines == 1
        for o = 1 : numel(neighbours)
            this_neighbour = neighbours(o);
            if this_neighbour > current
            this_t = handles.beams.(beamname(current,this_neighbour)).t;
            if iscollinear(matching_t,this_t)
                if not(isfield(handles.points.(pointname(this_neighbour)),'tvec')) || not(iscollinear(handles.points.(pointname(this_neighbour)).tvec,this_t))
                    if not(isfield(handles.points.(pointname(current)),'tvec')) || not(iscollinear(handles.points.(pointname(current)).tvec,this_t))
                        handles.points.(pointname(current)).pvec = matching_p;
                        handles.points.(pointname(current)).tvec = matching_t; 
                    end
                end
            end
            end
        end
    end
    
    
    
    % start the recursion
        former_nlines = nlines;
        handles = findotherpsi(current,matching_p,matching_t,handles); 

       
    % go back along the same path through the system after the recursion
    % 1) on how many locklines is "current" now?

    for s = 1 : numel(neighbours)
        if neighbours(s)~=k
        if isfield (handles.points.(pointname(neighbours(s))),'pvec')
        for d = 1 : numel(handles.points.(pointname(neighbours(s))).pvec(1,:))
            if isfield(handles.points.(pointname(neighbours(s))),'newfix')
            if handles.points.(pointname(neighbours(s))).newfix 
                pvecn = handles.points.(pointname(neighbours(s))).pvec;
                tvecn = handles.points.(pointname(neighbours(s))).tvec;
                if isonline(pvecn(:,d),tvecn(:,d),r)
                    if not(isempty(matching_t))
                    for x = 1 : numel(matching_t(1,:))
                        foundcollinear = false;
                        if iscollinear(matching_t(:,x),tvecn(:,d))
                            foundcollinear = true;
                            break;
                        end
                    end
                    else foundcollinear = false;
                    end
                    if not(foundcollinear)
                    nlines = nlines + 1;
                    matching_p = [matching_p pvecn(:,d)];
                    matching_t = [matching_t tvecn(:,d)];
                    end
                end
            end
            end
        end
        end
        end
    end
        % 3) Creating new locklines
        if nlines > 1
            handles.points.(pointname(current)).fix = true;
          if former_nlines < 2
                handles.points.(pointname(current)).newfix = true;
            % Create new locklines since this point is newfix
            for o = 1 : numel(neighbours)
            if isfield(handles.points.(pointname(neighbours(o))),'tvec')
            for p = 1 : numel(handles.points.(pointname(neighbours(o))).tvec(1,:))
                stop = false;
            if iscollinear(handles.beams.(beamname(current,neighbours(o))).t, handles.points.(pointname(neighbours(o))).tvec(:,p))
                stop = true;
                break;
            end
            end
            else stop = false;
            end
            if isfield(handles.points.(pointname(current)),'tvec')
            for p = 1 : numel(handles.points.(pointname(current)).tvec(1,:))
                stop2 = false;
                if iscollinear(handles.beams.(beamname(current,neighbours(o))).t, handles.points.(pointname(current)).tvec(:,p))
                stop2 = true;
                break;
                end
            end
            else
                stop2 = false;
            end

            if not(stop) && not(stop2)
                if isfield(handles.points.(pointname(current)),'pvec')
                handles.points.(pointname(current)).pvec = [handles.points.(pointname(current)).pvec r];
                handles.points.(pointname(current)).tvec = [handles.points.(pointname(current)).tvec handles.beams.(beamname(current,neighbours(o))).t];
                else
                handles.points.(pointname(current)).pvec = r;
                handles.points.(pointname(current)).tvec = handles.beams.(beamname(current,neighbours(o))).t;
                end
            end
            end
          end
        end
        % 4) Reanalysing the psis
        for s = 1 : numel(neighbours)
            if nlines >= 1
            if isfield(handles.points.(pointname(neighbours(s))),'newfix') && handles.points.(pointname(neighbours(s))).newfix
                handles.beams.(beamname(current,neighbours(s))).haspsi = false;
            end
            if nlines == 1 %collinear roller or free end (psi exists again, but "current" remains fix.
            if not(isempty(matching_t)) && iscollinear(matching_t,handles.beams.(beamname(current,neighbours(s))).t)
                handles.beams.(beamname(current,neighbours(s))).haspsi = true;
            end
            end
            end
        end
%     end
    end
end
result = handles;
end