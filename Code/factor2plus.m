function factor = factor2plus(pnr,handles)
current = handles.paths.(pathname(pnr))(numel(handles.paths.(pathname(pnr))));
precurrent = handles.paths.(pathname(pnr))(numel(handles.paths.(pathname(pnr)))-1);
ncurrent = handles.beams.(beamname(current,precurrent)).n;
tvec = handles.points.(pointname(current)).tvec;
for i = numel(handles.paths.(pathname(pnr))) : 3
    previous = handles.paths.(pathname(pnr))(i-1);
    preprevious = handles.paths.(pathname(pnr))(i-2);
    n = handles.beams.(beamname(preprevious,previous)).n;
    if not(iscollinear(n,ncurrent))
        if previous > preprevious
            n = n;
        else
            n = -n;
        end
        break;
    end
end
if dot(n,rot90vec(tvec)) >= 0
    factor = 1;
else
    factor = -1;
end
        
end