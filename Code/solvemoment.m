function [M0, Vatot, Vbtot, diagrams] = solvemoment(l,F,a,b,qa,qb,sm,M,knottype_a,knottype_b)
syms M0(x)

    M0(x) = 0;
    diagrams.pl = piecewise(x>=0 & x<=l, 0);
    diagrams.dl = 0;
    diagrams.m = 0; 
    
    [M0, VatotF, VbtotF, diagrams] = moment_pointload(l,F,knottype_a,knottype_b,M0, diagrams);

     if not(qa==0 && qb==0)
         [M0, Vatotq, Vbtotq, diagrams] = moment_distributedload(l,a,b,qa,qb,knottype_a,knottype_b,M0, diagrams);
     else
        Vatotq = 0;
        Vbtotq = 0;
     end

     if M~=0
        [M0, VatotM, VbtotM, diagrams] = moment_moment(l,sm,M,knottype_a,knottype_b,M0, diagrams);
     else
        VatotM = 0;
        VbtotM = 0;
     end
    
     Vatotq = Vatotq + VatotF(1);
     Vbtotq = Vbtotq + VbtotF(1);     
     VatotM = VatotM + VatotF(2);
     VbtotM = VbtotM + VbtotF(2);
     
    Vatot = [Vatotq; VatotM; VatotF(3:numel(VatotF))];
    Vbtot = [Vbtotq; VbtotM; VbtotF(3:numel(VbtotF))];
end
