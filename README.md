# Educational Software Baustatik II

This project contains an educational software about the displacement and the force method. The original version of the code has
been written in 2018 and an update has been made in 2020. 

The software aims at providing the students with a useful tool for introducing basic mechanical concepts. The software has been written in MATLAB.

## Repository Structure

The repo contains two folders (Code and Manual).

In the manual, the way of running the software and the system requirements are explained.
